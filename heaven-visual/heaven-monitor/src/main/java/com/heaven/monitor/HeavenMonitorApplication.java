package com.heaven.monitor;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 监控中心
 *
 * @author Vicene
 */
@EnableAdminServer
@SpringBootApplication
public class HeavenMonitorApplication {
    public static void main(String[] args) {
        SpringApplication.run(HeavenMonitorApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  苍穹-服务监控模块启动成功   ლ(´ڡ`ლ)ﾞ ");
    }
}
