package com.heaven.dataconverge.dataprovider.jdbc.adapter;

import com.heaven.common.core.exception.ServiceException;
import com.heaven.dataconverge.dataprovider.base.jdbc.JdbcProperties;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import reactor.core.Exceptions;

import java.io.Closeable;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;


@Slf4j
@Setter
@Getter
public class JdbcDataProviderAdapter implements Closeable {

    @Override
    public void close() throws IOException {

    }

    public boolean test(JdbcProperties properties) {
        try {
            Class.forName(properties.getDriverClass());
        } catch (ClassNotFoundException e) {
            String errMsg = "Driver class not found " + properties.getDriverClass();
            log.error(errMsg, e);
            throw new ServiceException("没有对应驱动异常");
        }
        try {
            DriverManager.getConnection(properties.getUrl(), properties.getUser(), properties.getPassword());
        } catch (SQLException sqlException) {
            throw new ServiceException("测试连接异常,请修改参数");
        }
        return true;
    }
}