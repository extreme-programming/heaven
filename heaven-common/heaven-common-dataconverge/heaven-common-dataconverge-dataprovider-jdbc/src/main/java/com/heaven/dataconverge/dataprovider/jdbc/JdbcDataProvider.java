package com.heaven.dataconverge.dataprovider.jdbc;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.heaven.common.core.exception.ServiceException;
import com.heaven.common.core.utils.file.FileUtils;
import com.heaven.common.data.constant.Constants;
import com.heaven.common.data.info.DataSourceInfo;
import com.heaven.common.data.provider.DataProvider;
import com.heaven.common.data.provider.DataProviderSource;
import com.heaven.dataconverge.dataprovider.base.jdbc.JdbcDriverInfo;
import com.heaven.dataconverge.dataprovider.base.jdbc.JdbcProperties;
import com.heaven.dataconverge.dataprovider.jdbc.adapter.JdbcDataProviderAdapter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.yaml.snakeyaml.Yaml;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.stream.Collectors;

@Service(Constants.DATA_PROVIDER_JDBC)
@Slf4j
public class JdbcDataProvider extends DataProvider {

    private static final String JDBC_DRIVER_BUILD_IN = "/jdbc-driver.yml";

    private static final String JDBC_DRIVER_EXT = "/jdbc-driver-ext.yml";

    public static final String DEFAULT_ADAPTER = "com.heaven.dataconverge.dataprovider.jdbc.adapter.JdbcDataProviderAdapter";

    public static final String DB_TYPE = "dbType";

    public static final String USER = "user";

    public static final String PASSWORD = "password";

    public static final String URL = "url";

    public static final String DRIVER_CLASS = "driverClass";

    public static final String ENABLE_SPECIAL_SQL = "enableSpecialSQL";

    /**
     * 获取连接时最大等待时间（毫秒）
     */
    public static final Integer DEFAULT_MAX_WAIT = 5000;

    @Override
    public Object testConnection(DataProviderSource source) {
        JdbcProperties jdbcProperties = conv2JdbcProperties(source);
        return ProviderFactory.createDataProvider(jdbcProperties, false).test(jdbcProperties);
    }

    @Override
    public Set<String> readAllDatabases(DataSourceInfo source) throws SQLException {
        return null;
    }

    @Override
    public Set<String> readTables(DataSourceInfo source, String database) throws SQLException {
        return null;
    }

    @Override
    public void updateSource(DataSourceInfo source) {

    }

    private JdbcProperties conv2JdbcProperties(DataProviderSource config) {
        JdbcProperties jdbcProperties = new JdbcProperties();
        jdbcProperties.setDbType(config.getProperties().get(DB_TYPE).toString().toUpperCase());
        jdbcProperties.setUrl(config.getProperties().get(URL).toString());
        Object user = config.getProperties().get(USER);
        if (user != null && StringUtils.isNotBlank(user.toString())) {
            jdbcProperties.setUser(user.toString());
        }
        Object password = config.getProperties().get(PASSWORD);
        if (password != null && StringUtils.isNotBlank(password.toString())) {
            jdbcProperties.setPassword(password.toString());
        }
        String driverClass = config.getProperties().getOrDefault(DRIVER_CLASS, "").toString();
        jdbcProperties.setDriverClass(StringUtils.isBlank(driverClass) ?
                ProviderFactory.getJdbcDriverInfo(jdbcProperties.getDbType()).getDriverClass() :
                driverClass);

        Object enableSpecialSQL = config.getProperties().get(ENABLE_SPECIAL_SQL);

        if (enableSpecialSQL != null && "true".equals(enableSpecialSQL.toString())) {
            jdbcProperties.setEnableSpecialSql(true);
        }

        Object properties = config.getProperties().get("properties");
        if (properties != null) {
            if (properties instanceof Map) {
                Properties prop = new Properties();
                prop.putAll((Map) properties);
                jdbcProperties.setProperties(prop);
            }
        }
        return jdbcProperties;
    }

    public static class ProviderFactory {

        private static final Map<String, JdbcDriverInfo> jdbcDriverInfoMap = new ConcurrentSkipListMap<>();
        public static Map<String, Map<String, String>>  CACHE_DRIVER_INFOS = new ConcurrentSkipListMap<>();

        public static JdbcDataProviderAdapter createDataProvider(JdbcProperties prop, boolean init) {
            List<JdbcDriverInfo> jdbcDriverInfos = loadDriverInfoFromResource();

            List<JdbcDriverInfo> driverInfos = jdbcDriverInfos.stream().filter(item -> prop.getDbType().equals(item.getDbType()))
                    .collect(Collectors.toList());

            if (driverInfos.size() == 0) {
                throw new ServiceException("不支持的DB类型: " + prop.getDbType());
            }
            if (driverInfos.size() > 1) {
                throw new ServiceException("重复的DB类型: " + prop.getDbType());
            }
            JdbcDriverInfo driverInfo = driverInfos.get(0);

            if (StringUtils.isNotBlank(prop.getDriverClass())) {
                driverInfo.setDriverClass(prop.getDriverClass());
            }
            JdbcDataProviderAdapter adapter = null;
            try {
                if (StringUtils.isNotBlank(driverInfo.getAdapterClass())) {
                    try {
                        Class<?> aClass = Class.forName(driverInfo.getAdapterClass());
                        adapter = (JdbcDataProviderAdapter) aClass.newInstance();
                    } catch (Exception e) {
                        log.error("Jdbc类加载器 (" + driverInfo.getAdapterClass() + ") 加载失败");
                    }
                }
                if (adapter == null) {
                    adapter = (JdbcDataProviderAdapter) Class.forName(DEFAULT_ADAPTER).newInstance();
                }
            } catch (Exception e) {
                log.error("Jdbc类加载器 (" + driverInfo.getAdapterClass() + ") 加载失败");
            }
            if (adapter == null) {
                throw new ServiceException("创建DB类型: " +prop.getDbType()+ "provider异常");
            }
            if (init) {
                //adapter.init(prop, driverInfo);
            }
            return adapter;
        }

        private static JdbcDriverInfo getJdbcDriverInfo(String dbType) {
            if (jdbcDriverInfoMap.isEmpty()) {
                for (JdbcDriverInfo jdbcDriverInfo : loadDriverInfoFromResource()) {
                    jdbcDriverInfoMap.put(jdbcDriverInfo.getDbType(), jdbcDriverInfo);
                }
            }
            return jdbcDriverInfoMap.get(dbType);
        }

        public static List<JdbcDriverInfo> loadDriverInfoFromResource() {
            if(CACHE_DRIVER_INFOS.size() == 0){
                loadDriverInfo();
            }
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.setPropertyNamingStrategy(PropertyNamingStrategy.KEBAB_CASE);
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            return CACHE_DRIVER_INFOS.entrySet().stream().map(entry -> {
                try {
                    JdbcDriverInfo jdbcDriverInfo = objectMapper.convertValue(entry.getValue(), JdbcDriverInfo.class);
                    if (StringUtils.isBlank(jdbcDriverInfo.getAdapterClass())) {
                        jdbcDriverInfo.setAdapterClass(DEFAULT_ADAPTER);
                    }
                    if (jdbcDriverInfo.getQuoteIdentifiers() == null) {
                        jdbcDriverInfo.setQuoteIdentifiers(true);
                    }
                    jdbcDriverInfo.setDbType(jdbcDriverInfo.getDbType().toUpperCase());
                    return jdbcDriverInfo;
                } catch (Exception e) {
                    log.error("DbType: " + entry.getKey() + " driver读取异常", e);
                }
                return null;
            }).filter(Objects::nonNull).sorted(Comparator.comparing(JdbcDriverInfo::getDbType)).collect(Collectors.toList());
        }

        public static Map<String, Map<String, String>> loadDriverInfo(){
            Map<String, Map<String, String>> buildIn = loadYml(JDBC_DRIVER_BUILD_IN);

            Map<String, Map<String, String>> extDrivers = loadYml(JDBC_DRIVER_EXT);
            if (!CollectionUtils.isEmpty(extDrivers)) {
                for (String key : extDrivers.keySet()) {
                    Map<String, String> driver = buildIn.get(key);
                    if (driver == null) {
                        buildIn.put(key, extDrivers.get(key));
                    } else {
                        driver.putAll(extDrivers.get(key));
                    }
                }
            }
            CACHE_DRIVER_INFOS = buildIn;
            return buildIn;
        }

        private static Map<String, Map<String, String>> loadYml(String file) {
            try (InputStream inputStream = ProviderFactory.class.getResourceAsStream(file)) {
                Yaml yaml = new Yaml();
                return yaml.loadAs(inputStream, HashMap.class);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        private static Map<String, Map<String, String>> loadYml(File file) {
            try (InputStream inputStream = new FileInputStream(file)) {
                Yaml yaml = new Yaml();
                return yaml.loadAs(inputStream, HashMap.class);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }


//    @Override
//    public void resetSource(DataProviderSource source) {
//        try {
//            JdbcDataProviderAdapter adapter = cachedProviders.remove(source.getSourceId());
//            if (adapter != null) {
//                adapter.close();
//            }
//            //log.info("jdbc source '{}-{}' updated, source has been reset", source.getSourceId(), source.getName());
//        } catch (Exception e) {
//            //log.error("source reset error.", e);
//        }
//    }
}
