package com.heaven.dataconverge.dataprovider.base;

import com.heaven.common.data.constant.Constants;
import com.heaven.common.data.info.DataProviderInfo;
import com.heaven.common.data.info.DataSourceInfo;
import com.heaven.common.data.provider.DataProvider;
import com.heaven.common.data.provider.DataProviderSource;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.Set;

@Service(Constants.DATA_PROVIDER_DEFAULT)
public class DefaultDataProvider extends DataProvider {

    @Override
    public Object testConnection(DataProviderSource source) throws Exception {
        return null;
    }

    @Override
    public Set<String> readAllDatabases(DataSourceInfo source) throws SQLException {
        return null;
    }

    @Override
    public Set<String> readTables(DataSourceInfo source, String database) throws SQLException {
        return null;
    }

    @Override
    public void updateSource(DataSourceInfo source) {

    }
}
