package com.heaven.dataconverge.dataprovider.base;

import com.heaven.common.data.info.DataProviderInfo;
import com.heaven.common.data.info.DataSourceInfo;
import com.heaven.common.data.provider.DataProvider;
import com.heaven.common.data.provider.DataProviderManager;
import com.heaven.common.data.provider.DataProviderSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.*;
@Service
public class ProviderManager implements DataProviderManager {

    @Autowired
    private Map<String, DataProvider> cachedDataProviders;


    @Override
    public Object testConnection(DataProviderSource source) throws Exception {
        return cachedDataProviders.get(source.getSourceType()).testConnection(source);
    }

    @Override
    public Set<String> readAllDatabases(DataSourceInfo source) throws SQLException {
        return null;
    }

    @Override
    public Set<String> readTables(DataSourceInfo source, String database) throws SQLException {
        return null;
    }

    @Override
    public void updateSource(DataSourceInfo source) {

    }
}
