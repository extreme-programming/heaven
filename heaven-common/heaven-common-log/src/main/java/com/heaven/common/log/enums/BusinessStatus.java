package com.heaven.common.log.enums;

/**
 * 操作状态
 * 
 * @author Vicene
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
