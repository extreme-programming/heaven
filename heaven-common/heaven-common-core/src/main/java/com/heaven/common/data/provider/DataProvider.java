package com.heaven.common.data.provider;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.heaven.common.data.info.DataProviderInfo;
import com.heaven.common.data.info.DataSourceInfo;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;

public abstract class DataProvider {
    static ObjectMapper MAPPER = new ObjectMapper();

    static {
        MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }
    public abstract Object testConnection(DataProviderSource source) throws Exception;

    public abstract Set<String> readAllDatabases(DataSourceInfo source) throws SQLException;

    public abstract Set<String> readTables(DataSourceInfo source, String database) throws SQLException;

    public abstract void updateSource(DataSourceInfo source);

}