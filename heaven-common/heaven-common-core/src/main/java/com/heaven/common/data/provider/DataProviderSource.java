package com.heaven.common.data.provider;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Map;

@Data
@ToString
public class DataProviderSource implements Serializable {

    private String sourceId;

    private String sourceType;

    private String name;

    private Map<String, Object> properties;

}