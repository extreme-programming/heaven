package com.heaven.common.core.exception;

/**
 * 权限异常
 * 
 * @author Vicene
 */
public class PreAuthorizeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public PreAuthorizeException()
    {
    }
}
