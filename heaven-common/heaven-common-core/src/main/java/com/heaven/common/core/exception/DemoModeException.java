package com.heaven.common.core.exception;

/**
 * 演示模式异常
 * 
 * @author Vicene
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
