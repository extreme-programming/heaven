package com.heaven.common.data.info;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DataProviderInfo {

    /** 名称 */
    private String name ;

    /** 数据类型 */
    private String sourceType ;

    /** 数据子类型 */
    private Integer childType ;

}
