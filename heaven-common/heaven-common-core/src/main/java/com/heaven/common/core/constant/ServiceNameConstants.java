package com.heaven.common.core.constant;

/**
 * 服务名称
 * 
 * @author Vicene
 */
public class ServiceNameConstants
{
    /**
     * 认证服务的serviceid
     */
    public static final String AUTH_SERVICE = "heaven-auth";

    /**
     * 系统模块的serviceid
     */
    public static final String SYSTEM_SERVICE = "heaven-system";

    /**
     * 文件服务的serviceid
     */
    public static final String FILE_SERVICE = "heaven-file";

    /**
     * 数据汇聚服务的serviceid
     */
    public static final String DATA_CONVERGE_SERVICE = "heaven-dataconverge";

    /**
     * 数据调度服务的serviceid
     */
    public static final String DATA_SCHEDULE_SERVICE = "heaven-dataschedule";

}
