package com.heaven.common.data.constant;

public class Constants {
    public static final String DATA_PROVIDER_DEFAULT = "DEFAULT";
    public static final String DATA_PROVIDER_JDBC = "JDBC";
    public static final String DATA_PROVIDER_FILE = "FILE";
    public static final String DATA_PROVIDER_HTTP = "HTTP";

    public static final String ENCRYPT_FLAG = "_encrypted_";

}
