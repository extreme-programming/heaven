package com.heaven.common.data.info;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataSourceInfo {

    /** url */
    private String url;

    /** 用户名 */
    private String username;

    /** 密码 */
    private String password;

    /** 驱动类 */
    private String driverName;

    /** 是否开启服务端聚合 */
    private Boolean enableServerAggregate;

    /** 是否允许未识别SQL执行 */
    private Boolean enableSpecialSQL;

    /** 是否定时同步数据库库表信息 */
    private Boolean enableSyncSchemas;

    /** 是否定时同步时间间隔（分钟） */
    private String syncInterval;

    /** 连接池参数 */
    private Map<String , Object> properties;
}
