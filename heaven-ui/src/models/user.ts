export interface stateProps {
  count: number
}

export default {
  namespace: 'user',
  state: {
    deptId: undefined, // 部门id传递
    currentNode: undefined
  },
  reducers: {
    setDeptId: (state: stateProps, { payload: deptId }: {payload: {deptId: number}}) => {
      return {
        ...state,
        deptId: deptId
      }
    },
    setCurrentNode: (state: stateProps, { payload: currentNode }: {payload: {currentNode: any}}) => {
      return {
        ...state,
        currentNode: currentNode
      }
    }
  },
}
