// @ts-ignore
/* eslint-disable */
export interface BusinessSectionPageList extends API.Result.AntDProPageResult {
  data: BusinessSectionItem []
}

export interface BusinessSectionResult extends API.Result.BaseResult {
  data: BusinessSectionItem
}

export interface BusinessSectionItem extends API.Result.BaseEntity {
  id: number
  name: string
  orderNum: string
  status: number
  remark: string
}
