import request from '@/services/ant-design-pro/request';
import {
  BusinessSectionPageList,
  BusinessSectionResult,
  BusinessSectionItem
} from "@/services/ant-design-pro/dataConverge/BusinessSection/result";

export async function getList (
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
)  {
  const data = await request<BusinessSectionPageList>('/api/dataconverge/businessSection/list', {
    method: 'GET',
    params: {
      ...params,
    },
    headers: {
      'Content-Type': 'application/json'
    },
    ...(options || {}),
  })
  return data;
}


export async function addBusinessSection(
  body:  BusinessSectionItem,
  options?: { [key: string]: any },
) {
  return request<BusinessSectionResult>('/api/dataconverge/businessSection', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    data: body,
    ...(options || {}),
  });
}

export async function removeBusinessSection(body: number[], options?: { [key: string]: any }) {
  const params = body.join(',');
  return request<BusinessSectionResult>(`/api/dataconverge/businessSection/${params}`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'DELETE',
    ...(options || {}),
  });
}

export async function getBusinessSection(body: number, options?: { [key: string]: any }) {
  const data = await request<BusinessSectionResult>(
    `/api/dataconverge/businessSection/${body}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      ...(options || {}),
    },
  );
  return data;
}

export async function editBusinessSection(
  body: BusinessSectionItem,
  options?: { [key: string]: any },
) {
  const result =  await request<BusinessSectionResult>('/api/dataconverge/businessSection', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PUT',
    data: body,
    ...(options || {}),
  });

  return result;
}
