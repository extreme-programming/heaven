import request from '@/services/ant-design-pro/request';
import {
  SourcePageList,
  SourceResult,
  SourceItem,
  TemplateResult, DriverInfoItemResult, ConnectionItem
} from "@/services/ant-design-pro/dataConverge/Source/result";
import {ResultCode, pageSize as constantPageSize, pageSize} from "@/pages/constants";
import {
  BusinessSectionItem,
  BusinessSectionPageList
} from "@/services/ant-design-pro/dataConverge/BusinessSection/result";
import {getList as getBusinessSectionList} from "@/services/ant-design-pro/dataConverge/BusinessSection/api";
import {message} from "antd";
import {WorkFlowResult} from "@/services/ant-design-pro/dataScheduler/WorkFlow/result";

export async function getSourceList (
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  const data = await request<SourcePageList>('/api/dataconverge/source/list', {
    method: 'GET',
    params: {
      ...params,
    },
    headers: {
      'Content-Type': 'application/json'
    },
    ...(options || {}),
  })
  return data;
}


export async function addSource(
  body: SourceItem,
  options?: { [key: string]: any },
) {
  return request<SourceResult>('/api/dataconverge/source', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    data: body,
    ...(options || {}),
  });
}

export async function removeSource(body: number[], options?: { [key: string]: any }) {
  const params = body.join(',');
  return request<SourceResult>(`/api/dataconverge/source/${params}`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'DELETE',
    ...(options || {}),
  });
}

export async function getSource(body: number, options?: { [key: string]: any }) {
  const data = await request<SourceResult>(
    `/api/system/dict/type/${body}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      ...(options || {}),
    },
  );
  return data;
}

export async function editSource(
  body: SourceItem,
  options?: { [key: string]: any },
) {
  return await request<SourceResult>('/api/system/dict/type', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PUT',
    data: body,
    ...(options || {}),
  });
}

export async function handleGetSource (params: {}, options?: { [key: string]: any }) {
  const data = await getSourceList(params , options);
  if(data.code = ResultCode.SUCCESS_CODE) {
    return data.data;
  } else {
    return [];
  }
}

export async function getDriverInfos() {
  const data = await request<DriverInfoItemResult>(
    `/api/dataconverge/source/getExtDriverInfos`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      }
    }
  );
  return data;
}

export async function handleGetBusinessSection () {
  const response: BusinessSectionPageList = await getBusinessSectionList();
  try {
    if(response.code === ResultCode.SUCCESS_CODE){
      if(response.rows){
        const data: BusinessSectionItem[] = response.rows;
        const result = [];
        for (const resultElement of data) {
          result.push({label:resultElement.name , value:resultElement.id})
        }
        return result;
      }
      return [];
    }
    return [];
  } catch (error) {
    message.error(response.msg);
    return [];
  }
}

export async function testConn(
  body: ConnectionItem,
  options?: { [key: string]: any },
) {
  return request<SourceResult>('/api/dataconverge/source/testConn', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    data: body,
    ...(options || {}),
  });
}

export async function getDetail(body: number, options?: { [key: string]: any }) {
  const data = await request<SourceResult>(
    `/api/dataconverge/source/detail/${body}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      ...(options || {}),
    },
  );
  return data;
}

