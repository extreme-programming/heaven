export interface SourcePageList extends API.Result.AntDProPageResult {
  data: SourceItem []
}

export interface SourceResult extends API.Result.BaseResult {
  data: SourceItem
}

export interface SourceItem extends API.Result.BaseEntity {
  id: number,
  name: string,
  config: string,
  sourceType: number,
  childType: string,
  orderNum: number,
  createTime: string,
  remark: string,
  status: number,
  businessSectionNamesStr: string
}

export interface TemplateResult extends API.Result.BaseResult {
  data: TemplateAttributes
}

export interface TemplateAttributes {
  name: string,
  displayName: string,
  required: boolean,
  encrypt: boolean,
  defaultValue: string,
  key: string,
  type: string,
  description: string,
  options: TemplateAttributesOptions[],
  children: any,
}

export interface TemplateAttributesOptions {
  driverClass: string,
  url: string,
  dbType: string,
}

export interface DriverInfoItemResult extends API.Result.BaseResult {
  data: DriverInfoItem[]
}

export interface DriverInfoItem{
  dbType: string
  version: string
  name: string
  driverClass: string
  literalQuote: string
  identifierQuote: string
  sqlDialect: string
  identifierEndQuote: string
  identifierEscapedQuote: string
  literalEndQuote: string
  adapterClass: string
  urlPrefix: string
  quoteIdentifiers: boolean
  supportSqlLimit: boolean
}

export interface SourceResult extends API.Result.BaseResult {
  data: true | false
}

export interface ConnectionItem{
  sourceId: number
  sourceType: string
  name: string
  properties: any
}
