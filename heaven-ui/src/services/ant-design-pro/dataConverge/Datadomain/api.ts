import request from '@/services/ant-design-pro/request';
import {
  BusinessDatadomainPageList,
  BusinessDatadomainItem,
  BusinessDatadomainResult
} from "./result";

export async function getList (
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
)  {
  const data = await request<BusinessDatadomainPageList>('/api/dataconverge/datadomain/list', {
    method: 'GET',
    params: {
      ...params,
    },
    headers: {
      'Content-Type': 'application/json'
    },
    ...(options || {}),
  })
  return data;
}


export async function add(
  body:  BusinessDatadomainItem,
  options?: { [key: string]: any },
) {
  return request<BusinessDatadomainResult>('/api/dataconverge/datadomain', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    data: body,
    ...(options || {}),
  });
}

export async function remove(body: number[], options?: { [key: string]: any }) {
  const params = body.join(',');
  return request<BusinessDatadomainResult>(`/api/dataconverge/datadomain/${params}`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'DELETE',
    ...(options || {}),
  });
}

export async function get(body: number, options?: { [key: string]: any }) {
  const data = await request<BusinessDatadomainResult>(
    `/api/dataconverge/datadomain/${body}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      ...(options || {}),
    },
  );
  return data;
}

export async function edit(
  body: BusinessDatadomainItem,
  options?: { [key: string]: any },
) {
  const result =  await request<BusinessDatadomainResult>('/api/dataconverge/datadomain', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PUT',
    data: body,
    ...(options || {}),
  });

  return result;
}
