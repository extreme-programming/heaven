// @ts-ignore
/* eslint-disable */
export interface BusinessDatadomainPageList extends API.Result.AntDProPageResult {
  data: BusinessDatadomainItem []
}

export interface BusinessDatadomainResult extends API.Result.BaseResult {
  data: BusinessDatadomainItem
}

export interface BusinessDatadomainItem extends API.Result.BaseEntity {
  id: number
  sectionId: number
  name: string
  orderNum: string
  status: number
  remark: string
}
