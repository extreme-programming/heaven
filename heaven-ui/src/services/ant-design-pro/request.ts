/** Request 网络请求工具 更详细的 api 文档: https://github.com/umijs/umi-request */
import { extend } from 'umi-request';
import { history } from 'umi';
import {getLocale} from "@@/plugin-locale/localeExports";
import {Constants, ResultCode} from "@/pages/constants";

const downloadLinks: string[] = ['/gen/batchGenCode'] ;

function isContainDownloadLink(url: string): boolean {
  let isContain = false;
  for (const urlElement of downloadLinks) {
    if(url.indexOf(urlElement) > -1){
      isContain = true;
      break;
    }
  }
  return isContain;
}

const errorHandler = (error: any) => {
  console.log('error ==',error)
  const { response } = error;
  return response;
};

/** 配置request请求时的默认参数 */
const request = extend({
  errorHandler,
  // 默认错误处理
  credentials: 'include', // 默认请求是否带上cookie
});

// request拦截器, 改变url 或 options.
// @ts-ignore
request.interceptors.request.use(async (url, options) => {
  if( url === '/api/code'){
    return { url , options };
  }

  const accessToken = localStorage.getItem(Constants.TOKEN);
  const locale = getLocale();
  if (accessToken) {
    options.headers = {
      ...options.headers,
      // 'Accept': 'application/json',
      'Authorization': `Bearer ${accessToken}`,
      'locale': locale
    };
  }
  const { params } = options;
  // 分页字段修改
  if (params) {
    params['pageNum'] = params['current'];
  }
  return { url , options };
})

// response拦截器, 处理response
request.interceptors.response.use(async (response: Response) => {
  const url = new URL(response.url);
  //判断是否是下载api
  if(isContainDownloadLink(url.href)){
    response.blob().then((data) =>{
      const blob = new Blob([data]);
      const fileName = "heaven.zip";
      const selfURL = window[window.webkitURL ? 'webkitURL' : 'URL'];
      let elink = document.createElement('a');
      if ('download' in elink) {
        elink.download = fileName;
        elink.style.display = 'none';
        elink.href = selfURL['createObjectURL'](blob);
        document.body.appendChild(elink);
        // 触发链接
        elink.click();
        selfURL.revokeObjectURL(elink.href);
        document.body.removeChild(elink)
      } else {
        navigator.msSaveBlob(blob, fileName);
      }
    });
  }

  const result = await response.clone().json();
  const accessToken = localStorage.getItem(Constants.TOKEN);
  if(!accessToken){
    history.push('/user/login');
  }

  const { rows , total , code } = result
  const searchParams = url.searchParams;
  if(code === ResultCode.SUCCESS_CODE) {
    if(searchParams.get("current") && rows) {
      return {
        data: rows,
        total,
        success: true,
        current: Number(searchParams.get("current")),
        pageSize: Number(searchParams.get("pageSize"))
      }
    }
  }

  return result;
});

export default request;
