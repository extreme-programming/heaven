import request from '@/services/ant-design-pro/request';
import {
  WorkFlowPageList,
  WorkFlowResult,
  WorkFlowItem
} from "@/services/ant-design-pro/dataScheduler/WorkFlow/result";

export async function getList (
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  const data = await request<WorkFlowPageList>('/api/dataschedule/processDefinition/list', {
    method: 'GET',
    params: {
      ...params,
    },
    headers: {
      'Content-Type': 'application/json'
    },
    ...(options || {}),
  })
  return data;
}

export async function add(
  body: any,
  options?: { [key: string]: any },
) {
  return request<WorkFlowResult>('/api/dataschedule/processDefinition', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    data: body,
    ...(options || {}),
  });
}

export async function remove(body: number[], options?: { [key: string]: any }) {
  const params = body.join(',');
  return request<WorkFlowResult>(`/api/dataschedule/processDefinition/${params}`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'DELETE',
    ...(options || {}),
  });
}

export async function get(body: number, options?: { [key: string]: any }) {
  const data = await request<WorkFlowResult>(
    `/api/dataschedule/processDefinition/${body}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      ...(options || {}),
    },
  );
  return data;
}



