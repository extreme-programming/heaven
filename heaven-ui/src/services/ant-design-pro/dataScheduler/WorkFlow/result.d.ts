export interface WorkFlowPageList extends API.Result.AntDProPageResult {
  data: WorkFlowItem []
}

export interface WorkFlowResult extends API.Result.BaseResult {
  data: WorkFlowItem
}

export interface WorkFlowItem extends API.Result.BaseEntity {
  id: number;
  name: string;
  version: string;
  releaseState: number;
  globalParams: string;
  flag: number;
  locations: string;
  warningGroupId: string;
  timeout: number;
  executionType: string
  createBy: number;
  createTime: string;
  updateBy: string;
  updateTime: number;
  remark: string;
  graphJson: string;
}
