export interface GenTableItemPageList extends API.Result.AntDProPageResult {
  data: GenTableItem []
}

export interface GetInfoResult extends API.Result.BaseResult{
  data:GetInfoData
}

export interface GetInfoData{
  tables: GenTableItem[],
  rows: GenColumnItem[],
  info: GenTableItem
}

export interface GenTableItemResult extends API.Result.BaseResult {
  data: GenTableItem
}

export interface GenTableItem extends API.Result.BaseEntity {
  tableId: number,
  tableName: string,
  tableComment: string,
  subTableName: string,
  subTableFkName: string,
  className: string,
  tplCategory: string,
  packageName: string,
  moduleName: string,
  businessName: string,
  functionName: string,
  functionAuthor: string,
  genType: number,
  genPath: string,
  pkColumn: string,
  subTable: string,
  param: any,
  options: any,
  columns: GenColumnItem[]
}

export interface GenColumnItem extends API.Result.BaseEntity {
  columnId: number,
  tableId: number,
  columnName: string,
  columnComment: string,
  columnType: string,
  javaType: string,
  javaField: string,
  isPk: boolean,
  isIncrement: boolean,
  isRequired: boolean,
  isInsert: boolean,
  isEdit: boolean,
  isList: boolean,
  isQuery: boolean,
  queryType: string,
  htmlType: string,
  dictType: string,
  sort: number,
  list: string,
  required: string,
  pk: boolean,
  insert: boolean,
  edit: boolean,
  usableColumn: boolean,
  superColumn: boolean,
  increment: boolean,
  query: boolean,
  capJavaField: string
}
