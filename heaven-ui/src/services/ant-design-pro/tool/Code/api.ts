import request from '@/services/ant-design-pro/request';
import {
  GenColumnItem,
  GenTableItem,
  GenTableItemPageList,
  GenTableItemResult, GetInfoResult
} from './result';

export async function removeGenTable(body: number[], options?: { [key: string]: any }) {
  const params = body.join(',');
  return request<GenTableItemResult>(`/api/code/gen/${params}`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'DELETE',
    ...(options || {}),
  });
}

export async function importGenTable(
  body: string
) {
  const response = await request<GenTableItemResult>(`/api/code/gen/importTable?tables=${body}`, {
    method: 'POST'
  });
  return response;
}

export async function getList (
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  const data = await request<GenTableItemPageList>('/api/code/gen/list', {
    method: 'GET',
    params: {
      ...params,
    },
    headers: {
      'Content-Type': 'application/json'
    },
    ...(options || {}),
  })
  return data;
}

export async function getDbList (
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  const data = await request<GenTableItemPageList>('/api/code/gen/db/list', {
    method: 'GET',
    params: {
      ...params,
    },
    headers: {
      'Content-Type': 'application/json'
    },
    ...(options || {}),
  })
  return data;
}


export async function getInfo(body: number, options?: { [key: string]: any }) {
  const data = await request<GetInfoResult>(
    `/api/code/gen/${body}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      ...(options || {}),
    },
  );
  return data;
}

export async function syncGenTable(body: string, options?: { [key: string]: any }) {
  const data = await request<GenTableItemResult>(
    `/api/code/gen/synchDb/${body}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      ...(options || {}),
    },
  );
  return data;
}

export async function batchGenCode(body: string, options?: { [key: string]: any }) {
  const data = await request(
    `/api/code/gen/batchGenCode?tables=${body}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      responseType: 'blob',
      ...(options || {}),
    },
  );
  return data;
}

export async function previewGenCode(body: number, options?: { [key: string]: any }) {
  const data = await request(
    `/api/code/gen/preview/${body}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      ...(options || {}),
    },
  );
  return data;
}

export async function handleBasic(
  body: GenTableItem,
  options?: { [key: string]: any },
) {
  const result = await request<GetInfoResult>('/api/code/gen', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PUT',
    data: body,
    ...(options || {}),
  });
  return result;
}

export async function handleBasicColumn(
  body: GenColumnItem[],
  options?: { [key: string]: any },
) {
  // const bodyWapper = {genColumnVo:{list:body}}
  const result = await request<DeptItemResult>('/api/code/gen/column', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PUT',
    data: body,
    ...(options || {}),
  });
  return result;
}
