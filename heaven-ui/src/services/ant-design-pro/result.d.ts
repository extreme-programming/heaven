declare namespace API.Result {
  export interface BaseResult {
    code: number,
    msg: string
  }

  export interface BaseEntity {
    searchValue: string
    createBy: string,
    createTime: string,
    updateBy: string,
    updateTime: string,
    remark: string,
    params: any,
  }

  export interface AntDProPageResult {
    total?: number;
    success?: boolean;
    current?: number;
    pageSize?: number;
    pageNum?: number;
  }
}
