declare namespace API.Param {
  export interface BasePageParam{
    current: number;
    pageNum: number;
    pageSize: number;
  }
}
