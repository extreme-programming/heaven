export interface LogininforItemPageList extends API.Result.AntDProPageResult {
  data: LogininforItem []
}

export interface LogininforItemResult extends API.Result.BaseResult {
  data: LogininforItem[]
}

export interface LogininforItem extends API.Result.BaseEntity {
  infoId: number,
  userName: string,
  status: number,
  ipaddr: string,
  loginLocation: string,
  browser: string,
  os: string,
  msg: string,
  accessTime: string
}

