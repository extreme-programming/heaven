import request from '@/services/ant-design-pro/request';
import {LogininforItemPageList, LogininforItemResult} from './result';

export async function removeLogininfor(body: number[], options?: { [key: string]: any }) {
  const params = body.join(',');
  return request<LogininforItemResult>(`/api/system/logininfor/${params}`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'DELETE',
    ...(options || {}),
  });
}

export async function getList (
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  const data = await request<LogininforItemPageList>('/api/system/logininfor/list', {
    method: 'GET',
    params: {
      ...params,
    },
    headers: {
      'Content-Type': 'application/json'
    },
    ...(options || {}),
  })
  return data;
}

