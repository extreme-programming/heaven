export interface OperateLogItemPageList extends API.Result.AntDProPageResult {
  data: OperateLogItem []
}

export interface OperateLogItemResult extends API.Result.BaseResult {
  data: OperateLogItem
}

export interface OperateLogItem extends API.Result.BaseEntity {
  operId: number,
  title: string,
  businessType: number,
  businessTypes: string,
  method: string,
  requestMethod: string,
  operatorType: number,
  operName: string,
  deptName: string,
  operUrl: string,
  operIp: string,
  operLocation: string,
  operParam: {},
  jsonResult: string,
  status: number,
  errorMsg: string,
  operTime: string
}
