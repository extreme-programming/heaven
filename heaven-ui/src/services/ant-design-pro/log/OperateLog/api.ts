import request from '@/services/ant-design-pro/request';
import {OperateLogItemPageList, OperateLogItemResult} from './result';

export async function removeOperateLog(body: number[], options?: { [key: string]: any }) {
  const params = body.join(',');
  return request<OperateLogItemResult>(`/api/system/operlog/${params}`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'DELETE',
    ...(options || {}),
  });
}

export async function getList (
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  const data = await request<OperateLogItemPageList>('/api/system/operlog/list', {
    method: 'GET',
    params: {
      ...params,
    },
    headers: {
      'Content-Type': 'application/json'
    },
    ...(options || {}),
  })
  return data;
}

export async function getOperateLog(body: number, options?: { [key: string]: any }) {
  const data = await request<OperateLogItemResult>(
    `/api/system/operlog/${body}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      ...(options || {}),
    },
  );
  return data;
}

