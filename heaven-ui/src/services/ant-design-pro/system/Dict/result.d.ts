
export interface DictItemPageList extends API.Result.AntDProPageResult {
  data: DictItem []
}

export interface DictItemResult extends API.Result.BaseResult {
  data: DictItem
}

export interface DictDataItemResult extends API.Result.BaseResult {
  data: DictDataItem
}

export interface DictItem extends API.Result.BaseEntity {
  dictId: number,
  dictName: string,
  dictType: string,
  status: number
}

export interface DictDataItem extends API.Result.BaseEntity {
  dictCode: number,
  dictSort: number,
  dictLabel: string,
  dictValue: number,
  dictType: string
}

