import request from '@/services/ant-design-pro/request';
import {
  DictDataItemResult,
  DictItem,
  DictItemPageList,
  DictItemResult
} from "@/services/ant-design-pro/system/Dict/result";
import {ResultCode} from "@/pages/constants";

export async function getDictTypeList (
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  const data = await request<DictItemPageList>('/api/system/dict/type/list', {
    method: 'GET',
    params: {
      ...params,
    },
    headers: {
      'Content-Type': 'application/json'
    },
    ...(options || {}),
  })
  //setListData(data.data)
  return data;
}


export async function addDictType(
  body: DictItem,
  options?: { [key: string]: any },
) {
  return request<DictItemResult>('/api/system/dict/type', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    data: body,
    ...(options || {}),
  });
}

export async function removeDictType(body: number[], options?: { [key: string]: any }) {
  const params = body.join(',');
  return request<DictItemResult>(`/api/system/dict/type/${params}`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'DELETE',
    ...(options || {}),
  });
}

export async function getDictType(body: number, options?: { [key: string]: any }) {
  const data = await request<DictItemResult>(
    `/api/system/dict/type/${body}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      ...(options || {}),
    },
  );
  return data;
}

export async function editDictType(
  body: DictItem,
  options?: { [key: string]: any },
) {
  return await request<DictItemResult>('/api/system/dict/type', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PUT',
    data: body,
    ...(options || {}),
  });
}

export async function getDictData(body: string, options?: { [key: string]: any }) {
  const data = await request<DictDataItemResult>(
    `/api/system/dict/data/type/${body}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      ...(options || {}),
    },
  );
  return data;
}

export async function handleDictData (sourceType) {
  const result = await getDictData(sourceType);
  try {
    if(result.code === ResultCode.SUCCESS_CODE){
      const dataValue = {};
      result.data.map((item)=>{
        const key = item.dictValue;
        const val = item.dictLabel;
        dataValue[key] = val;
      });
      return dataValue;
    }
    return [];
  } catch (error) {
    return [];
  }
};
