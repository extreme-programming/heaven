export interface CodeResult extends API.Result.BaseResult{
  captchaEnabled: boolean;
  uuid: string;
  img: string;
}

export interface LoginResult extends API.Result.BaseResult{
  data: LoginResultData;
}

export interface LoginResultData {
  access_token: string
  expires_in: number
}
