import { CodeResult , LoginResult} from './result.d';
import { LoginParam } from './param.d';
import request from '../../request';

export async function getCode(options?: { [key: string]: any }) {
  return request<CodeResult>('/api/code', {
    method: 'GET',
    ...(options || {}),
  });
}

/** 登录接口 POST /api/login/account */
export async function login(body: LoginParam, options?: { [key: string]: any }) {
  return request<LoginResult>('/api/auth/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

