export interface LoginParam {
  username?: string;
  password?: string;
  autoLogin?: boolean;
  type?: string;
  uuid?: string;
  code?: string;
}
