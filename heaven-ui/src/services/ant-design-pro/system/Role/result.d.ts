export interface RoleItemPageList extends API.Result.AntDProPageResult {
  data: RoleItem []
}

export interface RoleItemResult extends API.Result.BaseResult {
  data: RoleItem
}

export interface RoleItem extends API.Result.BaseEntity {
  roleId: number,
  roleName: string,
  roleKey: string,
  roleSort: number,
  dataScope: number
  menuCheckStrictly: boolean,
  deptCheckStrictly: boolean,
  status: boolean,
  delFlag: boolean,
  flag: boolean,
  menuIds: number[],
  deptIds: number[],
  permissions: number[],
  admin: boolean
}

