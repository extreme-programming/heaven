import request from '@/services/ant-design-pro/request';
import {RoleItem, RoleItemPageList, RoleItemResult} from "@/services/ant-design-pro/system/Role/result";

export async function getRoleList(
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  const data = await request<RoleItemPageList>('/api/system/role/list', {
    method: 'GET',
    params: {
      ...params,
    },
    headers: {
      'Content-Type': 'application/json'
    },
    ...(options || {}),
  })
  return data;
}

export async function addRole(
  body: RoleItem,
  options?: { [key: string]: any },
) {
  return request<RoleItemResult>('/api/system/role', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    data: body,
    ...(options || {}),
  });
}

export async function removeRole(body: number[], options?: { [key: string]: any }) {
  const params = body.join(',');
  return request<RoleItemResult>(`/api/system/role/${params}`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'DELETE',
    ...(options || {}),
  });
}

export async function getRole(body: number, options?: { [key: string]: any }) {
  const data = await request<RoleItemResult>(
    `/api/system/role/${body}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      ...(options || {}),
    },
  );
  return data;
}

export async function editRole(
  body: RoleItem,
  options?: { [key: string]: any },
) {
  return await request<RoleItemResult>('/api/system/role', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PUT',
    data: body,
    ...(options || {}),
  });
}
