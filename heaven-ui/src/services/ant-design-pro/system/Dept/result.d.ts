
export interface DeptItemPageList extends API.Result.AntDProPageResult {
  data: DeptItem []
}

export interface DeptItemResult extends API.Result.BaseResult {
  data: DeptItem[]
}

export interface DeptItem extends API.Result.BaseEntity {
  key: string,
  deptId: number,
  parentId: number,
  ancestors: string,
  deptName: string,
  orderNum: number,
  leader: string,
  phone: string,
  email: string,
  status: string,
  delFlag: string,
  parentName: string,
  children: DeptItem []
}
