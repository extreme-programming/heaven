import request from '@/services/ant-design-pro/request';
import { DeptItem , DeptItemResult } from './result';

export async function addDept(
  body: DeptItem,
  options?: { [key: string]: any },
) {
  return request<DeptItemResult>('/api/system/dept', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    data: body,
    ...(options || {}),
  });
}

export async function removeDept(body: number[], options?: { [key: string]: any }) {
  const params = body.join(',');
  return request<DeptItemResult>(`/api/system/dept/${params}`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'DELETE',
    ...(options || {}),
  });
}

export async function getDept(deptId: number, options?: { [key: string]: any }) {
  const data = await request<DeptItemResult>(
    `/api/system/dept/${deptId}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      ...(options || {}),
    },
  );
  return data;
}

export async function editDept(
  body: DeptItem,
  options?: { [key: string]: any },
) {
  const result = await request<DeptItemResult>('/api/system/dept', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PUT',
    data: body,
    ...(options || {}),
  });
  return result;
}

export const getTreeList = async () => {
  const response = await request<DeptItemResult>('/api/system/dept/list', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  return response;
}

export function handleTree(data, id, name, parentId, children ) {
  let config = {
    id: id || 'id',
    parentId: parentId || 'parentId',
    childrenList: children || 'children'
  };

  let childrenListMap = {};
  let nodeIds = {};
  let tree = [];

  for (let d of data) {
    let parentId = d[config.parentId];
    if (childrenListMap[parentId] == null) {
      childrenListMap[parentId] = [];
    }
    d['key'] = d[config.id]
    if(name){
      d['title'] = d[name]
    }
    d['value'] = d[id]
    nodeIds[d[config.id]] = d;
    childrenListMap[parentId].push(d);
  }

  for (let d of data) {
    let parentId = d[config.parentId];
    if (nodeIds[parentId] == null) {
      tree.push(d);
    }
  }

  for (let t of tree) {
    adaptToChildrenList(t);
  }

  function adaptToChildrenList(o) {
    if (childrenListMap[o[config.id]] !== null) {
      o[config.childrenList] = childrenListMap[o[config.id]];
    }
    if (o[config.childrenList]) {
      for (let c of o[config.childrenList]) {
        adaptToChildrenList(c);
      }
    }
  }
  return tree;
}
