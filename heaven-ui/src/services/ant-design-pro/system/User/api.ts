import {CurrentUserResult, User, UserPageList, UserResult} from "@/services/ant-design-pro/system/User/result";
import request from '../../request';


/** 获取当前的用户 GET /api/currentUser */
export function queryCurrentUser(options?: { [key: string]: any }) {
  return request<CurrentUserResult>('/api/system/user/getInfo', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
    ...(options || {}),
  });
}

export async function addUser(
  body: User,
  options?: { [key: string]: any },
) {
  return request<UserResult>('/api/system/user', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    data: body,
    ...(options || {}),
  });
}

export async function removeUser(body: number[], options?: { [key: string]: any }) {
  const params = body.join(',');
  return request<UserResult>(`/api/system/user/${params}`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'DELETE',
    ...(options || {}),
  });
}

export async function getUser(body: number, options?: { [key: string]: any }) {
  const data = await request<UserResult>(
    `/api/system/user/${body}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      ...(options || {}),
    },
  );
  return data;
}

export async function editUser(
  body: User,
  options?: { [key: string]: any },
) {
  const result = await request<UserResult>('/api/system/user', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PUT',
    data: body,
    ...(options || {}),
  });
  return result;
}

export async function getList  (
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
) {
  const data = await request<UserPageList>('/api/system/user/list', {
    method: 'GET',
    params: {
      ...params,
    },
    headers: {
      'Content-Type': 'application/json'
    },
    ...(options || {}),
  })
  return data;
}
