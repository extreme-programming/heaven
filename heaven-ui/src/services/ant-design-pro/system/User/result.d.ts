import {PostItem} from "@/services/ant-design-pro/system/Post/result";

export interface CurrentUserResult extends API.Result.BaseResult {
  data: CurrentUserDataResult
}

export interface UserPageList extends API.Result.AntDProPageResult {
  data: User[]
}

export interface UserResult extends API.Result.BaseResult {
  data: User
}

export interface CurrentUserDataResult {
  permissions: string [],
  roles: string [],
  user: User,
}

export interface User {
  searchValue:   string
  createBy:  string
  createTime:  string
  updateBy:  string
  updateTime:  string
  remark:  string
  params:  string
  userId:  number
  deptId:  number
  userName:  string
  nickName:  string
  email:  string
  phonenumber:  string
  sex:  string
  avatar:  string
  password:  string
  status:  string
  delFlag:  string
  loginIp:  string
  loginDate:  string
  dept:  Dept
  roles:  Role[]
  roleIds:  number[]
  postIds:  number[]
  roleId:  number
  admin:  boolean
  posts: PostItem[]
};

export interface Dept {
  searchValue:   string
  createBy:  string
  createTime:  string
  updateBy:  string
  updateTime:  string
  remark:  string
  params:  string
  deptId:  number
  parentId:  number
  ancestors:  string
  deptName:  string
  orderNum:  number
  leader:  string
  phone:  number
  email:  string
  status:  string
  delFlag:  string
  parentName:  string
  children:  Dept[]
};

export interface Role {
  searchValue:  string
  createBy: string
  createTime: string
  updateBy: string
  updateTime: string
  remark: string
  params: string
  roldId: number
  roleName: string
  roleKey: string
  roleSort: number
  dataScope: string
  menuCheckStrictly: boolean
  deptCheckStrictly: boolean
  status: string
  delFlag: string
  flag: boolean
  menuIds: number[]
  deptIds: number[]
  permissions: string[]
  admin: boolean
};
