import request from '@/services/ant-design-pro/request';
import {MenuItem, MenuItemForRoleResult, MenuItemResult} from "@/services/ant-design-pro/system/Menu/result";

export async function addMenu(
  body: MenuItem,
  options?: { [key: string]: any },
) {
  return request<MenuItemResult>('/api/system/menu', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    data: body,
    ...(options || {}),
  });
}

export async function removeMenu(body: number[], options?: { [key: string]: any }) {
  const param = body[0];
  return request<MenuItemResult>(`/api/system/menu/${param}`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'DELETE',
    ...(options || {}),
  });
}

export async function getMenu(menuId: number, options?: { [key: string]: any }) {
  const data = await request<MenuItemResult>(
    `/api/system/menu/${menuId}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      ...(options || {}),
    },
  );
  return data;
}

export async function editMenu(
  body: MenuItem,
  options?: { [key: string]: any },
) {
  const result = await request<MenuItemResult>('/api/system/menu', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PUT',
    data: body,
    ...(options || {}),
  });

  return result;
}

export async function getMenus(options?: { [key: string]: any }) {

  const userId = options.userId;
  return request(`/api/system/menu/getRouters/${userId}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
    ...(options || {}),
  });
}

export const getTreeList = async () => {
  const response = await request<MenuItemResult>('/api/system/menu/list', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  return response;
}

export async function getTreeListForRole(options?: { [key: string]: any }){
  // @ts-ignore
  const roleId = options.roleId;
  const response = await request<MenuItemForRoleResult>(`/api/system/menu/roleMenuTreeselect/${roleId}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
    ...(options || {}),
  });
  return response;
}

