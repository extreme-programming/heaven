
export interface MenuItemPageList extends API.Result.AntDProPageResult {
  data: MenuItem []
}

export interface MenuItemResult extends API.Result.BaseResult {
  data: MenuItem []
}

export interface MenuItemForRoleResult extends API.Result.BaseResult {
  data: MenuItemForRoleData
}

export interface MenuItem extends API.Result.BaseEntity {
  menuId: number,
  parentId: number,
  parentName: string,
  ancestors: string,
  menuName: string,
  orderNum: number,
  path: string,
  component: string,
  query: string,
  isFrame: string,
  isCache: string,
  menuType: string,
  visible: string,
  status: string,
  perms: string,
  icon: string,
  children: MenuItem []
}

export interface MenuItemForRoleData {
  menus: MenuItemForRole [],
  checkedKeys: string[]
}

export interface MenuItemForRole {
  id: number,
  key: string,
  name: string,
  title: string,
  value: string
}


