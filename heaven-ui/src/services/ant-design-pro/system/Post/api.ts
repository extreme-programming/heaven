import request from '@/services/ant-design-pro/request';
import {PostItem, PostItemPageList, PostItemResult} from "@/services/ant-design-pro/system/Post/result";

export async function getPostList (
  params: {
    // query
    /** 当前的页码 */
    current?: number;
    /** 页面的容量 */
    pageSize?: number;
  },
  options?: { [key: string]: any },
)  {
  const data = await request<PostItemPageList>('/api/system/post/list', {
    method: 'GET',
    params: {
      ...params,
    },
    headers: {
      'Content-Type': 'application/json'
    },
    ...(options || {}),
  })
  return data;
}


export async function addPost(
  body:  PostItem,
  options?: { [key: string]: any },
) {
  return request<PostItemResult>('/api/system/post', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'POST',
    data: body,
    ...(options || {}),
  });
}

export async function removePost(body: number[], options?: { [key: string]: any }) {
  const params = body.join(',');
  return request<PostItemResult>(`/api/system/post/${params}`, {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'DELETE',
    ...(options || {}),
  });
}

export async function getPost(body: number, options?: { [key: string]: any }) {
  const data = await request<PostItemResult>(
    `/api/system/post/${body}`,
    {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
      ...(options || {}),
    },
  );
  return data;
}

export async function editPost(
  body: PostItem,
  options?: { [key: string]: any },
) {
  const result =  await request<PostItemResult>('/api/system/post', {
    headers: {
      'Content-Type': 'application/json',
    },
    method: 'PUT',
    data: body,
    ...(options || {}),
  });

  return result;
}
