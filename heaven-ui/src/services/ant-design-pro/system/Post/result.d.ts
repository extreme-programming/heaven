// @ts-ignore
/* eslint-disable */
export interface PostItemPageList extends API.Result.AntDProPageResult {
  data: PostItem []
}

export interface PostItemResult extends API.Result.BaseResult {
  data: PostItem
}

export interface PostItem extends API.Result.BaseEntity {
  postId: number,
  postCode: string,
  postName: string,
  postSort: string,
  status: number
}
