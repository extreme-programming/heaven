import Footer from '@/components/Footer';
import RightContent from '@/components/RightContent';
import type { Settings as LayoutSettings } from '@ant-design/pro-components';
import { PageLoading, SettingDrawer } from '@ant-design/pro-components';
import { MenuDataItem } from '@umijs/route-utils';
import type { RunTimeLayoutConfig } from 'umi';
import { history, Link } from 'umi';
import defaultSettings from '../config/defaultSettings';
import { queryCurrentUser } from './services/ant-design-pro/system/User/api';

import {
  ToolFilled,
  UserAddOutlined,
  UsergroupAddOutlined,
  MenuOutlined,
  ClusterOutlined,
  SolutionOutlined,
  ReadOutlined,
  FileProtectOutlined,
  FileDoneOutlined,
  FundFilled,
  SmileFilled,
  SettingFilled,
  FileTextFilled,
  ApiFilled,
  BookFilled,
  ScheduleFilled
} from "@ant-design/icons";
// @ts-ignore
import {ProLayoutProps} from "@ant-design/pro-layout";
import {CurrentUserDataResult} from "@/services/ant-design-pro/system/User/result";
import { ResultCode } from './pages/constants';
import {getMenus} from "@/services/ant-design-pro/system/Menu/api";
const isDev = process.env.NODE_ENV === 'development';
const loginPath = '/user/login';

/** 获取用户信息比较慢的时候会展示一个 loading */
export const initialStateConfig = {
  loading: <PageLoading />,
};

const IconMap = {
  welcome: <SmileFilled />,
  system: <SettingFilled />,
  user: <UserAddOutlined />,
  role: <UsergroupAddOutlined />,
  menu: <MenuOutlined />,
  dept: <ClusterOutlined />,
  post: <SolutionOutlined />,
  dict: <ReadOutlined />,
  log: <FileTextFilled />,
  operateLog: <FileProtectOutlined />,
  loginLog: <FileDoneOutlined />,
  dataConverge: <FundFilled />,
  tool: <ToolFilled />,
  book: <BookFilled />,
  dataScheduler: <ScheduleFilled />
}

const loopMenuItem = (menus: MenuDataItem[]): MenuDataItem[] =>
  menus.map(({icon, children, ...item}) => ({
    ...item,
    icon: icon && IconMap[icon as string],
    children: children && loopMenuItem(children)
  }));
// const loopMenuItem = (menus: MenuDataItem[] , isChild: boolean): MenuDataItem[] => {
//   for (const menu of menus) {
//     if(isChild){
//       debugger;
//       menu.icon = React.createElement(menu?.icon?.type);
//       menu.children = menu.children && loopMenuItem(menu.children , true)
//     }else{
//       menu.icon =  menu.icon && IconMap[menu.icon as string];
//       menu.children = menu.children && loopMenuItem(menu.children , true)
//     }
//   }
//   return menus;
// }

/**
 * @see  https://umijs.org/zh-CN/plugins/plugin-initial-state
 * */
export async function getInitialState(): Promise<{
  settings?: Partial<LayoutSettings>;
  currentUser?: CurrentUserDataResult;
  loading?: boolean;
  routes?: MenuDataItem[];
  fetchUserInfo?: () => Promise<CurrentUserDataResult | undefined>;
}> {
  const fetchUserInfo = async () => {
    try {
      const res = await queryCurrentUser();
      if(res.code === ResultCode.SUCCESS_CODE){
        return res.data;
      }
    } catch (error) {
      history.push(loginPath);
    }
    return undefined;
  };
  // 如果不是登录页面，执行
  if (history.location.pathname !== loginPath) {
    const currentUser = await fetchUserInfo();
    return {
      fetchUserInfo,
      currentUser,
      settings: defaultSettings,
    };
  }
  return {
    fetchUserInfo,
    settings: defaultSettings,
  };
}

// ProLayout 支持的api https://procomponents.ant.design/components/layout
export const layout: RunTimeLayoutConfig = ({ initialState, setInitialState }) => {
  // @ts-ignore
  return {
    rightContentRender: () => <RightContent />,
    disableContentMargin: false,
    waterMarkProps: {
      content: initialState?.currentUser?.user.userName ,
    },
    footerRender: () => <Footer />,
    onPageChange: () => {
      const { location } = history;
      // 如果没有登录，重定向到 login
      if (!initialState?.currentUser && location.pathname !== loginPath) {
        history.push(loginPath);
      }
    },
    links: isDev
      ? [
        <Link key="openapi" to="https://gitee.com/extreme-programming/heaven" target="_blank">
          <ApiFilled />
          <span>仓库地址</span>
        </Link>,
        <Link to="/~docs" key="docs">
          <BookFilled />
          <span>组件文档</span>
        </Link>,
      ]
      : [],
    menuHeaderRender: undefined,
    menu: {
      // 每当 initialState?.currentUser?.userid 发生修改时重新执行 request
      params: initialState.currentUser,
      request: async (params, defaultMenuData) => {
        if(params && params.user){
          const menuData = await getMenus({userId:params.user.userId});
          return loopMenuItem(menuData.data , false);
        }
      }
    },
    // 自定义 403 页面
    // unAccessible: <div>unAccessible</div>,
    // 增加一个 loading 的状态
    childrenRender: (children: JSX.Element, props: ProLayoutProps) => {
      // if (initialState?.loading) return <PageLoading />;
      return (
        <>
          {children}
          {!props.location?.pathname?.includes('/login') && (
            <SettingDrawer
              disableUrlParams
              enableDarkTheme
              settings={initialState?.settings}
              onSettingChange={(settings) => {
                setInitialState((preInitialState) => ({
                  ...preInitialState,
                  settings,
                }));
              }}
            />
          )}
        </>
      );
    },
    ...initialState?.settings,
  };
};
