import { SlidersOutlined , RadarChartOutlined , AreaChartOutlined , PieChartOutlined ,DotChartOutlined , BarChartOutlined} from '@ant-design/icons';
import { DefaultFooter } from '@ant-design/pro-components';
import { useIntl } from 'umi';

const Footer: React.FC = () => {
  const intl = useIntl();
  const defaultMessage = intl.formatMessage({
    id: 'app.copyright.produced',
    defaultMessage: '极限编程出品',
  });

  const currentYear = new Date().getFullYear();

  return (
    <DefaultFooter
      copyright={`${currentYear} ${defaultMessage}`}
      links={[
        {
          key: 'barchart',
          title: <BarChartOutlined />,
          href: '#',
          blankTarget: true,
        }
        ,
        {
          key: 'radarchart',
          title: <RadarChartOutlined />,
          href: '#',
          blankTarget: true,
        }
        ,
        {
          key: 'piechart',
          title: <PieChartOutlined />,
          href: '#',
          blankTarget: true,
        }
        ,
        {
          key: 'sliderout',
          title: <SlidersOutlined />,
          href: '#',
          blankTarget: true,
        },
        {
          key: 'areachart',
          title: <AreaChartOutlined />,
          href: '#',
          blankTarget: true,
        },
        {
          key: 'dotchart',
          title: <DotChartOutlined />,
          href: '#',
          blankTarget: true,
        }
      ]}
    />
  );
};

export default Footer;
