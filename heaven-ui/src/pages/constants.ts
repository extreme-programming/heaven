export enum ResultCode {
  /**
   * 成功
   */
  SUCCESS_CODE = 200,

  /**
   * 校验失败
   */
  FAIL_DATA_VALIDATE = 401,

  /**
   * 服务异常
   */
  ERROR_CODE = 500,
}

export const pageSize = 10000;


export enum Constants {
  TOKEN = 'token',
  PROCESS_TASK_TYPE = 'process_task_type',
  // @ts-ignore
}

export function getToken() {
  const token = 'Bearer ' + localStorage.getItem(Constants.TOKEN);
  if (token) {
    return token;
  }
  return '';
}

export const PAGE_SIZE_ARRAY = [10 , 20 ,50 ,100];
