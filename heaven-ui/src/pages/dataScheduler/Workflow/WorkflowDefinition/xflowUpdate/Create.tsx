import React, {useRef} from 'react';

import {
  ModalForm,
  ProFormInstance,
  ProFormSelect, ProFormSwitch,
  ProFormText,
  ProFormTextArea
} from "@ant-design/pro-components";
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {
  ProcessDefinitionExecutionType,
} from "@/pages/enums";
import {add} from "@/services/ant-design-pro/dataScheduler/WorkFlow/api";
import {ResultCode} from "@/pages/constants";

const Create: React.FC = (props) => {
  const { intl ,
    createVisiable ,
    setCreateVisiable ,
    data
  } = props;

  const formRef = useRef<ProFormInstance>();

  return (
    <ModalForm
      title={intl.formatMessage({
        id: 'pages.searchTable.data.dataSchedule.processDefinition.node.SQL.component.title',
        defaultMessage: 'SQL component',
      })}
      modalProps={{
        destroyOnClose: true
      }}
      grid={true}
      key="addForm"
      formRef={formRef}
      visible={createVisiable}
      onVisibleChange={setCreateVisiable}
      onFinish={async (value) => {
        if(data) {
          console.log(data.nodeData)
          const result = await add({
            "remark": value.remark,
            "executionType": value.executionType,
            "name": value.name,
            "globalParams": value.globalParams,
            "graphData": data.graphData,
            "nodeData": data.nodeData
          })

          if (result.code === ResultCode.SUCCESS_CODE) {

          }
        }
      }}
    >
      <ProFormText
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.searchTable.data.dataSchedule.processDefinition.name"
                defaultMessage="processDefinition name"
              />
            ),
          },
        ]}
        label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.name" defaultMessage="processDefinition name"/>}
        width="md"
        name="name"
        colProps={{span: 8}}
      />

      <ProFormSwitch name="timeout"
                     label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.timeout" defaultMessage="processDefinition timeout"/>}
                     colProps={{span: 8}}
      />
      <ProFormSelect
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.searchTable.data.dataSchedule.processDefinition.executionType"
                defaultMessage="processDefinition executionType"
              />
            ),
          },
        ]}
        label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.executionType" defaultMessage="processDefinition executionType"/>}
        width="md"
        name="executionType"
        valueEnum={ProcessDefinitionExecutionType}
        colProps={{span: 8}}
      />

      <ProFormTextArea
        name="globalParams"
        width="md"
        label={intl.formatMessage({
          id: 'pages.searchTable.data.dataSchedule.processDefinition.globalParams',
          defaultMessage: 'processDefinition globalParams',
        })}
      />

      <ProFormTextArea
        name="remark"
        width="md"
        label={intl.formatMessage({
          id: 'pages.searchTable.data.dataSchedule.processDefinition.remark',
          defaultMessage: 'processDefinition remark',
        })}
      />
    </ModalForm>
  );
};

export default Create;
