import {
  NsNodeCollapsePanel,
  NsNodeCmd,
  uuidv4,
  XFlowNodeCommands,
  IFlowchartGraphProps,
} from '@antv/xflow';
import Nodes from './Nodes';
import getPorts from './ports';
import {getDriverInfos} from "@/services/ant-design-pro/dataConverge/Source/api";
import {Constants, ResultCode} from "@/pages/constants";
import { Card } from 'antd';
import {handleDictData} from "@/services/ant-design-pro/system/Dict/api";

export const onNodeDrop: NsNodeCollapsePanel.IOnNodeDrop = async (
  nodeConfig,
  commandService,
) => {
  const args: NsNodeCmd.AddNode.IArgs = {
    nodeConfig: { ...nodeConfig, id: uuidv4(), ports: getPorts() },
  };
  commandService.executeCommand<NsNodeCmd.AddNode.IArgs>(
    XFlowNodeCommands.ADD_NODE.id,
    args,
  );
};

const renderNode = (
  props: {
    data: NsNodeCollapsePanel.IPanelNode;
    isNodePanel: boolean;
  },
  style: { width: number; height: number },
) => {
  const Node = Nodes[props.data.renderKey!];
  return Node ? <Node {...props} style={style} /> : null;
};

// const NodeDescription = (props: { name: string }) => {
//   return (
//     <Card size="small" title="算法组件介绍" style={{ width: '200px' }} bordered={false}>
//       欢迎使用：{props.name}
//       这里可以根据服务端返回的数据显示不同的内容
//     </Card>
//   )
// }

const nodeList = (arr: any[]) => {
  const newArr = arr.map((s) => {
    const attrs = s.attrs;
    console.log(s.id)
    return {
      popoverContent: () => <div>{s.label}</div> ,
      renderKey: s.renderKey || 'CustomNode',
      renderComponent: (props: any) => renderNode(props, attrs.style),
      label: s.label,
      id: s.id,
      attrs: attrs,
      ...attrs.canvansStyle,
      ...attrs.style
    };
  });
  return newArr;
};

let nodeStyle = {
  "width": 120,
  "height": 30,
  "marginBottom": "10px",
  "alignItems": "center",
  "padding": "0 10px",
  "borderRadius": "4px",
  "transform": "translate(0, 0)",
  "boxSizing": "border-box",
  "cursor": "move",
  "fontSize": "12px",
  "borderColor": "rgb(239,239,245)",
  "borderWidth": "3px",
  "backgroundRepeat": "no-repeat",
  "backgroundSize": "18px 18px",
  "backgroundPosition": "2px 3px",
}

export const nodeDataService: NsNodeCollapsePanel.INodeDataService =
  async () => {
    // 这里可以通过接口获取节点列表
    const result = await handleDictData (Constants.PROCESS_TASK_TYPE);
    //const result = await getDriverInfos();
    const resData = [];
    debugger;
    const dataRes = result;
    let i = 1;
    for (const key in dataRes) {
      console.log(key)
      const backgroundImage = "url(/icons/task-icons/"+key.toLocaleLowerCase()+".png)";
      const itemRes = {
        id: i,
        renderKey: 'CustomNode',
        label: key,
        attrs: {
          attribute: {
            type: 'aaa',
            tag: [1, 2],
            id: i,
            label: key
          },
          style: {...nodeStyle , backgroundImage},
          canvansStyle: {...nodeStyle , backgroundImage},
        },
      }
      resData.push(itemRes);
      i++;
    }
    return [
      {
        id: 'NODE',
        header: '节点',
        children: nodeList(resData),
      },
    ];
  };

export const useGraphConfig: IFlowchartGraphProps['useConfig'] = (config) => {
  Object.keys(Nodes).map((key) => {
    config.setNodeRender(key, (props) => {
      return renderNode({ data: props.data, isNodePanel: false }, props.size);
    });
  });
  // config.setDefaultNodeRender((props) => {
  //   return <Nodes.DefaultNode {...props} />;
  // });
};

export const searchService: NsNodeCollapsePanel.ISearchService = async (
  nodes: NsNodeCollapsePanel.IPanelNode[] = [],
  keyword: string,
) => {
  const list = nodes.filter((node) => node?.label?.includes(keyword));
  return list;
};
