import {
  CanvasMiniMap,
  createGraphConfig,
  createToolbarConfig,
  IAppLoad, IconStore, IModelService, IToolbarItemOptions, MODELS,
  NsGraphCmd, NsGraphStatusCommand, NsNodeCmd,
  XFlowCanvas, XFlowDagCommands,
  XFlowGraphCommands
} from '@antv/xflow';
import React, {useRef, useEffect, useCallback, useState} from 'react';
/** 交互组件 */
import {
  NsGraph,
  /** XFlow核心组件 */
  XFlow,
  /** 流程图画布组件 */
  FlowchartCanvas,
  /** 流程图配置扩展 */
  FlowchartExtension,
  /** 通用组件：快捷键 */
  KeyBindings,
  /** 通用组件：画布缩放 */
  CanvasScaleToolbar,
  /** 通用组件：右键菜单 */
  CanvasContextMenu,
  /** 通用组件：工具栏 */
  CanvasToolbar,
  /** 通用组件：对齐线 */
  CanvasSnapline,
  /** 通用组件：节点连接桩 */
  CanvasNodePortTooltip,
} from '@antv/xflow';
import type { Graph } from '@antv/x6';
/** 配置Command*/
import { useCmdConfig } from './config-cmd';
/** 配置Menu */
import { useMenuConfig } from './config-menu';
/** 配置Toolbar */
import {TOOLBAR_ITEMS} from './config-toolbar';
/** 配置快捷键 */
import { useKeybindingConfig } from './config-keybinding';
/** 流程图节点组件 */
import CustomNodeCollapsePanel from './CustomNodeCollapsePanel';
/** 流程图表单组件 */
import CustomFlowchartFormPanel from './CustomFlowchartFormPanel';
/** 配置Dnd组件面板 */
import '@antv/xflow/dist/index.css';
import './index.less';
import { useGraphConfig } from './CustomNodeCollapsePanel/config-dnd-panel';
import { Modal } from 'antd';
import SQL from "@/pages/dataScheduler/Workflow/WorkflowDefinition/xflow/CustomFlowchartFormPanel/NodeComponent/SQL";
import {createVisiable, nodeData} from "@/pages/dataScheduler/Workflow/WorkflowDefinition/xflow/core";
import Create from "@/pages/dataScheduler/Workflow/WorkflowDefinition/xflow/Create";
import {
  CopyOutlined,
  GatewayOutlined, GroupOutlined, PlaySquareOutlined,
  RedoOutlined,
  SaveOutlined, SnippetsOutlined,
  UndoOutlined, UngroupOutlined,
  VerticalAlignBottomOutlined,
  VerticalAlignTopOutlined
} from "@ant-design/icons";

export interface IProps {
  meta: { flowId: string };
}

/** toolbar依赖的状态 */
interface IToolbarState {
  isMultiSelectionActive: boolean;
  isGroupSelected: boolean;
  isNodeSelected: boolean;
  isUndoable: boolean;
  isRedoable: boolean;
}

const getDependencies = async (modelService: IModelService) => {
  return [
    await MODELS.SELECTED_NODES.getModel(modelService),
    await MODELS.GRAPH_ENABLE_MULTI_SELECT.getModel(modelService),
    await MODELS.HISTORY_REDOABLE.getModel(modelService),
    await MODELS.HISTORY_UNDOABLE.getModel(modelService),
  ];
};

/** 注册icon 类型 */
const registerIcon = () => {
  IconStore.set('SaveOutlined', SaveOutlined);
  IconStore.set('UndoOutlined', UndoOutlined);
  IconStore.set('RedoOutlined', RedoOutlined);
  IconStore.set('VerticalAlignTopOutlined', VerticalAlignTopOutlined);
  IconStore.set('VerticalAlignBottomOutlined', VerticalAlignBottomOutlined);
  IconStore.set('GatewayOutlined', GatewayOutlined);
  IconStore.set('GroupOutlined', GroupOutlined);
  IconStore.set('UngroupOutlined', UngroupOutlined);
  IconStore.set('CopyOutlined', CopyOutlined);
  IconStore.set('SnippetsOutlined', SnippetsOutlined);
  IconStore.set('PlaySquareOutlined', PlaySquareOutlined)
};

export const Update: React.FC<IProps> = (props) => {
  const [createVisiable, setCreateVisiable] = useState(false);
  const [data, setData] = useState({});

  const { meta ,  updateModalVisible, handleUpdateModalVisible , intl ,currentRow } = props;
  const menuConfig = useMenuConfig();
  const keybindingConfig = useKeybindingConfig();
  const graphRef = useRef<Graph>();
  const commandConfig = useCmdConfig();

  const [taskSQLPanelState ,setTaskSQLPanelState] = useState(false);
  const [nodeId , setNodeId] = useState('');
  const [currentNode , setCurrentNode] = useState({});
  const [currentArgs , setCurrentArgs] = useState({});

  /**
   * @param app 当前XFlow工作空间
   * @param extensionRegistry 当前XFlow配置项
   */
  const onLoad: IAppLoad = async (app) => {
    graphRef.current = await app.getGraphInstance();
    debugger;
    const graphJson = currentRow.graph;
    const graphData: NsGraph.IGraphData = JSON.parse(
      graphJson,
    ) || { nodes: [], edges: [] };
    debugger;
    await app.executeCommand<NsGraphCmd.GraphRender.IArgs>(
      XFlowGraphCommands.GRAPH_RENDER.id,
      {
        graphData: graphData,
      },
    );
    // 居中
    await app.executeCommand<NsGraphCmd.GraphZoom.IArgs>(
      XFlowGraphCommands.GRAPH_ZOOM.id,
      {
        factor: 'real',
      },
    );
    graphBind();
  };

  const clickBind = (args , {id , label}) => {
    debugger;
    if(label === "SQL"){
      console.log(nodeData);
      setTaskSQLPanelState(true);
    }
  }

  const graphBind = useCallback(() => {
    if (graphRef.current) {
      graphRef.current.on('node:mouseleave', (args) => {
        console.log('mouseleave');
      });
      graphRef.current.on('node:click', (args) => {

      });
      graphRef.current.on('node:mouseup', (args) => {
        console.log('mouseup');
      });
      graphRef.current.on('node:dblclick', (args) => {
        const {id , label} = args.cell.store.data.attrs.attribute;
        const nodeId = args.node.id;
        setCurrentArgs(args);
        setNodeId(nodeId);
        console.log(nodeId);
        for (const data of nodeData) {
          if(nodeId === data.id){
            setCurrentNode(data);
          }
        }
        clickBind(args,{id , label});
      });
    }
  }, [graphRef]);

  /** toolbar依赖的状态 */
  const getToolbarState = async (modelService: IModelService) => {
    // isMultiSelectionActive
    const { isEnable: isMultiSelectionActive } =
      await MODELS.GRAPH_ENABLE_MULTI_SELECT.useValue(modelService);
    // isGroupSelected
    const isGroupSelected = await MODELS.IS_GROUP_SELECTED.useValue(
      modelService,
    );
    // isNormalNodesSelected: node不能是GroupNode
    const isNormalNodesSelected =
      await MODELS.IS_NORMAL_NODES_SELECTED.useValue(modelService);
    // undo redo
    const isUndoable = await MODELS.HISTORY_UNDOABLE.useValue(modelService);
    const isRedoable = await MODELS.HISTORY_REDOABLE.useValue(modelService);

    return {
      isUndoable,
      isRedoable,
      isNodeSelected: isNormalNodesSelected,
      isGroupSelected,
      isMultiSelectionActive,
    } as IToolbarState;
  };

  const getToolbarItems = async ( state: IToolbarState) => {
    const toolbarGroup: IToolbarItemOptions[] = [];

    /** 撤销 */
    // toolbarGroup.push({
    //   tooltip: '撤销',
    //   iconName: 'UndoOutlined',
    //   id: TOOLBAR_ITEMS.GRAPH_HISTORY_UNDO,
    //   isEnabled: state.isUndoable,
    //   onClick: async ({ commandService }) => {
    //     commandService.executeCommand<NsGraphCmd.GraphHistoryUndo.IArgs>(
    //       TOOLBAR_ITEMS.GRAPH_HISTORY_UNDO,
    //       {},
    //     );
    //   },
    // });
    //
    // /** 重做 */
    // toolbarGroup.push({
    //   tooltip: '重做',
    //   iconName: 'RedoOutlined',
    //   id: TOOLBAR_ITEMS.GRAPH_HISTORY_REDO,
    //   isEnabled: state.isRedoable,
    //   onClick: async ({ commandService }) => {
    //     commandService.executeCommand<NsGraphCmd.GraphHistoryRedo.IArgs>(
    //       TOOLBAR_ITEMS.GRAPH_HISTORY_REDO,
    //       {},
    //     );
    //   },
    // });
    //
    // /** FRONT_NODE */
    // toolbarGroup.push({
    //   tooltip: '置前',
    //   iconName: 'VerticalAlignTopOutlined',
    //   id: TOOLBAR_ITEMS.FRONT_NODE,
    //   isEnabled: state.isNodeSelected,
    //   onClick: async ({ commandService, modelService }) => {
    //     const node = await MODELS.SELECTED_NODE.useValue(modelService);
    //     commandService.executeCommand<NsNodeCmd.FrontNode.IArgs>(
    //       TOOLBAR_ITEMS.FRONT_NODE,
    //       {
    //         nodeId: node?.id,
    //       },
    //     );
    //   },
    // });
    //
    // /** BACK_NODE */
    // toolbarGroup.push({
    //   tooltip: '置后',
    //   iconName: 'VerticalAlignBottomOutlined',
    //   id: TOOLBAR_ITEMS.BACK_NODE,
    //   isEnabled: state.isNodeSelected,
    //   onClick: async ({ commandService, modelService }) => {
    //     const node = await MODELS.SELECTED_NODE.useValue(modelService);
    //     commandService.executeCommand<NsNodeCmd.FrontNode.IArgs>(
    //       TOOLBAR_ITEMS.BACK_NODE,
    //       {
    //         nodeId: node?.id,
    //       },
    //     );
    //   },
    // });
    //
    // /** 保存数据 */
    // toolbarGroup.push({
    //   tooltip: '保存',
    //   iconName: 'SaveOutlined',
    //   id: TOOLBAR_ITEMS.SAVE_GRAPH_DATA,
    //   onClick: async ({ commandService }) => {
    //     console.log('保存')
    //     commandService.executeCommand<NsGraphCmd.SaveGraphData.IArgs>(
    //       TOOLBAR_ITEMS.SAVE_GRAPH_DATA,
    //       {
    //         saveGraphDataService: async (meta, graphData) => {
    //           const data = {"graphData":graphData ,"nodeData":nodeData};
    //           console.log(data);
    //           setData(data);
    //           setCreateVisiable(true);
    //         },
    //       },
    //     );
    //   },
    // });
    //
    // toolbarGroup.push({
    //   id: XFlowDagCommands.QUERY_GRAPH_STATUS.id + 'play',
    //   tooltip: '开始执行',
    //   iconName: 'PlaySquareOutlined',
    //   isEnabled: true,
    //   onClick: async ({ commandService }) => {
    //     commandService.executeCommand<NsGraphStatusCommand.IArgs>(
    //       XFlowDagCommands.QUERY_GRAPH_STATUS.id,
    //       {
    //         graphStatusService: MockApi.graphStatusService,
    //         loopInterval: 3000,
    //       },
    //     )
    //   },
    // })

    return [
      {
        name: 'graphData',
        items: toolbarGroup,
      },
    ];
  };

  const useToolbarConfig = createToolbarConfig(( toolbarConfig, proxy) => {
    registerIcon();
    /** 生产 toolbar item */
    toolbarConfig.setToolbarModelService(
      async (toolbarModel, modelService, toDispose) => {
        const updateToolbarModel = async () => {
          const state = await getToolbarState(modelService);
          const toolbarItems = await getToolbarItems(state);

          toolbarModel.setValue((toolbar) => {
            toolbar.mainGroups = toolbarItems;
          });
        };
        const models = await getDependencies(modelService);
        const subscriptions = models.map((model) => {
          return model.watch(async () => {
            updateToolbarModel();
          });
        });
        toDispose.pushAll(subscriptions);
      },
    );
  });

  const toolbarConfig = useToolbarConfig();


  return (
    <>
      <Modal visible={updateModalVisible} onOk={()=>{handleUpdateModalVisible(false)}} onCancel={()=>{handleUpdateModalVisible(false)}} width={'100%'}>
        <XFlow
          className="flow-user-custom-clz"
          commandConfig={commandConfig}
          onLoad={onLoad}
          meta={meta}
        >
          <FlowchartExtension />
          <CustomNodeCollapsePanel intl={intl}/>
          <CanvasToolbar
            className="xflow-workspace-toolbar-top"
            layout="horizontal"
            config={toolbarConfig}
            position={{ top: 0, left: 0, right: 0, bottom: 0 }}
          />
          <FlowchartCanvas
            useConfig={useGraphConfig}
            position={{ top: 40, left: 0, right: 0, bottom: 0 }}
          >
            <CanvasScaleToolbar
              layout="horizontal"
              position={{ top: -40, right: 0 }}
              style={{
                width: 150,
                left: 'auto',
                height: 39,
              }}
            />
            <CanvasContextMenu config={menuConfig} />
            <CanvasSnapline color="#faad14" />
            <CanvasNodePortTooltip />
            <CanvasMiniMap
              position={{ bottom: 0, right: 0 }}
              nodeFillColor="#873bf4"
              borderColor="#873bf4"
              handlerColor="#873bf4"
            />
          </FlowchartCanvas>
          <KeyBindings config={keybindingConfig} />
          <SQL taskSQLPanelState={taskSQLPanelState}
               setTaskSQLPanelState={setTaskSQLPanelState}
               intl={intl}
               nodeId={nodeId}
               // setNodeData={setNodeData}
               currentNode={currentNode}
               currentArgs={currentArgs}
          />
          <Create
                createVisiable={createVisiable}
                setCreateVisiable={setCreateVisiable}
                intl={intl}
                data={data}/>
        </XFlow>
      </Modal>
    </>
  );
};

export default Update;
