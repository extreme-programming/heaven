import React from 'react';

import {ProFormDigit, ProFormRadio, ProFormSelect, ProFormText, ProFormTextArea} from "@ant-design/pro-components";
import {FormattedMessage, useIntl} from "@@/plugin-locale/localeExports";

const Base: React.FC = (props) => {
  const {intl} = props;
  return (
    <>
      <ProFormText
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.searchTable.data.dataSchedule.processDefinition.node.nodeName"
                defaultMessage="node nodeName"
              />
            ),
          },
        ]}
        label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.nodeName" defaultMessage="node nodeName"/>}
        width="md"
        name="dictName"
        colProps={{span: 8}}

      />

      <ProFormRadio.Group
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.searchTable.data.dataSchedule.processDefinition.node.flag"
                defaultMessage="node flag"
              />
            ),
          },
        ]}
        label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.flag" defaultMessage="node flag"/>}
        width="md"
        name="enableServerAggregate"
        options={[
          {
            label: "开启",
            value: true
          },
          {
            label: "关闭",
            value: false
          }
        ]}
        initialValue={false}
        colProps={{span: 8}}

      />

      <ProFormSelect
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.searchTable.data.dataSchedule.processDefinition.node.taskPriority"
                defaultMessage="node taskPriority"
              />
            ),
          },
        ]}
        label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.taskPriority" defaultMessage="node taskPriority"/>}
        width="md"
        name="status"
        valueEnum={{
          0: '正常',
          1: '停用'
        }}
        colProps={{span: 8}}
      />

      <ProFormTextArea
        name="remark"
        width="md"
        label={intl.formatMessage({
          id: 'pages.searchTable.data.dataSchedule.processDefinition.node.remark',
          defaultMessage: '规则描述',
        })}
        placeholder={intl.formatMessage({
          id: 'pages.searchTable.updateForm.ruleDesc.descPlaceholder',
          defaultMessage: '请输入至少五个字符',
        })}
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.searchTable.updateForm.ruleDesc.descRules"
                defaultMessage="请输入至少五个字符的规则描述！"
              />
            ),
            min: 5,
          },
        ]}
      />

      <ProFormDigit
        label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.failRetryTimes" defaultMessage="node failRetryTimes"/>}
        name=""
        colProps={{span: 8}}
      />

      <ProFormDigit
        label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.failRetryInterval" defaultMessage="node failRetryInterval"/>}
        name=""
        colProps={{span: 8}}
      />

      <ProFormDigit
        label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.delayTime" defaultMessage="node delayTime"/>}
        name=""
        colProps={{span: 8}}
      />

      <ProFormSelect
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.searchTable.data.dataSchedule.processDefinition.node.timeoutFlag"
                defaultMessage="node timeoutFlag"
              />
            ),
          },
        ]}
        label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.timeoutFlag" defaultMessage="node timeoutFlag"/>}
        width="md"
        name="status"
        valueEnum={{
          0: '正常',
          1: '停用'
        }}
        colProps={{span: 8}}
      />

      <ProFormSelect
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.searchTable.data.dataSchedule.processDefinition.node.timeoutNotifyStrategy"
                defaultMessage="node timeoutNotifyStrategy"
              />
            ),
          },
        ]}
        label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.timeoutNotifyStrategy" defaultMessage="node timeoutNotifyStrategy"/>}
        width="md"
        name="status"
        valueEnum={{
          0: '正常',
          1: '停用'
        }}
        colProps={{span: 8}}
      />

      <ProFormDigit
        label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.timeout" defaultMessage="node timeout"/>}
        name=""
        colProps={{span: 8}}
      />

    </>
  );
};

export default Base;
