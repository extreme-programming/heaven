import type {
  NsNodeCmd,
  NsGraph,
  NsEdgeCmd,
  NsJsonSchemaForm,
} from '@antv/xflow';
import {
  JsonSchemaForm,
  XFlowNodeCommands,
  XFlowEdgeCommands,
} from '@antv/xflow';
import { FC } from 'react';
import { CanvasService, canvasSchema } from './CanvasService';
import NodeComponent from './NodeComponent';
import EdgeComponent from './EdgeComponent';
import SQL from "@/pages/dataScheduler/Workflow/WorkflowDefinition/xflow/CustomFlowchartFormPanel/NodeComponent/SQL";
import {useIntl} from "@@/plugin-locale/localeExports";

namespace NsJsonForm {
  export const getCustomRenderComponent: NsJsonSchemaForm.ICustomRender = (
    targetType,
    targetData,
    _modelService,
    commandService,
  ) => {
    const updateNode = (node: NsGraph.INodeConfig) => {
      return commandService.executeCommand<NsNodeCmd.UpdateNode.IArgs>(
        XFlowNodeCommands.UPDATE_NODE.id,
        { nodeConfig: node },
      );
    };
    const updateEdge = (edge: NsGraph.IEdgeConfig) => {
      return commandService.executeCommand<NsEdgeCmd.UpdateEdge.IArgs>(
        XFlowEdgeCommands.UPDATE_EDGE.id,
        { edgeConfig: edge, options: {} },
      );
    };
    if (targetType === 'node') {
      console.log(targetData);
      if(targetData.label === 'SQL'){
        return () => (
          <SQL updateNode={updateNode} targetData={targetData} />
        );
      }
      return () => (
        <NodeComponent updateNode={updateNode} targetData={targetData} />
      );
    }
    if (targetType === 'edge') {
      return () => (
        <EdgeComponent updateEdge={updateEdge} targetData={targetData} />
      );
    }
    if (targetType === 'canvas') {
      return () => <CanvasService />;
    }

    return null;
  };
  export const formSchemaService: NsJsonSchemaForm.IFormSchemaService = async (
    args,
  ) => {
    return canvasSchema();
  };
}

const CustomFlowchartFormPanel: FC = (props) => {
  return (
    <JsonSchemaForm
      targetType={['canvas', 'node', 'edge']}
      formSchemaService={NsJsonForm.formSchemaService}
      getCustomRenderComponent={NsJsonForm.getCustomRenderComponent}
      position={{ top: 40, bottom: 0, right: 0, width: 300 }}
    />
  );
};

export default CustomFlowchartFormPanel;
