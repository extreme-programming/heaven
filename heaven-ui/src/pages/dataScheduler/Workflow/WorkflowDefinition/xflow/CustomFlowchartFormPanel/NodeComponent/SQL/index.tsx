import React, {useEffect, useRef, useState} from 'react';

import {
  ModalForm,
  ProFormCheckbox,
  ProFormDigit, ProFormInstance,
  ProFormRadio,
  ProFormSelect, ProFormSwitch,
  ProFormText,
  ProFormTextArea
} from "@ant-design/pro-components";
import {FormattedMessage, useIntl} from "@@/plugin-locale/localeExports";
import {handleDictData} from "@/services/ant-design-pro/system/Dict/api";
import {handleGetSource} from "@/services/ant-design-pro/dataConverge/Source/api";
import {TaskPriority, TaskRunningState, TaskTimeout, TaskTimeoutNotifyStrategy} from "@/pages/enums";
import {nodeData, setData} from '../../../core';

const SQL: React.FC = (props) => {
  const { intl ,
          taskSQLPanelState ,
          setTaskSQLPanelState ,
          nodeId,
          currentNode} = props;

  const [childTypeData , setChildTypeData] = useState([]);
  const [datasourceInstance , setDatasourceInstance] = useState({});
  const formRef = useRef<ProFormInstance>();

  const setChildType = async () => {
    const childTypeData = await handleDictData('data_source_child_type');
    setChildTypeData(childTypeData);
  }

  useEffect(async function () {
    setChildType();
  },[]);


  const handleDbTypeChange = async (value) => {
    setDatasourceInstance({});
    formRef.current?.setFieldsValue({
      datasource: null
    })
    const result = await handleGetSource({"childType": value});
    //const data = [];
    const dataValue = {};
    for (const res of result) {
      const key = res.id;
      const val = {"text":res.name};
      dataValue[key] = val;
    }
    setDatasourceInstance(dataValue);
  }

  return (
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.data.dataSchedule.processDefinition.node.SQL.component.title',
          defaultMessage: 'SQL component',
        })}
        modalProps={{
          destroyOnClose: true
        }}
        grid={true}
        key="addForm"
        formRef={formRef}
        visible={taskSQLPanelState}
        onVisibleChange={setTaskSQLPanelState}
        onFinish={async (value) => {
          const taskParams = {};
          taskParams.sql = value.sql;
          taskParams.type = value.type;
          taskParams.datasource = value.datasource;
          taskParams.localParams = value.localParams;
          value['taskParams'] = JSON.stringify(taskParams);
          console.log(value);
          nodeData.filter(item => (item.id !== value.id))
          nodeData.push(value);
          setTaskSQLPanelState(false);
        }}
      >
        {/*base----start*/}
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataSchedule.processDefinition.node.nodeId"
                  defaultMessage="node nodeId"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.nodeName" defaultMessage="node nodeName"/>}
          width="md"
          name="id"
          initialValue={nodeId}
          hidden={true}
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataSchedule.processDefinition.node.nodeName"
                  defaultMessage="node nodeName"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.nodeName" defaultMessage="node nodeName"/>}
          width="md"
          name="name"
          colProps={{span: 8}}
          initialValue={(currentNode !== null && currentNode !== "") ? currentNode.name : ""}
        />

        <ProFormRadio.Group
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataSchedule.processDefinition.node.flag"
                  defaultMessage="node flag"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.flag" defaultMessage="node flag"/>}
          width="md"
          name="flag"
          valueEnum={TaskRunningState}
          colProps={{span: 8}}
          initialValue={(currentNode !== null && currentNode !== "") ? currentNode.flag : ""}

        />

        <ProFormSelect
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataSchedule.processDefinition.node.taskPriority"
                  defaultMessage="node taskPriority"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.taskPriority" defaultMessage="node taskPriority"/>}
          width="md"
          name="taskPriority"
          valueEnum={TaskPriority}
          colProps={{span: 8}}
          initialValue={(currentNode !== null && currentNode !== "") ? currentNode.taskPriority : ""}

        />

        <ProFormTextArea
          name="remark"
          width="md"
          label={intl.formatMessage({
            id: 'pages.searchTable.data.dataSchedule.processDefinition.node.remark',
            defaultMessage: '规则描述',
          })}
          placeholder={intl.formatMessage({
            id: 'pages.searchTable.updateForm.ruleDesc.descPlaceholder',
            defaultMessage: '请输入至少五个字符',
          })}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.updateForm.ruleDesc.descRules"
                  defaultMessage="请输入至少五个字符的规则描述！"
                />
              ),
              min: 5,
            },
          ]}
          initialValue={(currentNode !== null && currentNode !== "") ? currentNode.remark : ""}

        />

        <ProFormDigit
          label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.failRetryTimes" defaultMessage="node failRetryTimes"/>}
          name="failRetryTimes"
          colProps={{span: 8}}
          initialValue={(currentNode !== null && currentNode !== "") ? currentNode.failRetryTimes : ""}
        />

        <ProFormDigit
          label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.failRetryInterval" defaultMessage="node failRetryInterval"/>}
          name="failRetryInterval"
          colProps={{span: 8}}
          initialValue={(currentNode !== null && currentNode !== "") ? currentNode.failRetryInterval : ""}
        />

        <ProFormDigit
          label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.delayTime" defaultMessage="node delayTime"/>}
          name="delayTime"
          colProps={{span: 8}}
          initialValue={(currentNode !== null && currentNode !== "") ? currentNode.delayTime : ""}
        />

        <ProFormSwitch name="timeoutFlag"
                       label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.timeoutFlag" defaultMessage="node timeoutFlag"/>}
                       colProps={{span: 8}}
                       initialValue={(currentNode !== null && currentNode !== "") ? currentNode.timeoutFlag : ""}

        />

        <ProFormCheckbox.Group
          name="timeoutNotifyStrategy"
          label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.timeoutNotifyStrategy" defaultMessage="node timeoutNotifyStrategy"/>}
          valueEnum={TaskTimeoutNotifyStrategy}
          colProps={{span: 8}}
          initialValue={(currentNode !== null && currentNode !== "") ? currentNode.timeoutNotifyStrategy : ""}

        />

        <ProFormDigit
          label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.timeout" defaultMessage="node timeout"/>}
          name="timeout"
          colProps={{span: 8}}
          initialValue={(currentNode !== null && currentNode !== "") ? currentNode.timeout : ""}
        />
        {/*base----end*/}

        <ProFormSelect
          rules={[
            {
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.source.dbType"
                  defaultMessage="source dbType"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.dbType"
                                   defaultMessage="source dbType"/>}
          name="taskType"
          valueEnum={childTypeData}
          colProps={{span: 12}}
          onChange={handleDbTypeChange}
          initialValue={(currentNode !== null && currentNode !== "") ? currentNode.type : ""}

        />
        <ProFormSelect
          rules={[
            {
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataSchedule.processDefinition.node.SQL.datasource"
                  defaultMessage="SQL datasource"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataSchedule.processDefinition.node.SQL.datasource"
                                   defaultMessage="SQL datasource"/>}
          name="datasource"
          colProps={{span: 12}}
          valueEnum={datasourceInstance}
          initialValue={(currentNode !== null && currentNode !== "") ? currentNode.datasource : ""}

        />

        <ProFormTextArea
          name="sql"
          width="md"
          label={intl.formatMessage({
            id: 'pages.searchTable.data.dataSchedule.processDefinition.node.SQL.sql',
            defaultMessage: '规则描述',
          })}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.updateForm.ruleDesc.descRules"
                  defaultMessage="请输入至少五个字符的规则描述！"
                />
              ),
              min: 5,
            },
          ]}
          initialValue={(currentNode !== null && currentNode !== "") ? currentNode.sql : ""}

        />
        <ProFormTextArea
          name="localParams"
          width="md"
          label={intl.formatMessage({
            id: 'pages.searchTable.data.dataSchedule.processDefinition.node.SQL.localParams',
            defaultMessage: 'SQL localParams',
          })}
          initialValue={(currentNode !== null && currentNode !== "") ? currentNode.localParams : ""}
        />
      </ModalForm>
  );
};

export default SQL;
