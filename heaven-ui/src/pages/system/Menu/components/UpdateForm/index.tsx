// @ts-ignore
import React , {useEffect} from "react";
import {ModalForm, ProFormRadio, ProFormSelect, ProFormText, ProFormTreeSelect} from "@ant-design/pro-components";
// @ts-ignore
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {message} from "antd";

// @ts-ignore
import {ResultCode} from "@/pages/constants";
// @ts-ignore
import { editMenu } from "@/services/ant-design-pro/system/Menu/api";
// @ts-ignore
import {MenuItem} from "@/services/ant-design-pro/system/Menu/result";
import {getTreeList } from "@/services/ant-design-pro/system/Menu/api";
import {handleTree} from "@/services/ant-design-pro/system/Dept/api";
/**
 * @en-US Update node
 * @zh-CN 更新节点
 *
 * @param fields
 */
const handleUpdate = async (fields: MenuItem) => {
  const hide = message.loading('正在修改');
  const result = await editMenu(fields);
  try {
    if(result.code === ResultCode.SUCCESS_CODE){
      message.success(result.msg);
    }
    hide();
    return true;
  } catch (error) {
    hide();
    message.error(result.msg);
    return false;
  }
};

// @ts-ignore
const UpdateForm: React.FC = ({actionRef , intl  ,updateModalVisible , handleUpdateModalVisible ,currentRow , flag  ,menuTypeChange}) => {

  // useEffect(function (){
  //
  // },[]);

  return (
    <>
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.system.menu.updateForm.title',
          defaultMessage: 'Update Menu',
        })}
        modalProps={{
          destroyOnClose: true
        }}
        key="editForm"
        grid={true}
        visible={updateModalVisible}
        onVisibleChange={handleUpdateModalVisible}
        onFinish={async (value) => {
          const success = await handleUpdate(value as MenuItem);
          if (success) {
            handleUpdateModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormTreeSelect
          label={<FormattedMessage id="pages.searchTable.system.menu.SuperiorMenu"
                                   defaultMessage="Superior Menu"/>}
          name="parentId"
          request={async () => {
            const response = await getTreeList();
            const {  data } = response;
            const deptList = handleTree(data, "menuId" , "menuName");
            return deptList;
          }}
          required={true}
          initialValue={currentRow?.parentId || null}
        />

        <ProFormRadio.Group
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.menu.MenuType"
                  defaultMessage="Menu type"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.menu.MenuType" defaultMessage="Menu type"/>}
          width="md"
          name="menuType"
          options={[
            {
              label: "目录",
              value: "M"
            },
            {
              label: "菜单",
              value: "C"
            },
            {
              label: "按钮",
              value: "F"
            }
          ]}
          initialValue={currentRow?.menuType || null}
          onChange={menuTypeChange}
        />

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.menu.MenuName"
                  defaultMessage="Menu name"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.menu.MenuName" defaultMessage="Menu name"/>}
          width="md"
          name="menuName"
          colProps={{span: 12}}
          initialValue={""+currentRow?.menuName || null}
        />

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.menu.OrderNum"
                  defaultMessage="Menu order num"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.menu.OrderNum"
                                   defaultMessage="Menu order num"/>}
          width="md"
          name="orderNum"
          colProps={{span: 12}}
          initialValue={currentRow?.orderNum || null}
        />

        {((flag.catalogueFlag || flag.menuFlag) && !flag.buttonFlag) && (
          <>
            <ProFormText
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="pages.searchTable.system.menu.Icon"
                      defaultMessage="Menu icon"
                    />
                  ),
                },
              ]}
              label={<FormattedMessage id="pages.searchTable.system.menu.Icon" defaultMessage="Menu icon"/>}
              name="icon"
              initialValue={currentRow?.icon || null}
            />
            <ProFormSelect
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="pages.searchTable.system.menu.IsFrame"
                      defaultMessage="Menu isFrame"
                    />
                  ),
                },
              ]}
              label={<FormattedMessage id="pages.searchTable.system.menu.IsFrame"
                                       defaultMessage="Menu isFrame"/>}
              width="md"
              name="isFrame"
              valueEnum={{
                0: '是',
                1: '否'
              }}
              colProps={{span: 12}}
              initialValue={currentRow?.isFrame || null}
            />
            <ProFormText
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="pages.searchTable.system.menu.Path"
                      defaultMessage="Menu path"
                    />
                  ),
                },
              ]}
              label={<FormattedMessage id="pages.searchTable.system.menu.Path" defaultMessage="Menu path"/>}
              width="md"
              name="path"
              colProps={{span: 12}}
              initialValue={currentRow?.path || null}
            />
          </>)
        }

        {!flag.catalogueFlag && flag.menuFlag && !flag.buttonFlag && (
          <>
            <ProFormText
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="pages.searchTable.system.menu.Component"
                      defaultMessage="Menu component"
                    />
                  ),
                },
              ]}
              label={<FormattedMessage id="pages.searchTable.system.menu.Component"
                                       defaultMessage="Menu component"/>}
              width="md"
              name="component"
              colProps={{span: 12}}
              initialValue={currentRow?.component || null}
            />
            <ProFormText
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="pages.searchTable.system.menu.Query"
                      defaultMessage="Menu query"
                    />
                  ),
                },
              ]}
              label={<FormattedMessage id="pages.searchTable.system.menu.Query" defaultMessage="Menu query"/>}
              width="md"
              name="query"
              colProps={{span: 12}}
              initialValue={currentRow?.query || null}
            />
            <ProFormSelect
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="pages.searchTable.system.menu.IsCache"
                      defaultMessage="Menu isCache"
                    />
                  ),
                },
              ]}
              label={<FormattedMessage id="pages.searchTable.system.menu.IsCache"
                                       defaultMessage="Menu isCache"/>}
              width="md"
              name="isCache"
              valueEnum={{
                0: '是',
                1: '否'
              }}
              colProps={{span: 12}}
              initialValue={currentRow?.isCache || null}
            />
          </>
        )}

        {(!flag.catalogueFlag || flag.menuFlag || flag.buttonFlag) && (
          <>
            <ProFormText
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="pages.searchTable.system.menu.Perms"
                      defaultMessage="Menu perms"
                    />
                  ),
                },
              ]}
              label={<FormattedMessage id="pages.searchTable.system.menu.Perms" defaultMessage="Menu perms"/>}
              width="md"
              name="perms"
              colProps={{span: 12}}
              initialValue={currentRow?.perms || null}
            />
          </>
        )}

        {((flag.catalogueFlag || flag.menuFlag) && !flag.buttonFlag) && (
          <>
            <ProFormSelect
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="pages.searchTable.system.menu.Visible"
                      defaultMessage="Menu visible"
                    />
                  ),
                },
              ]}
              label={<FormattedMessage id="pages.searchTable.system.menu.Visible"
                                       defaultMessage="Menu visible"/>}
              width="md"
              name="visible"
              valueEnum={{
                0: '是',
                1: '否'
              }}
              colProps={{span: 12}}
              initialValue={currentRow?.visible || null}
            />
            <ProFormSelect
              rules={[
                {
                  required: true,
                  message: (
                    <FormattedMessage
                      id="pages.searchTable.system.menu.Status"
                      defaultMessage="Menu status"
                    />
                  ),
                },
              ]}
              label={<FormattedMessage id="pages.searchTable.system.menu.Status"
                                       defaultMessage="Menu status"/>}
              width="md"
              name="status"
              valueEnum={{
                0: '正常',
                1: '停用'
              }}
              colProps={{span: 12}}
              initialValue={currentRow?.status || null}
            />
          </>
        )}

        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.system.MenuId" defaultMessage="Menu id" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.MenuId" defaultMessage="Menu id"/>}
          width="md"
          name="menuId"
          initialValue={"" +currentRow?.menuId || null}
          hidden={true}
        />
      </ModalForm>
    </>
  )
}

export default UpdateForm;
