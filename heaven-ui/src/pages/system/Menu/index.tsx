import {PlusOutlined, EditOutlined} from '@ant-design/icons';
import {
  ActionType,
  ProColumns,
  ProTable,
} from '@ant-design/pro-components';
import {
  FooterToolbar,
  PageContainer,
} from '@ant-design/pro-components';
import {Button, message, RadioChangeEvent, TreeSelect} from 'antd';
import React, {useEffect, useRef, useState} from 'react';
import {FormattedMessage, useIntl} from 'umi';
import request from "@/services/ant-design-pro/request";

import {handleTree} from "@/services/ant-design-pro/system/Dept/api";
import {getMenu, removeMenu} from "@/services/ant-design-pro/system/Menu/api";
import AddForm from "@/pages/system/Menu/components/AddForm";
import UpdateForm from "@/pages/system/Menu/components/UpdateForm";
import {MenuItem, MenuItemResult} from '@/services/ant-design-pro/system/Menu/result';
import {TableListIsSuccessEnum} from "@/pages/enums";

interface Flag{
  catalogueFlag: boolean,
  menuFlag: boolean,
  buttonFlag: boolean
}

/**
 *  Delete node
 * @zh-CN 删除节点
 *
 * @param selectedRows
 */
const handleRemove = async (selectedRows: MenuItem[]) => {
  const hide = message.loading('正在删除');
  if (!selectedRows) return true;

  const params: number[] = [];
  selectedRows.map((item) => {
    params.push(item.menuId)
  })
  try {
    const result = await removeMenu(params);
    hide();
    message.success(result.msg);
    return true;
  } catch (error) {
    hide();
    message.error('Delete failed, please try again');
    return false;
  }
};

const Menu: React.FC = () => {
  /**
   * @en-US Pop-up window of new window
   * @zh-CN 新建窗口的弹窗
   *  */
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);

  /**
   * @en-US The pop-up window of the distribution update window
   * @zh-CN 分布更新窗口的弹窗
   * */
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<MenuItem>();
  const [selectedRowsState, setSelectedRows] = useState<MenuItem[]>([]);
  const [flag, setFlag] = useState<Flag>({
    catalogueFlag: true,
    menuFlag: false,
    buttonFlag: false
  });

  const menuTypeChange = ({target:{value}}: RadioChangeEvent) => {
    verifyFlag(value)
  }

  function verifyFlag (value: string) {
    if(value === 'M'){
      setFlag({
        catalogueFlag: true,
        menuFlag: false,
        buttonFlag: false
      })
    }else if(value === 'C'){
      setFlag({
        catalogueFlag: false,
        menuFlag: true,
        buttonFlag: false
      })
    }else if(value === 'F'){
      setFlag({
        catalogueFlag: false,
        menuFlag: false,
        buttonFlag: true
      })
    }
  }

  useEffect(() => {
    setFlag({
      catalogueFlag: true,
      menuFlag: false,
      buttonFlag: false
    })
  }, []);

  const getList = async (params: {}, options?: { [key: string]: any }) => {
    const response = await request<MenuItemResult>('/api/system/menu/list', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
      params: {
        ...params,
      },
      ...(options || {}),
    })
    const {code, data, msg} = response;
    const list = handleTree(data, "menuId");
    const result = {code, msg, success: true, data: list};
    return result;
  }


  /**
   * @en-US International configuration
   * @zh-CN 国际化配置
   * */
  const intl = useIntl();

  const columns: ProColumns<MenuItem>[] = [
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.system.menu.MenuName"
          defaultMessage="Menu name"
        />
      ),
      align: 'center',
      dataIndex: 'menuName',
      key: 'menuName',
      renderText: (val: string) => {
        return val
      }
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.system.menu.Icon"
          defaultMessage="Menu icon"
        />
      ),
      align: 'center',
      dataIndex: 'icon',
      key: 'icon',
      hideInSearch: true,
      hideInForm: true,
      renderText: (val: string) => {
        return val
      }
    },
    {
      title: <FormattedMessage id="pages.searchTable.system.menu.OrderNum" defaultMessage="Menu orderNum"/>,
      dataIndex: 'orderNum',
      align: 'center',
      hideInSearch: true,
      hideInForm: true,
      key: 'orderNum',
    }
    ,
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.system.menu.Perms"
          defaultMessage="Menu perms"
        />
      ),
      align: 'center',
      dataIndex: 'perms',
      key: 'perms',
      hideInSearch: true,
      hideInForm: true,
      renderText: (val: string) => {
        return val
      }
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.system.menu.Component"
          defaultMessage="Menu component"
        />
      ),
      align: 'center',
      dataIndex: 'component',
      key: 'component',
      hideInSearch: true,
      hideInForm: true,
      renderText: (val: string) => {
        return val
      }
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.system.menu.Status"
          defaultMessage="Menu status"
        />
      ),
      dataIndex: 'status',
      align: 'center',
      valueType: 'select',
      key: 'status',
      renderText: (val: string) => {
        return val
      }
      ,
      valueEnum: TableListIsSuccessEnum,
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.system.menu.CreateTime"
          defaultMessage="Menu create time"
        />
      ),
      render: (_, record) => <span>{record.createTime}</span>,
      search: {
        transform: (value) => {
          return {
            'params[beginTime]': value[0],
            'params[endTime]': value[1],
          }
        }
      },
      // sorter: true,
      hideInSearch: true,
      hideInForm: true,
      dataIndex: 'createTime',
      align: 'center',
      valueType: 'dateRange',
      key: 'createTime',
      renderText: (val: string) => {
        return val;
      }
      ,
    },
    {
      title: intl.formatMessage({
        id: 'pages.searchTable.titleOption',
        defaultMessage: 'Enquiry Option',
      }),
      dataIndex: 'option',
      valueType: 'option',
      key: 'update',
      align: 'center',
      render: (_, record) => [
        <Button
          type="primary"
          onClick={async () => {
            const result: MenuItemResult = await getMenu(record.menuId);

            const res = result.data;
            //verifyFlag(res['menuType']);
            setCurrentRow(res);
            handleUpdateModalVisible(true);
          }}
          shape="round"
        >
          <EditOutlined/> <FormattedMessage id="pages.searchTable.update" defaultMessage="Update"/>
        </Button>
      ],
    },
  ];

  // @ts-ignore
  return (
    <PageContainer>
      <ProTable<MenuItem, API.Param.BasePageParam>
        headerTitle={intl.formatMessage({
          id: 'pages.searchTable.title',
          defaultMessage: 'Enquiry form',
        })}
        actionRef={actionRef}
        rowKey="menuId"
        search={{
          labelWidth: 120,
        }}
        recordCreatorProps={{
          position: 'bottom',
          onClick: () => {
            handleModalVisible(true);
          }
        }}
        columns={columns}
        request={getList}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
          type: 'radio'
        }}
        toolBarRender={() => [
          <Button
            type="primary"
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <PlusOutlined/> <FormattedMessage id="pages.searchTable.new" defaultMessage="New"/>
          </Button>,
        ]}
        pagination={false}
        bordered={true}

      />
      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.searchTable.chosen" defaultMessage="Chosen"/>{' '}
              <a style={{fontWeight: 600}}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.searchTable.item" defaultMessage="项"/>
            </div>
          }
        >
          <Button
            onClick={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
          >
            <FormattedMessage
              id="pages.searchTable.batchDeletion"
              defaultMessage="Batch deletion"
            />
          </Button>
        </FooterToolbar>
      )}

      <AddForm actionRef={actionRef}
             intl={intl}
             createModalVisible={createModalVisible}
             handleModalVisible={handleModalVisible}
             flag={flag}
             setFlag={setFlag}
             menuTypeChange={menuTypeChange}
      />

      <UpdateForm actionRef={actionRef}
             intl={intl}
             updateModalVisible={updateModalVisible}
             handleUpdateModalVisible={handleUpdateModalVisible}
             currentRow={currentRow}
             flag={flag}
             setFlag={setFlag}
             menuTypeChange={menuTypeChange}
      />

    </PageContainer>
  )
};

export default Menu;
