import React from "react";
import {ModalForm, ProFormSelect, ProFormText} from "@ant-design/pro-components";
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {DictItem} from "@/services/ant-design-pro/system/Dict/result";
import {message} from "antd";
import {addDictType} from "@/services/ant-design-pro/system/Dict/api";
import {ResultCode} from "@/pages/constants";

const handleAdd = async (fields: DictItem) => {
  const hide = message.loading('正在添加');
  const result = await addDictType({ ...fields });
  try {
    if(result.code === ResultCode.SUCCESS_CODE){
      message.success(result.msg);
    }
    hide();
    return true;
  } catch (error) {
    hide();
    message.error(result.msg);
    return false;
  }
};

// @ts-ignore
const AddForm: React.FC = ({actionRef , intl  ,createModalVisible , handleModalVisible}) => {
  return (
    <>
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.system.dict.addForm.title',
          defaultMessage: 'New Dict Type',
        })}
        modalProps={{
          destroyOnClose: true
        }}
        key="addForm"
        width="400px"
        visible={createModalVisible}
        onVisibleChange={handleModalVisible}
        onFinish={async (value) => {
          const success = await handleAdd(value as DictItem);

          if (success) {
            handleModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.dict.DictName"
                  defaultMessage="Dict name"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dict.DictName" defaultMessage="Dict name"/>}
          width="md"
          name="dictName"
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="pages.searchTable.system.dict.DictType" defaultMessage="Dict type" />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dict.DictType" defaultMessage="Dict type"/>}
          width="md"
          name="dictType"
        />

        <ProFormSelect
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.dict.Status"
                  defaultMessage="Dict status"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dict.Status" defaultMessage="Dict status"/>}
          width="md"
          name="status"
          valueEnum={{
            0: '正常',
            1: '停用'
          }}
        />

        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.system.dict.Remark" defaultMessage="Dict remark" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dict.Remark" defaultMessage="Dict remark"/>}
          width="md"
          name="remark"
        />
      </ModalForm>
    </>
  )
}

export default AddForm;
