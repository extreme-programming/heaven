import React from "react";
import {ModalForm, ProFormSelect, ProFormText} from "@ant-design/pro-components";
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {DictItem} from "@/services/ant-design-pro/system/Dict/result";
import {message} from "antd";
import { editDictType} from "@/services/ant-design-pro/system/Dict/api";
import {ResultCode} from "@/pages/constants";

/**
 * @en-US Update node
 * @zh-CN 更新节点
 *
 * @param fields
 */
const handleUpdate = async (fields: DictItem) => {

  const hide = message.loading('正在修改');
  const result = await editDictType(fields);
  try {
    if(result.code === ResultCode.SUCCESS_CODE){
      message.success(result.msg);
    }
    hide();
    return true;
  } catch (error) {
    hide();
    message.error(result.msg);
    return false;
  }
};

// @ts-ignore
const AddForm: React.FC = ({actionRef , intl  ,updateModalVisible , handleUpdateModalVisible ,currentRow}) => {
  return (
    <>
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.system.dict.updateForm.title',
          defaultMessage: 'Update dict type',
        })}
        modalProps={{
          destroyOnClose: true
        }}
        key="updateForm"
        width="400px"
        visible={updateModalVisible}
        onVisibleChange={handleUpdateModalVisible}
        onFinish={async (value) => {
          const success = await handleUpdate(value as DictItem);
          if (success) {
            handleUpdateModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.dict.DictName"
                  defaultMessage="Dict name"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dict.DictName" defaultMessage="Dict name"/>}
          width="md"
          name="dictName"
          initialValue={"" +currentRow?.dictName || null}

        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="pages.searchTable.system.dict.DictType" defaultMessage="Dict type" />
              ),
            },
          ]}
          width="md"
          name="dictType"
          label={<FormattedMessage id="pages.searchTable.system.dict.DictType" defaultMessage="Dict type"/>}
          //todo 必须加 "" 否则警告添加不了
          initialValue={"" + currentRow?.dictType || null}
        />

        <ProFormSelect
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.dict.Status"
                  defaultMessage="Dict status"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dict.Status" defaultMessage="Dict status"/>}
          width="md"
          name="status"
          valueEnum={{
            0: '正常',
            1: '停用'
          }}
          initialValue={currentRow?.status || null}
        />

        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.system.dict.Remark" defaultMessage="Dict remark" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dict.Remark" defaultMessage="Dict remark"/>}
          width="md"
          name="remark"
          initialValue={"" +currentRow?.remark || null}
        />
        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.system.dict.DictId" defaultMessage="Dict id" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dict.DictId" defaultMessage="Dict id"/>}
          width="md"
          name="dictId"
          initialValue={"" +currentRow?.dictId || null}
          hidden={true}
        />
      </ModalForm>
    </>
  )
}

export default AddForm;
