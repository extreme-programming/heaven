import React from "react";
import {ModalForm, ProFormSelect, ProFormText, ProFormTreeSelect} from "@ant-design/pro-components";
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {message} from "antd";

import {editDept, handleTree , getTreeList} from "@/services/ant-design-pro/system/Dept/api";
import {DeptItem} from "@/services/ant-design-pro/system/Dept/result";
import {ResultCode} from "@/pages/constants";

/**
 * @en-US Update node
 * @zh-CN 更新节点
 *
 * @param fields
 */
const handleUpdate = async (fields: DeptItem) => {
  const hide = message.loading('正在修改');
  const result = await editDept(fields);
  try {
    if(result.code === ResultCode.SUCCESS_CODE){
      message.success(result.msg);
    }
    hide();
    return true;
  } catch (error) {
    hide();
    message.error(result.msg);
    return false;
  }
};

// @ts-ignore
const UpdateForm: React.FC = ({actionRef , intl  ,updateModalVisible , handleUpdateModalVisible ,currentRow}) => {
  return (
    <>
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.system.dept.updateForm.title',
          defaultMessage: 'Update Dept',
        })}
        modalProps={{
          destroyOnClose: true
        }}
        key="editForm"
        width="400px"
        visible={updateModalVisible}
        onVisibleChange={handleUpdateModalVisible}
        onFinish={async (value) => {
          const success = await handleUpdate(value as DeptItem);
          if (success) {
            handleUpdateModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormTreeSelect
          label={<FormattedMessage id="pages.searchTable.system.dept.SuperiorDept" defaultMessage="Superior Dept"/>}
          width="md"
          name="parentId"
          request={async  () => {
            const response = await getTreeList()
            const {  data } = response;
            const deptList = handleTree(data, "deptId" , "deptName");
            return deptList;
          }}
          required={true}
          initialValue={"" +currentRow?.parentId || null}
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.dept.DeptName"
                  defaultMessage="Dept name"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dept.DeptName" defaultMessage="Dept name"/>}
          width="md"
          name="deptName"
          initialValue={"" +currentRow?.deptName || null}
        />
        <ProFormSelect
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.dept.Status"
                  defaultMessage="Dept status"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dept.Status" defaultMessage="Dept status"/>}
          width="md"
          name="status"
          valueEnum={{
            0: '正常',
            1: '停用'
          }}
          initialValue={"" +currentRow?.status || null}
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.dept.OrderNum"
                  defaultMessage="Dept order num"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dept.OrderNum" defaultMessage="Dept order num"/>}
          width="md"
          name="orderNum"
          initialValue={"" +currentRow?.orderNum || null}
        />
        <ProFormText
          rules={[
            {
              required: false,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.dept.Leader"
                  defaultMessage="Dept leader"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dept.Leader" defaultMessage="Dept leader"/>}
          width="md"
          name="leader"
          initialValue={"" +currentRow?.leader || null}
        />

        <ProFormText
          rules={[
            {
              required: false,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.dept.Phone"
                  defaultMessage="Dept order num"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dept.Phone" defaultMessage="Dept phone"/>}
          width="md"
          name="phone"
          initialValue={"" +currentRow?.phone || null}
        />

        <ProFormText
          rules={[
            {
              required: false,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.dept.Email"
                  defaultMessage="Dept email"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dept.Email" defaultMessage="Dept email"/>}
          width="md"
          name="email"
          initialValue={"" +currentRow?.email || null}
        />

        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.system.dept.DeptId" defaultMessage="Dept id" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dept.DeptId" defaultMessage="Dept id"/>}
          width="md"
          name="deptId"
          initialValue={"" +currentRow?.deptId || null}
          hidden={true}
        />

      </ModalForm>
    </>
  )
}

export default UpdateForm;
