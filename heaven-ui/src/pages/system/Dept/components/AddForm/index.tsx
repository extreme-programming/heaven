import React from "react";
import {ModalForm, ProFormSelect, ProFormText, ProFormTreeSelect} from "@ant-design/pro-components";
import {message} from "antd";
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {addDept, handleTree ,getTreeList} from "@/services/ant-design-pro/system/Dept/api";
import {DeptItem} from "@/services/ant-design-pro/system/Dept/result";
import {ResultCode} from "@/pages/constants";

/**
 * @en-US Add node
 * @zh-CN 添加节点
 * @param fields
 */
const handleAdd = async (fields: DeptItem) => {
  const hide = message.loading('正在添加');
  const result = await addDept({ ...fields });
  try {
    if(result.code === ResultCode.SUCCESS_CODE){
      message.success(result.msg);
    }
    hide();
    return true;
  } catch (error) {
    hide();
    message.error(result.msg);
    return false;
  }
};

// @ts-ignore
const AddForm: React.FC = ({actionRef , intl  ,createModalVisible , handleModalVisible}) => {
  return (
    <>
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.system.dept.addForm.title',
          defaultMessage: 'New Dept',
        })}
        modalProps={{
          destroyOnClose: true
        }}
        key="addForm"
        width="400px"
        visible={createModalVisible}
        onVisibleChange={handleModalVisible}

        onFinish={async (value) => {
          const success = await handleAdd(value as DeptItem);
          if (success) {
            handleModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormTreeSelect
          label={<FormattedMessage id="pages.searchTable.system.dept.SuperiorDept" defaultMessage="Superior Dept"/>}
          width="md"
          name="parentId"
          request={async () => {
            const response = await getTreeList()
            const {  data } = response;
            const deptList = handleTree(data, "deptId" , "deptName");
            return deptList;
          }}
          required={true}

        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.dept.DeptName"
                  defaultMessage="Dept name"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dept.DeptName" defaultMessage="Dept name"/>}
          width="md"
          name="deptName"
        />
        <ProFormSelect
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.dept.Status"
                  defaultMessage="Dept status"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dept.Status" defaultMessage="Dept status"/>}
          width="md"
          name="status"
          valueEnum={{
            0: '正常',
            1: '停用'
          }}
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.dept.OrderNum"
                  defaultMessage="Dept order num"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dept.OrderNum" defaultMessage="Dept order num"/>}
          width="md"
          name="orderNum"
        />
        <ProFormText
          rules={[
            {
              required: false,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.dept.Leader"
                  defaultMessage="Dept leader"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dept.Leader" defaultMessage="Dept leader"/>}
          width="md"
          name="leader"
        />

        <ProFormText
          rules={[
            {
              required: false,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.dept.Phone"
                  defaultMessage="Dept order num"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dept.Phone" defaultMessage="Dept phone"/>}
          width="md"
          name="phone"
        />

        <ProFormText
          rules={[
            {
              required: false,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.dept.Email"
                  defaultMessage="Dept email"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.dept.Email" defaultMessage="Dept email"/>}
          width="md"
          name="email"
        />
      </ModalForm>
    </>
  )
}

export default AddForm;
