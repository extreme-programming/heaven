import { PlusOutlined , EditOutlined } from '@ant-design/icons';
import {ActionType, ProColumns , ProTable} from '@ant-design/pro-components';
import {
  FooterToolbar,
  PageContainer,
} from '@ant-design/pro-components';
import { Button, message } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage, useIntl } from 'umi';
import request from "@/services/ant-design-pro/request";

import {DeptItem, DeptItemResult} from "@/services/ant-design-pro/system/Dept/result";
import { handleTree , getDept  ,  removeDept} from "@/services/ant-design-pro/system/Dept/api";
import AddForm from "@/pages/system/Dept/components/AddForm";
import UpdateForm from "@/pages/system/Dept/components/UpdateForm";
import {TableListIsSuccessEnum} from "@/pages/enums";

/**
 *  Delete node
 * @zh-CN 删除节点
 *
 * @param selectedRows
 */
const handleRemove = async (selectedRows: DeptItem[]) => {
  const hide = message.loading('正在删除');
  if (!selectedRows) return true;
  const params: number[] = [];
  selectedRows.map((item) =>{
    params.push(item.deptId)
  })
  try {
    const result = await removeDept(params);
    hide();
    message.success(result.msg);
    return true;
  } catch (error) {
    hide();
    message.error('Delete failed, please try again');
    return false;
  }
};


const TableList: React.FC = () => {
  /**
   * @en-US Pop-up window of new window
   * @zh-CN 新建窗口的弹窗
   *  */
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);

  /**
   * @en-US The pop-up window of the distribution update window
   * @zh-CN 分布更新窗口的弹窗
   * */
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<DeptItem>();
  const [selectedRowsState, setSelectedRows] = useState<DeptItem[]>([]);

  const getDeptList = async (params: {},options?: { [key: string]: any }) => {
    const response = await request<DeptItemResult>('/api/system/dept/list', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
      params: {
        ...params,
      },
      ...(options || {}),
    })
    const { code , data , msg } = response;
    const deptList = handleTree(data, "deptId");
    const result = {code , msg , success:true ,data:deptList};
    return result;
  }

  /**
   * @en-US International configuration
   * @zh-CN 国际化配置
   * */
  const intl = useIntl();

  const columns: ProColumns<DeptItem>[] = [
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.system.dept.DeptName"
          defaultMessage="Dept name"
        />
      ),
      align: 'center',
      dataIndex: 'deptName',
      key: 'deptName',
      renderText: (val: string) => {return val}
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.system.dept.Status"
          defaultMessage="Dept status"
        />
      ),
      dataIndex: 'status',
      align: 'center',
      valueType: 'select',
      key: 'status',
      renderText: (val: string) => {
        return val
      }
      ,
      valueEnum: TableListIsSuccessEnum,
    },
    {
      title: <FormattedMessage id="pages.searchTable.system.dept.OrderNum" defaultMessage="Dept orderNum" />,
      dataIndex: 'orderNum',
      align: 'center',
      hideInSearch: true,
      hideInForm: true,
      key: 'orderNum',
    }
    ,
    {
      title: intl.formatMessage({
        id: 'pages.searchTable.titleOption',
        defaultMessage: 'Enquiry Option',
      }),
      dataIndex: 'option',
      valueType: 'option',
      key: 'update',
      align: 'center',
      render: (_, record) => [
        <Button
          type="primary"
          onClick={async () => {
            const result: ResultTypings.ResultForInterfaceWithObj = await getDept(record.deptId);
            setCurrentRow(result.data);
            handleUpdateModalVisible(true);
          }}
          shape="round"
        >
          <EditOutlined />  <FormattedMessage id="pages.searchTable.update" defaultMessage="Update"/>
        </Button>
      ],
    },
  ];

  return (
    <PageContainer>
      <ProTable<DeptItem, API.Param.BasePageParam>
        headerTitle={intl.formatMessage({
          id: 'pages.searchTable.title',
          defaultMessage: 'Enquiry form',
        })}
        actionRef={actionRef}
        rowKey="deptId"
        search={{
          labelWidth: 120,
        }}
        recordCreatorProps={{
          position: 'bottom',
          onClick: () => {
            handleModalVisible(true);
          }
        }}
        columns={columns}
        request={getDeptList}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
          type :'radio'
        }}
        toolBarRender={() => [
          <Button
            type="primary"
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <PlusOutlined /> <FormattedMessage id="pages.searchTable.new" defaultMessage="New"/>
          </Button>,
        ]}
        pagination={false}
        bordered={true}
      />
      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.searchTable.chosen" defaultMessage="Chosen" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.searchTable.item" defaultMessage="项" />
            </div>
          }
        >
          <Button
            onClick={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
          >
            <FormattedMessage
              id="pages.searchTable.batchDeletion"
              defaultMessage="Batch deletion"
            />
          </Button>
        </FooterToolbar>
      )}

      <AddForm actionRef={actionRef}
             intl={intl}
             createModalVisible={createModalVisible}
             handleModalVisible={handleModalVisible}
      />

      <UpdateForm actionRef={actionRef}
             intl={intl}
             updateModalVisible={updateModalVisible}
             handleUpdateModalVisible={handleUpdateModalVisible}
             currentRow={currentRow}
      />
    </PageContainer>
  );
};

export default TableList;
