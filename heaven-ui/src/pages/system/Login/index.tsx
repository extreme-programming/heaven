import Footer from '@/components/Footer';
import {login} from '@/services/ant-design-pro/system/Login/api';
import {
  LoginForm,
} from '@ant-design/pro-components';
import {Alert, message} from 'antd';
import React, {useRef, useState} from 'react';
import {history, SelectLang, useModel} from 'umi';
import styles from './index.less';
import Account from "@/pages/system/Login/Account";
import {LoginParam} from "@/services/ant-design-pro/system/Login/param.d";
import {Constants, ResultCode} from "@/pages/constants";
import {LoginResult} from "@/services/ant-design-pro/system/Login/result";
import {useIntl} from "@@/plugin-locale/localeExports";


import type { ProFormInstance } from '@ant-design/pro-form';


const LoginMessage: React.FC<{
  content: string;
  width: string | number;
}> = ({content, width}) => (
  <Alert
    style={{
      marginBottom: 24,
      width
    }}
    message={content}
    type="error"
    showIcon
  />
);

const Login: React.FC = () => {
  // @ts-ignore
  const [userLoginState, setUserLoginState] = useState<LoginResult>({code: 0, data: {}, msg: ""});
  const type = 'account';
  const {initialState, setInitialState} = useModel('@@initialState');
  const intl = useIntl();
  const formRef = useRef<ProFormInstance>();

  const fetchUserInfo = async () => {
    const userInfo = await initialState?.fetchUserInfo?.();
    if (userInfo) {
      await setInitialState((s) => ({
        ...s,
        currentUser: userInfo,
      }));
    }
  };

  const handleSubmit = async (values: LoginParam) => {
    try {
      // 登录
      const {code, msg, data} = await login({...values, type});

      if (code === ResultCode.SUCCESS_CODE) {
        const defaultLoginSuccessMessage = intl.formatMessage({
          id: 'pages.login.success',
          defaultMessage: '登录成功！',
        });
        message.success(defaultLoginSuccessMessage);
        localStorage.setItem(Constants.TOKEN, data.access_token);
        await fetchUserInfo();
        /** 此方法会跳转到 redirect 参数所在的位置 */
        if (!history) return;
        const {query} = history.location;
        const {redirect} = query as { redirect: string };
        history.push(redirect || '/');
        return;
      }
      // 如果失败去设置用户错误信息
      setUserLoginState({code, msg, data});
    } catch (error) {
      const defaultLoginFailureMessage = intl.formatMessage({
        id: 'pages.login.failure',
        defaultMessage: '登录失败，请重试！',
      });
      message.error(defaultLoginFailureMessage);
    }
  };
  // @ts-ignore
  const {code, msg} = userLoginState;

  // @ts-ignore
  return (
    <div className={styles.container}>
      <div className={styles.lang} data-lang>
        {SelectLang && <SelectLang/>}
      </div>
      <div className={styles.content}>
        <LoginForm
          logo={<img alt="logo" src="/logo.png" />}
          title="Heaven Data Platform"
          subTitle={intl.formatMessage({
            id: 'pages.layouts.userLayout.title',
            defaultMessage: '一个人可以走得更快 , 但一群人会走得更远',
          })}
          grid={true}
          initialValues={{
            autoLogin: true,
          }}
          actions={[]}
          onFinish={async (values) => {
            await handleSubmit(values as LoginParam);
          }}
          formRef={formRef}
        >
          {msg && code && code !== ResultCode.SUCCESS_CODE && type === 'account' &&
            <LoginMessage content={msg} width={'100%'}/>}

          {type === 'account' && <Account formRef={formRef} intl={intl}/>}
        </LoginForm>
      </div>
      <Footer/>
    </div>
  );
};

export default Login;
