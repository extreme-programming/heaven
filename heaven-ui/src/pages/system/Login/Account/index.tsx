import React, {useEffect, useState} from "react";
import {ProFormText} from "@ant-design/pro-components";
import {LockOutlined,SafetyCertificateOutlined, UserOutlined} from "@ant-design/icons";
import styles from "@/pages/system/Login/index.less";
import {FormattedMessage, useIntl} from "@@/plugin-locale/localeExports";
import { getCode } from "@/services/ant-design-pro/system/Login/api";
import {CodeResult} from "@/services/ant-design-pro/system/Login/result.d";
import { Image } from 'antd';



const Account: React.FC = ({formRef , intl}) => {
  const [ imageData, setImageData] = useState<string>('');
  const [ captchaEnabled, setCaptchaEnabled] = useState<boolean>(true);

  const handleGetCode = () => {
    getCode().then((data: CodeResult) => {
      setImageData('data:image/gif;base64,' + data.img);
      setCaptchaEnabled(data.captchaEnabled);
      formRef?.current?.setFieldsValue({
        uuid: data.uuid,
      });
    });
  };

  useEffect(() => {
    handleGetCode();
  }, []);

  return (
    <>
      <ProFormText hidden={true} name="uuid" />
      <ProFormText
        name="username"
        fieldProps={{
          size: 'large',
          prefix: <UserOutlined className={styles.prefixIcon}/>,
          style: { color: '#319cff' },
        }}
        placeholder={intl.formatMessage({
          id: 'pages.login.username.placeholder',
          defaultMessage: '用户名: admin or user',
        })}
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.login.username.required"
                defaultMessage="请输入用户名!"
              />
            ),
          },
        ]}
      />
      <ProFormText.Password
        name="password"
        fieldProps={{
          size: 'large',
          prefix: <LockOutlined className={styles.prefixIcon}/>,
          style: { color: '#319cff' },
        }}
        placeholder={intl.formatMessage({
          id: 'pages.login.password.placeholder',
          defaultMessage: '密码: ant.design',
        })}
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.login.password.required"
                defaultMessage="请输入密码！"
              />
            ),
          },
        ]}
      />
      <ProFormText
        name="code"
        fieldProps={{
          size: 'large',
          style: {
            color: '#319cff',
          },
          prefix: <SafetyCertificateOutlined style={{ color: '#319cff' }} />,
        }}
        placeholder={intl.formatMessage({
          id: 'pages.login.code.placeholder',
          defaultMessage: '请输入验证码',
        })}
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.login.code.required"
                defaultMessage="请输入验证码!"
              />
            ),
          },
        ]}
        colProps={{span: 18}}
      />

      {captchaEnabled && (
        <Image
          src={imageData}
          onClick={handleGetCode}
          preview={false}
          width="23%"
          style={{ height: '40px', verticalAlign: 'middle', marginLeft: '2%' }}
        />
      )}
    </>
  )
}

export default Account;
