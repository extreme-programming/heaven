import {
  PageContainer,
  ProCard
} from '@ant-design/pro-components';
import React, { useState} from 'react';
import {  useIntl } from 'umi';
import RcResizeObserve from 'rc-resize-observer';
import UserDeptTree from "@/pages/system/User/components/DeptTree";
import UserTable from "@/pages/system/User/components/Table";


const UserInfo: React.FC = () => {
  const intl = useIntl();
  const [responsive ,setResponsive] = useState(false);

  // @ts-ignore
  // @ts-ignore
  return (
    <RcResizeObserve key="resize-observe" onResize={(offset) => {setResponsive(offset.width < 596)}}>
      <PageContainer>
        <ProCard gutter={[16,16]}>
          <ProCard colSpan="20%" headerBordered bordered>
            <div>
              <UserDeptTree intl={intl}/>
            </div>
          </ProCard>
          <ProCard bordered headerBordered>
            <div>
              <UserTable intl={intl}/>
            </div>
          </ProCard>
        </ProCard>
      </PageContainer>
    </RcResizeObserve>
  );
};

export default UserInfo;
