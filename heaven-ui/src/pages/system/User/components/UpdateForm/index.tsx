import React, { useRef } from "react";
import {
  ModalForm,
  ProFormRadio,
  ProFormSelect,
  ProFormText,
  ProFormTextArea,
  ProFormTreeSelect
} from "@ant-design/pro-components";
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {message} from "antd";
import {ProFormInstance} from "@ant-design/pro-form";
import {User} from "@/services/ant-design-pro/system/User/result";
import {getTreeList, handleTree} from "@/services/ant-design-pro/system/Dept/api";
import {editUser} from "@/services/ant-design-pro/system/User/api";


/**
 * @en-US Update node
 * @zh-CN 更新节点
 *
 * @param fields
 */
const handleUpdate = async (fields: User) => {
  const hide = message.loading('正在修改');
  const result = await editUser(fields);
  try {
    message.success(result.msg);
    hide();
    return true;
  } catch (error) {
    hide();
    message.error(result.msg);
    return false;
  }
};

// @ts-ignore
const UpdateForm: React.FC = ({actionRef , intl  ,updateModalVisible , handleUpdateModalVisible ,currentRow }) => {

  const formRef = useRef<ProFormInstance>()

  // @ts-ignore
  return (
    <>
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.system.user.addForm.title',
          defaultMessage: 'New User',
        })}
        modalProps={{
          destroyOnClose: true
        }}
        key="addForm"
        grid={true}
        visible={updateModalVisible}
        onVisibleChange={handleUpdateModalVisible}
        onFinish={async (value) => {
          const success = await handleUpdate(value as User);
          if (success) {
            handleUpdateModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.user.nickName"
                  defaultMessage="User nickName"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.user.nickName" defaultMessage="User nickName"/>}
          width="md"
          name="nickName"
          colProps={{span: 12}}
          initialValue={''+ currentRow?.user.nickName || null}
        />

        <ProFormTreeSelect
          label={<FormattedMessage id="pages.searchTable.system.user.deptId" defaultMessage="User dept"/>}
          width="md"
          name="deptId"
          request={async () => {
            const response = await getTreeList()
            const {  data } = response;
            const deptList = handleTree(data, "deptId" , "deptName");
            return deptList;
          }}
          required={true}
          colProps={{span: 12}}
          initialValue={''+ currentRow?.user.deptId || null}
        />

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.user.phonenumber"
                  defaultMessage="User phonenumber"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.user.phonenumber"
                                   defaultMessage="User phonenumber"/>}
          width="md"
          name="phonenumber"
          colProps={{span: 12}}
          initialValue={''+ currentRow?.user.phonenumber || null}
        />

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.user.email"
                  defaultMessage="User email"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.user.email"
                                   defaultMessage="User email"/>}
          width="md"
          name="email"
          colProps={{span: 12}}
          initialValue={''+ currentRow?.user.email || null}
        />

        <ProFormRadio.Group
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.user.sex"
                  defaultMessage="User sex"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.user.sex" defaultMessage="User sex"/>}
          width="md"
          name="sex"
          options={[
            {
              label: "男",
              value: "0"
            },
            {
              label: "女",
              value: "1"
            },
            {
              label: "未知",
              value: "2"
            }
          ]}
          initialValue={''+ currentRow?.user.sex || null}
          // onChange={menuTypeChange}
          colProps={{span: 12}}
        />

        <ProFormRadio.Group
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.user.status"
                  defaultMessage="User status"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.user.status" defaultMessage="User status"/>}
          width="md"
          name="status"
          options={[
            {
              label: "正常",
              value: "0"
            },
            {
              label: "停用",
              value: "1"
            }
          ]}
          // onChange={menuTypeChange}
          colProps={{span: 12}}
          initialValue={''+ currentRow?.user.status || null}
        />

        <ProFormSelect.SearchSelect
          name="postIds"
          label={<FormattedMessage id="pages.searchTable.system.user.post" defaultMessage="User post"/>}
          debounceTime={300}
          request={async () => {
            const data = []
            currentRow?.posts.map((item)=>{
              data.push({label:item.postName , value:item.postId});
            })
            return data;
          }}
          colProps={{span: 12}}
          //  修改为false 提交的postIds值为value[]
          fieldProps={{
            labelInValue: false
          }}
          initialValue={currentRow?.postIds || null}
        />

        <ProFormSelect.SearchSelect
          name="roleIds"
          label={<FormattedMessage id="pages.searchTable.system.user.roleId" defaultMessage="User role"/>}
          debounceTime={300}
          request={async () => {
            const data = []
            currentRow?.roles.map((item)=>{
              data.push({label:item.roleName , value:item.roleId});
            })
            return data;
          }}
          colProps={{span: 12}}
          fieldProps={{
            labelInValue: false
          }}
          initialValue={currentRow?.roleIds || null}
        />

        <ProFormTextArea
          name="remark"
          label={<FormattedMessage id="pages.searchTable.system.user.remark" defaultMessage="User remark"/>}
          initialValue={''+ currentRow?.user.remark || null}
        />

        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.system.user.userId" defaultMessage="User id" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.user.userId" defaultMessage="User id"/>}
          width="md"
          name="userId"
          initialValue={"" +currentRow?.user.userId || null}
          hidden={true}
        />

        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.system.user.userName" defaultMessage="User userName" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.user.userName" defaultMessage="User userName"/>}
          width="md"
          name="userName"
          initialValue={"" +currentRow?.user.userName || null}
          hidden={true}
        />
      </ModalForm>

    </>
  )
}

export default UpdateForm;
