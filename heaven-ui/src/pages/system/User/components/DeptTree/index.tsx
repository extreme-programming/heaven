import React, {useEffect, useRef, useState} from "react";
import {Input, Tree} from "antd";
import {getTreeList, handleTree} from "@/services/ant-design-pro/system/Dept/api";
// @ts-ignore
import { connect } from 'dva';

const {Search} = Input;

const UserDeptTree: React.FC = ({intl ,dispatch }) => {
  const [treeData, setTreeData] = useState<[]>();
  const [listData, setListData] = useState<[]>();
  const treeRef = useRef(null);

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const list = [];
    const { value } = e.target;
    listData.map(item => {
      if (item.title.indexOf(value) > -1) {
        list.push(item);
      }
    })
    const deptList = handleTree(list, "deptId" , "deptName");
    setTreeData(deptList);
  };

  const onSelect  = (selectedKeys , e: {selected: boolean , selectedNodes , node , event}) => {
    console.log(e.node)
    dispatch({
      type: 'user/setDeptId',
      payload: e.node.deptId,
    });
    dispatch({
      type: 'user/setCurrentNode',
      payload: e,
    });
    e.selected = false;
    console.log(treeRef);
  }

  // @ts-ignore
  useEffect(async () => {
    const response = await getTreeList();
    const {  data } = response;
    const deptList = handleTree(data, "deptId" , "deptName");
    // @ts-ignore
    setTreeData(deptList)
    // @ts-ignore
    setListData(data)
  },[])

  return (
    <>
      <Search style={{marginBottom: 8}} placeholder={intl.formatMessage({
        id: 'pages.searchTable.system.user.tree.search',
        defaultMessage: 'tree search',
      })} onChange={onChange}/>
      <Tree
        autoExpandParent={true}
        treeData={treeData === undefined ? [] : treeData}
        showLine={true}
        defaultExpandAll={true}
        onSelect={onSelect}
        ref={treeRef}
      />
    </>
  )
}

// export default UserDeptTree;

export default connect(({ user }) => ({
  deptId: user.deptId,
  currentNode: user.currentNode
}))(UserDeptTree);
