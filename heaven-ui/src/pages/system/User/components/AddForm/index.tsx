import React from "react";
import {ModalForm, ProFormRadio, ProFormSelect, ProFormText, ProFormTreeSelect,ProFormTextArea} from "@ant-design/pro-components";
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {RoleItem, RoleItemPageList} from "@/services/ant-design-pro/system/Role/result";
import {message } from "antd";
import {ResultCode} from "@/pages/constants";
import {getTreeList, handleTree} from "@/services/ant-design-pro/system/Dept/api";
import { User } from "@/services/ant-design-pro/system/User/result";
import { getPostList} from "@/services/ant-design-pro/system/Post/api";
import { getRoleList} from "@/services/ant-design-pro/system/Role/api";
import {PostItem, PostItemPageList} from "@/services/ant-design-pro/system/Post/result";
import {addUser} from "@/services/ant-design-pro/system/User/api";

const handleAdd = async (fields: User) => {
  const hide = message.loading('正在添加');
  const result = await addUser({ ...fields });
  try {
    message.success(result.msg);
    hide();
    return true;
  } catch (error) {
    hide();
    message.error(result.msg);
    return false;
  }
};

const handleGetPost = async () => {
  const response: PostItemPageList = await getPostList({status:0},{});
  try {
    if(response.code === ResultCode.SUCCESS_CODE){
      if(response.rows){
        const data: PostItem[] = response.rows;
        const result = [];
        for (const resultElement of data) {
          result.push({label:resultElement.postName , value:resultElement.postId})
        }
        return result;
      }
      return [];
    }
    return [];
  } catch (error) {
    message.error(response.msg);
    return [];
  }
}

const handleGetRole = async () => {
  const response: RoleItemPageList = await getRoleList({status:0},{});
  try {
    if(response.code === ResultCode.SUCCESS_CODE){
      if(response.rows){
        const data: RoleItem[] = response.rows;
        const result = [];
        for (const resultElement of data) {
          result.push({label:resultElement.roleName , value:resultElement.roleId})
        }
        return result;
      }
      return [];
    }
    return [];
  } catch (error) {
    message.error(response.msg);
    return [];
  }
}

// @ts-ignore
const AddForm: React.FC = ({actionRef , intl  ,createModalVisible , handleModalVisible }) => {
  return (
    <>
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.system.user.addForm.title',
          defaultMessage: 'New User',
        })}
        modalProps={{
          destroyOnClose: true
        }}
        key="addForm"
        grid={true}
        visible={createModalVisible}
        onVisibleChange={handleModalVisible}
        onFinish={async (value) => {
          const success = await handleAdd(value as User);
          if (success) {
            handleModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.user.nickName"
                  defaultMessage="User nickName"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.user.nickName" defaultMessage="User nickName"/>}
          width="md"
          name="nickName"
          colProps={{span: 12}}
        />

        <ProFormTreeSelect
          label={<FormattedMessage id="pages.searchTable.system.user.deptId" defaultMessage="User dept"/>}
          width="md"
          name="deptId"
          request={async () => {
            const response = await getTreeList()
            const {  data } = response;
            const deptList = handleTree(data, "deptId" , "deptName");
            return deptList;
          }}
          required={true}
          colProps={{span: 12}}
        />

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.user.phonenumber"
                  defaultMessage="User phonenumber"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.user.phonenumber"
                                   defaultMessage="User phonenumber"/>}
          width="md"
          name="phonenumber"
          colProps={{span: 12}}
        />

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.user.email"
                  defaultMessage="User email"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.user.email"
                                   defaultMessage="User email"/>}
          width="md"
          name="email"
          colProps={{span: 12}}
        />

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.user.userName"
                  defaultMessage="User userName"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.user.userName"
                                   defaultMessage="User userName"/>}
          width="md"
          name="userName"
          colProps={{span: 12}}
        />

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.user.password"
                  defaultMessage="User password"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.user.password"
                                   defaultMessage="User password"/>}
          width="md"
          name="password"
          colProps={{span: 12}}
        />

        <ProFormRadio.Group
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.user.sex"
                  defaultMessage="User sex"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.user.sex" defaultMessage="User sex"/>}
          width="md"
          name="sex"
          options={[
            {
              label: "男",
              value: 0
            },
            {
              label: "女",
              value: 1
            },
            {
              label: "未知",
              value: 2
            }
          ]}
          initialValue={0}
          // onChange={menuTypeChange}
          colProps={{span: 12}}
        />

        <ProFormRadio.Group
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.user.status"
                  defaultMessage="User status"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.user.status" defaultMessage="User status"/>}
          width="md"
          name="status"
          options={[
            {
              label: "正常",
              value: 0
            },
            {
              label: "停用",
              value: 1
            }
          ]}
          initialValue={0}
          // onChange={menuTypeChange}
          colProps={{span: 12}}
        />

        <ProFormSelect.SearchSelect
          name="postIds"
          label={<FormattedMessage id="pages.searchTable.system.user.post" defaultMessage="User post"/>}
          debounceTime={300}
          request={handleGetPost}
          colProps={{span: 12}}

          //  修改为false 提交的postIds值为value[]
          fieldProps={{
            labelInValue: false
          }}
        />

        <ProFormSelect.SearchSelect
          name="roleIds"
          label={<FormattedMessage id="pages.searchTable.system.user.roleId" defaultMessage="User role"/>}
          debounceTime={300}
          request={handleGetRole}
          colProps={{span: 12}}
          fieldProps={{
            labelInValue: false
          }}
          // initialValue={['all','open']}
        />

        <ProFormTextArea
          name="remark"
          label={<FormattedMessage id="pages.searchTable.system.user.remark" defaultMessage="User remark"/>}
        />
      </ModalForm>
    </>
  )
}

export default AddForm;
