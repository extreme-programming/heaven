import React, {useRef, useState} from "react";
import {Button, message,Space} from "antd";
import {ActionType, FooterToolbar, ProColumns, ProTable} from "@ant-design/pro-components";
import {User, UserResult} from "@/services/ant-design-pro/system/User/result";
import {EditOutlined, PlusOutlined} from "@ant-design/icons";
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {PAGE_SIZE_ARRAY} from "@/pages/constants";
import {getList, getUser, removeUser} from "@/services/ant-design-pro/system/User/api";
import UpdateForm from "@/pages/system/User/components/UpdateForm";
import AddForm from "@/pages/system/User/components/AddForm";
import { connect } from 'dva';
import {TableListIsSuccessEnum} from "@/pages/enums";

const handleRemove = async (selectedRows: User[]) => {
  const hide = message.loading('正在删除');
  if (!selectedRows) return true;
  const params: number[] = [];
  selectedRows.map((item) => {
    params.push(item.userId)
  })
  try {
    const result = await removeUser(params);
    hide();
    message.success(result.msg);
    return true;
  } catch (error) {
    hide();
    message.error('Delete failed, please try again');
    return false;
  }
};

const UserTable: React.FC = ({intl , deptId , dispatch , currentNode}) => {
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<User>();
  const [selectedRowsState, setSelectedRows] = useState<User[]>([]);

  const columns: ProColumns<User>[] = [
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.system.user.avatar"
          defaultMessage="User avatar"
        />
      ),
      valueType: 'avatar',
      align: 'left',
      dataIndex: 'avatar',
      key: 'avatar',
      hideInSearch: true,
      hideInTable: false,
      render: (dom,entity) => (
        <Space>
          <span>{dom}</span>
            <a href = {entity.avatar} target="_blank">{entity.nickName}</a>
        </Space>
      )
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.system.user.userName"
          defaultMessage="User name"
        />
      ),
      align: 'center',
      dataIndex: 'userName',
      key: 'userName',
      tip: intl.formatMessage({
        id: 'pages.searchTable.unique',
        defaultMessage: 'value unique',
      }),
      renderText: (val: string) => {
        return val
      }
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.system.user.nickName"
          defaultMessage="Nick name"
        />
      ),
      align: 'center',
      dataIndex: 'nickName',
      key: 'nickName',
      hideInSearch: true,
      hideInTable: true,
      renderText: (val: string) => {
        return val
      }
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.system.user.deptId"
          defaultMessage="User dept"
        />
      ),
      align: 'center',
      dataIndex: 'dept',
      key: 'dept',
      hideInSearch: true,
      hideInTable: false,
      renderText: (val) => {
        return val['deptName']
      }
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.system.user.phonenumber"
          defaultMessage="User phonenumber"
        />
      ),
      align: 'center',
      dataIndex: 'phonenumber',
      key: 'phonenumber',
      hideInSearch: false,
      hideInTable: false,
      renderText: (val: string) => {
        return val
      }
    },

    {
      title: (
        <FormattedMessage
          id="pages.searchTable.system.user.status"
          defaultMessage="User status"
        />
      ),
      dataIndex: 'status',
      align: 'center',
      valueType: 'select',
      key: 'status',
      renderText: (val: string) => {
        return val
      }
      ,
      valueEnum: TableListIsSuccessEnum,
    },

    {
      title: (
        <FormattedMessage
          id="pages.searchTable.system.user.CreateTime"
          defaultMessage="User create time"
        />
      ),
      render: (_, record) => <span>{record.createTime}</span>,
      search: {
        transform: (value) => {
          return {
            'params[beginTime]': value[0],
            'params[endTime]': value[1],
          }
        }
      },
      dataIndex: 'createTime',
      align: 'center',
      valueType: 'dateRange',
      key: 'createTime',
      renderText: (val: string) => {
        return val;
      }
      ,
    },
    {
      title: intl.formatMessage({
          id: 'pages.searchTable.titleOption',
          defaultMessage: 'Enquiry Option',
        }),
      dataIndex: 'option',
      valueType: 'option',
      key: 'update',
      align: 'center',
      render: (_, record) => [
        <Button
          type="primary"
          onClick={async () => {
            const result: UserResult = await getUser(record.userId);
            setCurrentRow(result.data);
            handleUpdateModalVisible(true);
          }}
          shape="round"
        >
          <EditOutlined/> <FormattedMessage id="pages.searchTable.update" defaultMessage="Update"/>
        </Button>
      ],
    },
  ];

  return (
    <>
      <ProTable<User, API.Param.BasePageParam>
        headerTitle={intl.formatMessage({
          id: 'pages.searchTable.title',
          defaultMessage: 'Enquiry form',
        })}
        actionRef={actionRef}
        rowKey="userId"
        search={{
          labelWidth: 120,
          optionRender:({form})=>{
            return [
              <Button key="resetKey" onClick={()=>{
                actionRef.current?.reset();
                dispatch({
                  type: 'user/setDeptId',
                  payload: undefined,
                });
                dispatch({
                  type: 'user/setCurrentNode',
                  payload: currentNode,
                });
              }}>
                <FormattedMessage id="pages.searchTable.reset" defaultMessage="table reset"/>
              </Button>,
              <Button key="select" onClick={()=>{
                columns[2].initialValue = deptId;
                form?.submit();
              }} type={"primary"}>
                <FormattedMessage id="pages.searchTable.select" defaultMessage="table search"/>
              </Button>,

            ];}
        }}
        toolBarRender={() => [
          <Button
            type="primary"
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <PlusOutlined/> <FormattedMessage id="pages.searchTable.new" defaultMessage="New"/>
          </Button>,
        ]}
        params={{deptId:deptId}}
        request={ async (params ,sort , filter) => {
            return await getList(params);
          }
        }
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
          type: 'checkbox'
        }}
        pagination={{
          showSizeChanger: true,
          showQuickJumper: true,
          pageSizeOptions: PAGE_SIZE_ARRAY
        }}
      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.searchTable.chosen" defaultMessage="Chosen"/>{' '}
              <a style={{fontWeight: 600}}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.searchTable.item" defaultMessage="项"/>
            </div>
          }
        >
          <Button
            onClick={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
          >
            <FormattedMessage
              id="pages.searchTable.batchDeletion"
              defaultMessage="Batch deletion"
            />
          </Button>
        </FooterToolbar>
      )}

      <AddForm actionRef={actionRef}
             intl={intl}
             createModalVisible={createModalVisible}
             handleModalVisible={handleModalVisible}
      />

      <UpdateForm actionRef={actionRef}
             intl={intl}
             updateModalVisible={updateModalVisible}
             handleUpdateModalVisible={handleUpdateModalVisible}
             currentRow={currentRow}
      />
    </>
  )
}

export default connect(({ user }) => ({
  deptId: user.deptId,
  currentNode: user.currentNode
}))(UserTable);
