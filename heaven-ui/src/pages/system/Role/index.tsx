import {PlusOutlined, EditOutlined, SmileOutlined} from '@ant-design/icons';
import {ActionType, ProColumns } from '@ant-design/pro-components';
import {
  FooterToolbar,
  PageContainer,
  ProTable,
} from '@ant-design/pro-components';
import { Button, message } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage, useIntl } from 'umi';
import request from "@/services/ant-design-pro/request";
import { getRole, removeRole ,getRoleList } from "@/services/ant-design-pro/system/Role/api";
import {PAGE_SIZE_ARRAY} from "@/pages/constants"
import {RoleItem, RoleItemResult} from "@/services/ant-design-pro/system/Role/result";
import AddForm from "@/pages/system/Role/componnets/AddForm";
import UpdateForm from "@/pages/system/Role/componnets/UpdateForm";
import {MenuItemForRoleResult, MenuItemResult} from "@/services/ant-design-pro/system/Menu/result";
import {handleTree} from "@/services/ant-design-pro/system/Dept/api";
import {getTreeListForRole} from "@/services/ant-design-pro/system/Menu/api";
import {TableListIsSuccessEnum} from "@/pages/enums";


/**
 *  Delete node
 * @zh-CN 删除节点
 *
 * @param selectedRows
 */
const handleRemove = async (selectedRows: RoleItem[]) => {
  const hide = message.loading('正在删除');
  if (!selectedRows) return true;

  const params: number[] = [];
  selectedRows.map((item) =>{
    params.push(item.roleId)
  })
  try {
    const result = await removeRole(params);
    hide();
    message.success(result.msg);
    return true;
  } catch (error) {
    hide();
    message.error('Delete failed, please try again');
    return false;
  }
};

const Role: React.FC = () => {
  /**
   * @en-US Pop-up window of new window
   * @zh-CN 新建窗口的弹窗
   *  */
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  /**
   * @en-US The pop-up window of the distribution update window
   * @zh-CN 分布更新窗口的弹窗
   * */
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<RoleItem>();
  const [selectedRowsState, setSelectedRows] = useState<RoleItem[]>([]);

  const [treeValues, setTreeValues] = useState<string[]>();

  /**
   * @en-US International configuration
   * @zh-CN 国际化配置
   * */
  const intl = useIntl();

  const columns: ProColumns<RoleItem>[] = [
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.system.role.RoleName"
          defaultMessage="Rule name"
        />
      ),
      align: 'center',
      dataIndex: 'roleName',
      key: 'roleName',
      renderText: (val: string) => {return val}
    },
    {
      title: <FormattedMessage id="pages.searchTable.system.role.RoleKey" defaultMessage="role key" />,
      dataIndex: 'roleKey',
      align: 'center',
      key: 'roleKey'
    },
    {
      title: <FormattedMessage id="pages.searchTable.system.role.Remark" defaultMessage="role remark" />,
      dataIndex: 'remark',
      align: 'center',
      hideInSearch: true,
      key: 'remark',
      hideInForm: true
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.system.role.Status"
          defaultMessage="Role status"
        />
      ),
      dataIndex: 'status',
      align: 'center',
      valueType: 'select',
      key: 'status',
      renderText: (val: string) => {return val}
      ,
      valueEnum: TableListIsSuccessEnum,
    }
    ,
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.system.role.CreateTime"
          defaultMessage="role create time"
        />
      ),
      render: (_, record) => <span>{record.createTime}</span>,
      search: {
        transform: (value) => {
          return {
            'params[beginTime]': value[0],
            'params[endTime]': value[1],
          }
        }
      },
      dataIndex: 'createTime',
      align: 'center',
      valueType: 'dateRange',
      key: 'createTime',
      renderText: (val: string) => {
        return val;
      }
      ,
    }
    ,
    {
      title: intl.formatMessage({
        id: 'pages.searchTable.titleOption',
        defaultMessage: 'Enquiry Option',
      }),
      dataIndex: 'option',
      valueType: 'option',
      key: 'update',
      align: 'center',
      render: (_, record) => [
        <Button
          type="primary"
          onClick={async () => {
            const result: RoleItemResult = await getRole(record.roleId);
            const treeValues: MenuItemForRoleResult = await getTreeListForRole({roleId:record.roleId});
            setTreeValues(treeValues.data.checkedKeys);
            setCurrentRow(result.data);
            handleUpdateModalVisible(true);
          }}
          shape="round"
        >
          <EditOutlined />  <FormattedMessage id="pages.searchTable.update" defaultMessage="Update"/>
        </Button>
      ],
    },
  ];

  // @ts-ignore
  return (
    <PageContainer>
      <ProTable<RoleItem, API.Param.BasePageParam>
        headerTitle={intl.formatMessage({
          id: 'pages.searchTable.title',
          defaultMessage: 'Enquiry form',
        })}
        actionRef={actionRef}
        rowKey="roleId"
        search={{
          labelWidth: 120,
        }}
        toolBarRender={() => [
          <Button
            type="primary"
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <PlusOutlined /> <FormattedMessage id="pages.searchTable.new" defaultMessage="New"/>
          </Button>,
        ]}
        request={getRoleList}
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
          type :'checkbox'
        }}
        pagination={{
          showSizeChanger: true,
          showQuickJumper: true,
          pageSizeOptions: PAGE_SIZE_ARRAY
        }}
      />
      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.searchTable.chosen" defaultMessage="Chosen" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.searchTable.item" defaultMessage="项" />
            </div>
          }
        >
          <Button
            onClick={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
          >
            <FormattedMessage
              id="pages.searchTable.batchDeletion"
              defaultMessage="Batch deletion"
            />
          </Button>
        </FooterToolbar>
      )}

      <AddForm actionRef={actionRef}
             intl={intl}
             createModalVisible={createModalVisible}
             handleModalVisible={handleModalVisible}
      />

      <UpdateForm actionRef={actionRef}
             intl={intl}
             updateModalVisible={updateModalVisible}
             handleUpdateModalVisible={handleUpdateModalVisible}
             currentRow={currentRow}
             treeValues={treeValues}
      />
    </PageContainer>
  );
};

export default Role;
