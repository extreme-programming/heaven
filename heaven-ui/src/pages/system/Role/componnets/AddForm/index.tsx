import React, {useState} from "react";
import {ModalForm, ProFormSelect, ProFormText, ProFormTreeSelect} from "@ant-design/pro-components";
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {RoleItem} from "@/services/ant-design-pro/system/Role/result";
import {message , TreeSelect} from "antd";
import {addRole} from "@/services/ant-design-pro/system/Role/api";
import {ResultCode} from "@/pages/constants";
import {getTreeList} from "@/services/ant-design-pro/system/Menu/api";
import {handleTree} from "@/services/ant-design-pro/system/Dept/api";

const {SHOW_ALL} = TreeSelect;

const handleAdd = async (fields: RoleItem) => {
  const hide = message.loading('正在添加');
  const result = await addRole({ ...fields });
  try {
    if(result.code === ResultCode.SUCCESS_CODE){
      message.success(result.msg);
    }
    hide();
    return true;
  } catch (error) {
    hide();
    message.error(result.msg);
    return false;
  }
};

// @ts-ignore
const AddForm: React.FC = ({actionRef , intl  ,createModalVisible , handleModalVisible }) => {

  return (
    <>
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.system.role.addForm.title',
          defaultMessage: 'New Role',
        })}
        modalProps={{
          destroyOnClose: true
        }}
        key="addForm"
        width="400px"
        visible={createModalVisible}
        onVisibleChange={handleModalVisible}
        onFinish={async (value) => {
          const success = await handleAdd(value as RoleItem);

          if (success) {
            handleModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.role.RoleName"
                  defaultMessage="Role name"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.role.RoleName" defaultMessage="Role name"/>}
          width="md"
          name="roleName"
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="pages.searchTable.system.role.RoleKey" defaultMessage="Role key" />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.role.RoleKey" defaultMessage="Role key"/>}
          width="md"
          name="roleKey"
        />

        <ProFormSelect
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.role.Status"
                  defaultMessage="Role status"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.role.Status" defaultMessage="Role status"/>}
          width="md"
          name="status"
          valueEnum={{
            0: '正常',
            1: '停用'
          }}
        />

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.role.RoleSort"
                  defaultMessage="Role sort"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.role.RoleSort" defaultMessage="Role sort"/>}
          width="md"
          name="roleSort"
        />

        <ProFormTreeSelect
          label={<FormattedMessage id="pages.searchTable.system.role.Menus"
                                   defaultMessage="Role menu select"/>}
          name="menuIds"
          width="md"
          request={async () => {
            const response = await getTreeList();
            const {  data } = response;
            const deptList = handleTree(data, "menuId" , "menuName");
            return deptList;
          }}
          required={true}
          fieldProps={{
            treeCheckable:true,
            showCheckedStrategy: SHOW_ALL,
            treeLine: true,
            multiple: true,
          }}
        />

        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.system.role.Remark" defaultMessage="Role remark" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.role.Remark" defaultMessage="Role remark"/>}
          width="md"
          name="remark"
        />
      </ModalForm>
    </>
  )
}

export default AddForm;
