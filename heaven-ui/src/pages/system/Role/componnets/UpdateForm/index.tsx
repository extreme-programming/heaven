import React, { useRef } from "react";
import {ModalForm, ProFormSelect, ProFormText, ProFormTreeSelect} from "@ant-design/pro-components";
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {RoleItem} from "@/services/ant-design-pro/system/Role/result";
import {message, TreeSelect} from "antd";
import {editRole} from "@/services/ant-design-pro/system/Role/api";
import {ResultCode} from "@/pages/constants";
import {MenuItemForRoleResult} from "@/services/ant-design-pro/system/Menu/result";
import {getTreeListForRole} from "@/services/ant-design-pro/system/Menu/api";
import {ProFormInstance} from "@ant-design/pro-form";


/**
 * @en-US Update node
 * @zh-CN 更新节点
 *
 * @param fields
 */
const handleUpdate = async (fields: RoleItem) => {
  const hide = message.loading('正在修改');
  const result = await editRole(fields);
  try {
    if(result.code === ResultCode.SUCCESS_CODE){
      message.success(result.msg);
    }
    hide();
    return true;
  } catch (error) {
    hide();
    message.error(result.msg);
    return false;
  }
};

const {SHOW_ALL} = TreeSelect;

// @ts-ignore
const UpdateForm: React.FC = ({actionRef , intl  ,updateModalVisible , handleUpdateModalVisible ,currentRow , treeValues}) => {

  const formRef = useRef<ProFormInstance>()

  // @ts-ignore
  return (
    <>
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.system.role.updateForm.title',
          defaultMessage: 'Update Role',
        })}
        modalProps={{
          destroyOnClose: true
        }}
        key="addForm"
        grid={true}
        visible={updateModalVisible}
        onVisibleChange={handleUpdateModalVisible}
        onFinish={async (value) => {
          const success = await handleUpdate(value as RoleItem);

          if (success) {
            handleUpdateModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.role.RoleName"
                  defaultMessage="Role name"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.role.RoleName" defaultMessage="Role name"/>}
          width="md"
          name="roleName"
          initialValue={currentRow?.roleName || null}
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="pages.searchTable.system.role.RoleKey" defaultMessage="Role key" />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.role.RoleKey" defaultMessage="Role key"/>}
          width="md"
          name="roleKey"
          initialValue={currentRow?.roleKey || null}
        />

        <ProFormSelect
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.role.Status"
                  defaultMessage="Role status"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.role.Status" defaultMessage="Role status"/>}
          width="md"
          name="status"
          valueEnum={{
            0: '正常',
            1: '停用'
          }}
          initialValue={currentRow?.status || null}
        />

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.role.RoleSort"
                  defaultMessage="Role sort"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.role.RoleSort" defaultMessage="Role sort"/>}
          width="md"
          name="roleSort"
          initialValue={currentRow?.roleSort || null}
        />

        <ProFormTreeSelect
          label={<FormattedMessage id="pages.searchTable.system.role.Menus"
                                   defaultMessage="Role menu select"/>}
          name="menuIds"
          width="md"
          request={async ()=>{
            const res: MenuItemForRoleResult = await getTreeListForRole({roleId:currentRow.roleId});
            // 只能用initialValue + request回显树
            // formRef.current?.setFieldsValue({
            //   menuIds: res.data.checkedKeys
            // });
            return res.data.menus
          }}
          required={true}
          fieldProps={{
            treeCheckable:true,
            showCheckedStrategy: SHOW_ALL,
            treeLine: true,
            treeDefaultExpandAll: true,
          }}
          initialValue={treeValues === undefined ? [] : treeValues}
        />

        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.system.role.Remark" defaultMessage="Role remark" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.role.Remark" defaultMessage="Role remark"/>}
          width="md"
          name="remark"
          initialValue={currentRow?.remark || null}
        />

        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.system.role.RoleId" defaultMessage="Role id" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.role.RoleId" defaultMessage="Role id"/>}
          width="md"
          name="roleId"
          initialValue={"" +currentRow?.roleId || null}
          hidden={true}
        />
      </ModalForm>
    </>
  )
}

export default UpdateForm;
