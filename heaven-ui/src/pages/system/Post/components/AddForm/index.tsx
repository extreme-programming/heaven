// @ts-ignore
import React from "react";
import {ModalForm, ProFormSelect, ProFormText} from "@ant-design/pro-components";
// @ts-ignore
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {message} from "antd";
// @ts-ignore
import {ResultCode} from "@/pages/constants";
// @ts-ignore
import {addPost} from "@/services/ant-design-pro/system/Post/api";
// @ts-ignore
import { PostItem } from "@/services/ant-design-pro/system/Post/result";

/**
 * @en-US Add node
 * @zh-CN 添加节点
 * @param fields
 */
const handleAdd = async (fields: PostItem) => {
  const hide = message.loading('正在添加');
  const result = await addPost({ ...fields });
  try {
    if(result.code === ResultCode.SUCCESS_CODE){
      message.success(result.msg);
    }
    hide();
    return true;
  } catch (error) {
    hide();
    message.error(result.msg);
    return false;
  }
};

// @ts-ignore
const AddForm: React.FC = ({actionRef , intl  ,createModalVisible , handleModalVisible}) => {
  return (
    <>
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.system.post.addForm.title',
          defaultMessage: 'New Post',
        })}
        modalProps={{
          destroyOnClose: true
        }}
        key="addForm"
        width="400px"
        visible={createModalVisible}
        onVisibleChange={handleModalVisible}
        onFinish={async (value) => {
          const success = await handleAdd(value as PostItem);

          if (success) {

            handleModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.post.PostName"
                  defaultMessage="Post name"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.post.PostName" defaultMessage="Post name"/>}
          width="md"
          name="postName"
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="pages.searchTable.system.post.PostCode" defaultMessage="Post code" />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.post.PostCode" defaultMessage="Post code"/>}
          width="md"
          name="postCode"
        />

        <ProFormSelect
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.post.Status"
                  defaultMessage="Post status"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.post.Status" defaultMessage="Post status"/>}
          width="md"
          name="status"
          valueEnum={{
            0: '正常',
            1: '停用'
          }}
        />

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.post.PostSort"
                  defaultMessage="Dept post sort"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.post.PostSort" defaultMessage="Dept post sort"/>}
          width="md"
          name="postSort"
        />

        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.system.post.Remark" defaultMessage="Post remark" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.post.Remark" defaultMessage="Post remark"/>}
          width="md"
          name="remark"
        />
      </ModalForm>
    </>
  )
}

export default AddForm;
