// @ts-ignore
import React from "react";
import {ModalForm, ProFormSelect, ProFormText} from "@ant-design/pro-components";
// @ts-ignore
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {message} from "antd";
// @ts-ignore
import {ResultCode} from "@/pages/constants";
// @ts-ignore
import { PostItem } from "@/services/ant-design-pro/system/Post/result";
// @ts-ignore
import {editPost} from "@/services/ant-design-pro/system/Post/api";

/**
 * @en-US Update node
 * @zh-CN 更新节点
 *
 * @param fields
 */
const handleUpdate = async (fields: PostItem) => {
  const hide = message.loading('正在修改');
  const result = await editPost(fields);

  try {
    if(result.code === ResultCode.SUCCESS_CODE){
      message.success(result.msg);
    }
    hide();
    return true;
  } catch (error) {
    hide();
    message.error(result.msg);
    return false;
  }
}

// @ts-ignore
const UpdateForm: React.FC = ({actionRef , intl  ,updateModalVisible , handleUpdateModalVisible ,currentRow}) => {
  return (
    <>
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.system.post.updateForm.title',
          defaultMessage: 'Update post',
        })}
        modalProps={{
          destroyOnClose: true
        }}
        key="updateForm"
        width="400px"
        visible={updateModalVisible}
        onVisibleChange={handleUpdateModalVisible}
        onFinish={async (value) => {

          const success = await handleUpdate(value as PostItem);
          if (success) {
            handleUpdateModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.post.PostName"
                  defaultMessage="Post name"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.post.PostName" defaultMessage="Post name"/>}
          width="md"
          name="postName"
          initialValue={"" +currentRow?.postName || null}
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="pages.searchTable.system.post.PostCode" defaultMessage="Post code" />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.post.PostCode" defaultMessage="Post code"/>}
          width="md"
          name="postCode"
          initialValue={"" +currentRow?.postCode || null}
        />

        <ProFormSelect
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.post.Status"
                  defaultMessage="Post status"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.post.Status" defaultMessage="Post status"/>}
          width="md"
          name="status"
          valueEnum={{
            0: '正常',
            1: '停用'
          }}
          initialValue={"" +currentRow?.status || null}
        />

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.system.post.PostSort"
                  defaultMessage="Dept post sort"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.post.PostSort" defaultMessage="Dept post sort"/>}
          width="md"
          name="postSort"
          initialValue={"" +currentRow?.postSort || null}
        />

        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.system.post.Remark" defaultMessage="Post remark" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.post.Remark" defaultMessage="Post remark"/>}
          width="md"
          name="remark"
          initialValue={"" +currentRow?.remark || null}
        />

        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.system.PostId" defaultMessage="Post id" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.system.PostId" defaultMessage="Post id"/>}
          width="md"
          name="postId"
          initialValue={"" +currentRow?.postId || null}
          hidden={true}
        />
      </ModalForm>
    </>
  )
}

export default UpdateForm;
