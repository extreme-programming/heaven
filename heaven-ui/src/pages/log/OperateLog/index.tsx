import {ActionType, ProColumns } from '@ant-design/pro-components';
import ProDescriptions from '@ant-design/pro-descriptions';
import type {ProDescriptionsItemProps} from '@ant-design/pro-descriptions';
import {
  FooterToolbar,
  PageContainer,
  ProTable,
} from '@ant-design/pro-components';
import { Button, message,Tag, Drawer} from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage, useIntl } from 'umi';
import {PAGE_SIZE_ARRAY} from "@/pages/constants"
import {OperateLogItem} from "@/services/ant-design-pro/log/OperateLog/result";
import {getList, removeOperateLog} from "@/services/ant-design-pro/log/OperateLog/api";
import {TableListIsSuccessEnum} from "@/pages/enums";

/**
 *  Delete node
 * @zh-CN 删除节点
 *
 * @param selectedRows
 */
const handleRemove = async (selectedRows: OperateLogItem[]) => {
  const hide = message.loading('正在删除');
  if (!selectedRows) return true;

  const params: number[] = [];
  selectedRows.map((item) =>{
    params.push(item.operId)
  })
  try {
    const result = await removeOperateLog(params);
    hide();
    message.success(result.msg);
    return true;
  } catch (error) {
    hide();
    message.error('Delete failed, please try again');
    return false;
  }
};

//0=其它,1=新增,2=修改,3=删除,4=授权,5=导出,6=导入,7=强退,8=生成代码,9=清空数据
const OperateLog: React.FC = () => {
  const bussinessTypeMap = {
    0: {
      color: 'grey',
      text: '其它'
    },
    1: {
      color: 'green',
      text: '新增'
    },
    2: {
      color: 'blue',
      text: '修改'
    },
    3: {
      color: 'red',
      text: '删除'
    },
    4: {
      color: 'purple',
      text: '授权'
    },
    5: {
      color: 'silver',
      text: '导出'
    },
    6: {
      color: 'silver',
      text: '导入'
    },
    7: {
      color: 'pink',
      text: '强退'
    },
    8: {
      color: 'yellow',
      text: '生成代码'
    },
    9: {
      color: 'orange',
      text: '清空数据'
    },
    10:{
      color: 'brown',
      text: '数据源测试连接'
    }
  }
  // 0=其它,1=后台用户,2=手机端用户
  const operatorTypeMap = {
      0: {
        color: 'grey',
          text: '其它'
      },
      1: {
        color: 'green',
          text: '后台用户'
      },
      2: {
        color: 'blue',
          text: '手机端用户'
      }
  }

  const actionRef = useRef<ActionType>();
  const [selectedRowsState, setSelectedRows] = useState<OperateLogItem[]>([]);

  const [showDetail, setShowDetail] = useState<boolean>(false);
  const [currentRow, setCurrentRow] = useState<OperateLogItem>();
  /**
   * @en-US International configuration
   * @zh-CN 国际化配置
   * */
  const intl = useIntl();

  const columns: ProColumns<OperateLogItem>[] = [
    {
      title: <FormattedMessage id="pages.searchTable.log.operateLog.operId" defaultMessage="operateLog operId" />,
      dataIndex: 'operId',
      align: 'center',
      key: 'operId',
      hideInForm: true,
      hideInTable: false,
      hideInSearch: true,
      render: (dom , entity) => {
        return (
          <a onClick={()=>{
            setCurrentRow(entity);
            setShowDetail(true);
          }}
          >
            {dom}
          </a>
        )
      }
    },
    {
      title: <FormattedMessage id="pages.searchTable.log.operateLog.operName" defaultMessage="operateLog operName" />,
      dataIndex: 'operName',
      align: 'center',
      hideInSearch: false,
      key: 'operName',
      hideInForm: true
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.log.operateLog.title"
          defaultMessage="operateLog title"
        />
      ),
      align: 'center',
      dataIndex: 'title',
      key: 'title',
      renderText: (val: string) => {return val}
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.log.operateLog.businessType"
          defaultMessage="operateLog businessType"
        />
      ),
      dataIndex: 'bussinessTypeFlag',
      align: 'center',
      key: 'bussinessTypeFlag',
      hideInForm: false,
      hideInTable: false,
      hideInSearch: true,
      hideInDescriptions: false,
      render: (_, record) => <Tag color = {record.bussinessTypeFlag.color}>{record.bussinessTypeFlag.text}</Tag>
    }
    ,
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.log.operateLog.businessType"
          defaultMessage="operateLog businessType"
        />
      ),
      dataIndex: 'businessType',
      align: 'center',
      valueType: 'select',
      key: 'businessType',
      hideInSearch: false,
      hideInForm: true,
      hideInTable: true,
      hideInDescriptions: true,
      renderText: (val: string) => {return val}
      ,
      valueEnum: bussinessTypeMap,
    }
    ,
    {
      title: <FormattedMessage id="pages.searchTable.log.operateLog.requestMethod" defaultMessage="operateLog requestMethod" />,
      dataIndex: 'requestMethod',
      align: 'center',
      key: 'requestMethod',
      hideInSearch: true,
      hideInForm: false,
      hideInTable: false,
    },
    {
      title: <FormattedMessage id="pages.searchTable.log.operateLog.operatorType" defaultMessage="operateLog operatorType" />,
      dataIndex: 'operatorTypeFlag',
      align: 'center',
      hideInSearch: true,
      key: 'operatorTypeFlag',
      hideInForm: true,
      render: (_, record) => <Tag color = {record.operatorTypeFlag.color}>{record.operatorTypeFlag.text}</Tag>
    },
    {
      title: <FormattedMessage id="pages.searchTable.log.operateLog.method" defaultMessage="operateLog method" />,
      dataIndex: 'method',
      align: 'center',
      key: 'method',
      hideInForm: true,
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: <FormattedMessage id="pages.searchTable.log.operateLog.operUrl" defaultMessage="operateLog operUrl" />,
      dataIndex: 'operUrl',
      align: 'center',
      key: 'operUrl',
      hideInForm: true,
      hideInSearch: true,
      hideInTable: true
    },
    {
      title: <FormattedMessage id="pages.searchTable.log.operateLog.operIp" defaultMessage="operateLog operIp" />,
      dataIndex: 'operIp',
      align: 'center',
      key: 'operIp',
      hideInForm: true,
      hideInSearch: true,
      hideInTable: true
    },
    {
      title: <FormattedMessage id="pages.searchTable.log.operateLog.operLocation" defaultMessage="operateLog operLocation" />,
      dataIndex: 'operLocation',
      align: 'center',
      key: 'operLocation',
      hideInForm: true,
      hideInSearch: true,
      hideInTable: true
    },
    {
      title: <FormattedMessage id="pages.searchTable.log.operateLog.operParam" defaultMessage="operateLog operParam" />,
      dataIndex: 'operParam',
      align: 'center',
      key: 'operParam',
      hideInForm: true,
      hideInSearch: true,
      hideInTable: true,
      valueType: 'jsonCode'
    },
    {
      title: <FormattedMessage id="pages.searchTable.log.operateLog.jsonResult" defaultMessage="operateLog jsonResult" />,
      dataIndex: 'jsonResult',
      align: 'center',
      key: 'jsonResult',
      hideInForm: true,
      hideInSearch: true,
      hideInTable: true,
      valueType: 'jsonCode',
    },
    {
      title: <FormattedMessage id="pages.searchTable.log.operateLog.errorMsg" defaultMessage="operateLog errorMsg" />,
      dataIndex: 'errorMsg',
      align: 'center',
      key: 'errorMsg',
      hideInForm: true,
      hideInSearch: true,
      hideInTable: true,
      valueType: 'jsonCode',
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.log.logininfor.status"
          defaultMessage="logininfor status"
        />
      ),
      dataIndex: 'status',
      align: 'center',
      valueType: 'select',
      key: 'status',
      renderText: (val: string) => {return val}
      ,
      valueEnum: TableListIsSuccessEnum,
    }
    ,
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.log.operateLog.operTime"
          defaultMessage="operateLog operTime"
        />
      ),
      render: (_, record) => <span>{record.operTime}</span>,
      search: {
        transform: (value) => {
          return {
            'params[beginTime]': value[0],
            'params[endTime]': value[1],
          }
        }
      },
      dataIndex: 'operTime',
      align: 'center',
      valueType: 'dateRange',
      key: 'operTime',
      renderText: (val: string) => {
        return val;
      }
      ,
    }
  ];

  // @ts-ignore
  // @ts-ignore
  return (
    <PageContainer>
      <ProTable<OperateLogItem, API.Param.BasePageParam>
        headerTitle={intl.formatMessage({
          id: 'pages.searchTable.title',
          defaultMessage: 'Enquiry form',
        })}
        actionRef={actionRef}
        rowKey="operId"
        search={{
          labelWidth: 120,
        }}
        request={getList}
        postData={(data)=>{
          data.map((item)=>{
            item.bussinessTypeFlag = bussinessTypeMap[item.businessType]
            item.operatorTypeFlag = operatorTypeMap[item.operatorType]
          })
          return data;
        }}
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
          type :'checkbox'
        }}
        pagination={{
          showSizeChanger: true,
          showQuickJumper: true,
          pageSizeOptions: PAGE_SIZE_ARRAY
        }}
      />
      <Drawer
        width={600}
        visible={showDetail}
        onClose={() => {
          setCurrentRow(undefined);
          setShowDetail(false);
        }}
        closable={false}
      >
        {currentRow?.operId && (
          <ProDescriptions<OperateLogItem>
            tooltip={intl.formatMessage({
              id: 'pages.searchTable.log.operateLog.businessType',
              defaultMessage: 'operateLog businessType',
            })}
            column={2}
            title={currentRow?.title}
            request={async () => {
              return {data: currentRow || {}}
            }}
            columns={columns as ProDescriptionsItemProps<OperateLogItem>[]}
          />
        )}
      </Drawer>

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.searchTable.chosen" defaultMessage="Chosen" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.searchTable.item" defaultMessage="项" />
            </div>
          }
        >
          <Button
            onClick={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
          >
            <FormattedMessage
              id="pages.searchTable.batchDeletion"
              defaultMessage="Batch deletion"
            />
          </Button>
        </FooterToolbar>
      )}

    </PageContainer>
  );
};

export default OperateLog;
