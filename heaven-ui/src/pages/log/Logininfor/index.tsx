import {ActionType, ProColumns } from '@ant-design/pro-components';
import {
  FooterToolbar,
  PageContainer,
  ProTable,
} from '@ant-design/pro-components';
import { Button, message } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage, useIntl } from 'umi';
import {PAGE_SIZE_ARRAY} from "@/pages/constants"
import {LogininforItem} from "@/services/ant-design-pro/log/Logininfor/result";
import {getList, removeLogininfor} from "@/services/ant-design-pro/log/Logininfor/api";
import {TableListIsSuccessEnum} from "@/pages/enums";

/**
 *  Delete node
 * @zh-CN 删除节点
 *
 * @param selectedRows
 */
const handleRemove = async (selectedRows: LogininforItem[]) => {
  const hide = message.loading('正在删除');
  if (!selectedRows) return true;

  const params: number[] = [];
  selectedRows.map((item) =>{
    params.push(item.infoId)
  })
  try {
    const result = await removeLogininfor(params);
    hide();
    message.success(result.msg);
    return true;
  } catch (error) {
    hide();
    message.error('Delete failed, please try again');
    return false;
  }
};

const Logininfor: React.FC = () => {

  const actionRef = useRef<ActionType>();
  const [selectedRowsState, setSelectedRows] = useState<LogininforItem[]>([]);
  /**
   * @en-US International configuration
   * @zh-CN 国际化配置
   * */
  const intl = useIntl();

  const columns: ProColumns<LogininforItem>[] = [
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.log.logininfor.userName"
          defaultMessage="logininfor userName"
        />
      ),
      align: 'center',
      dataIndex: 'userName',
      key: 'userName',
      renderText: (val: string) => {return val}
    },
    {
      title: <FormattedMessage id="pages.searchTable.log.logininfor.ipaddr" defaultMessage="logininfor ipaddr" />,
      dataIndex: 'ipaddr',
      align: 'center',
      key: 'ipaddr',
      hideInSearch: true,
      hideInForm: false
    },
    // {
    //   title: <FormattedMessage id="pages.searchTable.log.logininfor.loginLocation" defaultMessage="logininfor loginLocation" />,
    //   dataIndex: 'loginLocation',
    //   align: 'center',
    //   hideInSearch: true,
    //   key: 'loginLocation',
    //   hideInForm: true
    // },
    // {
    //   title: <FormattedMessage id="pages.searchTable.log.logininfor.browser" defaultMessage="logininfor browser" />,
    //   dataIndex: 'browser',
    //   align: 'center',
    //   hideInSearch: true,
    //   key: 'browser',
    //   hideInForm: true
    // },
    // {
    //   title: <FormattedMessage id="pages.searchTable.log.logininfor.os" defaultMessage="logininfor os" />,
    //   dataIndex: 'os',
    //   align: 'center',
    //   hideInSearch: true,
    //   key: 'os',
    //   hideInForm: true
    // },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.log.logininfor.status"
          defaultMessage="logininfor status"
        />
      ),
      dataIndex: 'status',
      align: 'center',
      valueType: 'select',
      key: 'status',
      renderText: (val: string) => {return val}
      ,
      valueEnum: TableListIsSuccessEnum,
    }
    ,
    {
      title: <FormattedMessage id="pages.searchTable.log.logininfor.msg" defaultMessage="logininfor msg" />,
      dataIndex: 'msg',
      align: 'center',
      hideInSearch: true,
      key: 'msg',
      hideInForm: true
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.log.logininfor.accessTime"
          defaultMessage="logininfor accessTime"
        />
      ),
      render: (_, record) => <span>{record.accessTime}</span>,
      search: {
        transform: (value) => {
          return {
            'params[beginTime]': value[0],
            'params[endTime]': value[1],
          }
        }
      },
      // sorter: true,
      dataIndex: 'accessTime',
      align: 'center',
      valueType: 'dateRange',
      key: 'accessTime',
      renderText: (val: string) => {
        return val;
      }
      ,
    }
  ];

  // @ts-ignore
  return (
    <PageContainer>
      <ProTable<LogininforItem, API.Param.BasePageParam>
        headerTitle={intl.formatMessage({
          id: 'pages.searchTable.title',
          defaultMessage: 'Enquiry form',
        })}
        actionRef={actionRef}
        rowKey="infoId"
        search={{
          labelWidth: 120,
        }}
        request={getList}
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
          type :'checkbox'
        }}
        pagination={{
          showSizeChanger: true,
          showQuickJumper: true,
          pageSizeOptions: PAGE_SIZE_ARRAY
        }}
      />
      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.searchTable.chosen" defaultMessage="Chosen" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.searchTable.item" defaultMessage="项" />
            </div>
          }
        >
          <Button
            onClick={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
          >
            <FormattedMessage
              id="pages.searchTable.batchDeletion"
              defaultMessage="Batch deletion"
            />
          </Button>
        </FooterToolbar>
      )}

    </PageContainer>
  );
};

export default Logininfor;
