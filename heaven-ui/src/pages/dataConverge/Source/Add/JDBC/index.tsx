import React, {useState} from "react";
import {
  EditableProTable,
  ProColumns,
  ProFormRadio,
  ProFormSelect,
  ProFormText
} from "@ant-design/pro-components";
import {
  Button, message,
} from 'antd';
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {CloseCircleTwoTone, EditTwoTone, SaveTwoTone, StopTwoTone,PoweroffOutlined} from "@ant-design/icons";
import {RecordKey} from "@ant-design/pro-utils/lib/useEditableArray";
import { testConn } from "@/services/ant-design-pro/dataConverge/Source/api";
import {ResultCode} from "@/pages/constants";


interface jdbcItem{
  id: string;
  key: string;
  val: string;
}

// @ts-ignore
const JDBC: React.FC = ({childTypeData , intl , jdbcProps ,setJdbcProps ,driverInfos, formRef,sourceTypeLabel}) => {
  const [position , setPosition] = useState('bottom');
  const [loading  ,setLoading] = useState(false);

  const handleDbTypeChange = async (value) => {
    const driverInfo = driverInfos[value];
    formRef?.current?.setFieldsValue({
      url: driverInfo['url-prefix'],
      driverName: driverInfo['driver-class']
    });
  }

  const handleDelete = (key: RecordKey, row: (jdbcItem & {index?: number | undefined})) =>{
    let arr = JSON.parse(
      JSON.stringify(jdbcProps)
    )
    arr.filter(item => {
      item.columnId === key});
    setJdbcProps(arr)
  }

  const handleSave = (key, record) =>{
    setJdbcProps([...jdbcProps , record])
  }

  const handleTableCancel = (key, record, originRow) => {
  }

  const columns: ProColumns<jdbcItem>[] =
    [{
      title: 'key',
      dataIndex: 'key',
      formItemProps: (form, { rowIndex }) => {
        return {
          rules: rowIndex > 1 ? [{ required: true, message: '此项为必填项' }] : [],
        };
      },
      width: '35%',
    },{
      title: 'val',
      dataIndex: 'val',
      formItemProps: (form, { rowIndex }) => {
        return {
          rules: rowIndex > 1 ? [{ required: true, message: '此项为必填项' }] : [],
        };
      },
      width: '35%',
    },{
      title: intl.formatMessage({
        id: 'pages.searchTable.titleOption',
        defaultMessage: 'code updateColumn',
      }),
      valueType: 'option',
      align: 'center',
      render: (text, record, _, action) => [
        <a
          key="editable"
          onClick={() => {
            action?.startEditable?.(record.id);
          }}
        >
          <EditTwoTone />
        </a>
      ],
    }];

  const onTest = async () => {
    setLoading(true);
    const user = formRef.current?.getFieldValue('username');
    const password = formRef.current?.getFieldValue('password');
    const driverClass = formRef.current?.getFieldValue('driverName');
    const url = formRef.current?.getFieldValue('url');
    const name = formRef.current?.getFieldValue('name');
    const sourceType = sourceTypeLabel;
    const dbType = formRef.current?.getFieldValue('dbType');
    const param = {sourceType , name , properties:{dbType ,user,password,url,driverClass}};
    const result = await testConn(param);
    if(result.code === ResultCode.SUCCESS_CODE){
      message.success(result.msg);
    } else {
      message.error(result.msg);
    }
    setLoading(false);
  }

  return (
    <>
      <ProFormSelect
        rules={[
          {
            message: (
              <FormattedMessage
                id="pages.searchTable.data.dataConverge.source.dbType"
                defaultMessage="source dbType"
              />
            ),
          },
        ]}
        label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.dbType"
                                 defaultMessage="source dbType"/>}
        name="dbType"
        valueEnum={childTypeData}
        colProps={{span: 12}}
        onChange={handleDbTypeChange}
      />
      <ProFormText
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.searchTable.data.dataConverge.source.url"
                defaultMessage="source url"
              />
            ),
          },
        ]}
        label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.url" defaultMessage="source url"/>}
        width="md"
        name="url"
        colProps={{span: 12}}
        fieldProps={{
          suffix:<Button type="link" size="small" loading={loading} onClick={onTest} icon={<PoweroffOutlined />}>测试连接</Button>
        }}
      />

      <ProFormText
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.searchTable.data.dataConverge.source.user"
                defaultMessage="source user"
              />
            ),
          },
        ]}
        label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.user" defaultMessage="source user"/>}
        width="md"
        name="username"
        colProps={{span: 12}}
      />
      <ProFormText
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.searchTable.data.dataConverge.source.password"
                defaultMessage="source password"
              />
            ),
          },
        ]}
        label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.password" defaultMessage="source password"/>}
        width="md"
        name="password"
        colProps={{span: 12}}
      />
      <ProFormText
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.searchTable.data.dataConverge.source.driverClass"
                defaultMessage="source driverClass"
              />
            ),
          },
        ]}
        label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.driverClass" defaultMessage="source driverClass"/>}
        width="md"
        name="driverName"
        colProps={{span: 12}}
      />
      <ProFormText
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.searchTable.data.dataConverge.source.syncInterval"
                defaultMessage="source syncInterval"
              />
            ),
          },
        ]}
        label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.syncInterval" defaultMessage="source syncInterval"/>}
        width="md"
        name="syncInterval"
        colProps={{span: 12}}
      />
      <ProFormRadio.Group
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.searchTable.data.dataConverge.source.serverAggregate"
                defaultMessage="source serverAggregate"
              />
            ),
          },
        ]}
        label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.serverAggregate" defaultMessage="source serverAggregate"/>}
        width="md"
        name="enableServerAggregate"
        options={[
          {
            label: "开启",
            value: true
          },
          {
            label: "关闭",
            value: false
          }
        ]}
        initialValue={false}
        colProps={{span: 8}}
      />
      <ProFormRadio.Group
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.searchTable.data.dataConverge.source.enableSpecialSQL"
                defaultMessage="source enableSpecialSQL"
              />
            ),
          },
        ]}
        label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.enableSpecialSQL" defaultMessage="source enableSpecialSQL"/>}
        width="md"
        name="enableSpecialSQL"
        options={[
          {
            label: "开启",
            value: true
          },
          {
            label: "关闭",
            value: false
          }
        ]}
        initialValue={false}
        colProps={{span: 8}}
      />
      <ProFormRadio.Group
        rules={[
          {
            required: true,
            message: (
              <FormattedMessage
                id="pages.searchTable.data.dataConverge.source.enableSyncSchemas"
                defaultMessage="source enableSyncSchemas"
              />
            ),
          },
        ]}
        label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.enableSyncSchemas" defaultMessage="source enableSyncSchemas"/>}
        width="md"
        name="enableSyncSchemas"
        options={[
          {
            label: "开启",
            value: true
          },
          {
            label: "关闭",
            value: false
          }
        ]}
        initialValue={false}
        colProps={{span: 8}}
      />

      <EditableProTable<jdbcItem>
        headerTitle={<FormattedMessage id="pages.searchTable.data.dataConverge.source.properties" defaultMessage="source properties"/>}
        rowKey="id"
        colProps={{span: 24}}
        recordCreatorProps={
          position !== 'hidden'
          ? {
              position: 'bottom',
              record: () => ({ id: (Math.random() * 1000000).toFixed(0) }),
            }
           : false
        }
        loading={false}
        columns={columns}
        dataSource={[]}
        editable={{
          type: 'multiple',
          actionRender: (row, config, defaultDoms) => {
            return [
              defaultDoms.delete, defaultDoms.save ,defaultDoms.cancel
            ];
          },
          onValuesChange: (record, recordList) => {
            setJdbcProps(recordList);
          },
          cancelText:  <StopTwoTone />,
          saveText: <SaveTwoTone />,
          deleteText: <CloseCircleTwoTone />,
          onDelete: handleDelete,
          onSave: handleSave,
          onCancel: handleTableCancel
        }}
      />
    </>
  )
}

export default JDBC;
