import React, {useEffect, useRef, useState} from "react";
import {ModalForm, ProFormSelect, ProFormText} from "@ant-design/pro-components";
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import { handleDictData} from "@/services/ant-design-pro/system/Dict/api";
import JDBC from "@/pages/dataConverge/Source/Add/JDBC";
import HTTP from "@/pages/dataConverge/Source/Add/HTTP";
import FILE from "@/pages/dataConverge/Source/Add/FILE";
import {ResultCode} from "@/pages/constants";
import {message} from "antd";
import {addSource, getDriverInfos, handleGetBusinessSection} from "@/services/ant-design-pro/dataConverge/Source/api";
import {DriverInfoItemResult, SourceItem} from "@/services/ant-design-pro/dataConverge/Source/result";
import type {ProFormInstance} from '@ant-design/pro-components';

const handleAdd = async (fields: SourceItem) => {
  const hide = message.loading('正在添加');
  const result = await addSource({ ...fields });
  try {
    if(result.code === ResultCode.SUCCESS_CODE){
      message.success(result.msg);
    }
    hide();
    return true;
  } catch (error) {
    hide();
    message.error(result.msg);
    return false;
  }
};

const handleGetDriverInfos = async () => {
  const response: DriverInfoItemResult = await getDriverInfos();
  if(response.code === ResultCode.SUCCESS_CODE){
    return response.data;
  }
}

const initSourceType = '2';
// @ts-ignore
const Add: React.FC = ({formTableRef , intl  ,createModalVisible , handleModalVisible}) => {

  const formRef = useRef<ProFormInstance>();
  const [sourceType , setSourceType] = useState(initSourceType);
  const [sourceTypeLabel , setSourceTypeLabel] = useState("JDBC");
  const [childTypeData , setChildTypeData] = useState([]);
  const [sourceTypeData , setSourceTypeData] = useState([]);
  const [jdbcProps , setJdbcProps] = useState({});
  const [businessSections , setBusinessSections] = useState({});
  const [driverInfos , setDriverInfos] = useState({});

  const setChildType = async () => {
    const childTypeData = await handleDictData('data_source_child_type');
    setChildTypeData(childTypeData);
  }

  const setType = async () => {
    const sourceTypeData = await handleDictData('data_source_type');
    setSourceTypeData(sourceTypeData);
  }

  const handleSourceTypeChange = async (value:{value: string;label: React.ReactNode}) => {
    //const {label , value as val} = value;
    const val = value.value;
    const label = value.label;
    setSourceType(val);
    setSourceTypeLabel(label);
    //setSourceTypeLabel();
    if(val === "2"){
      setChildType();
    } else{
      setChildTypeData([]);
    }
  }

  const handleJDBCParam = (value) => {
    const {dbType,driverName,enableSpecialSQL,enableSyncSchemas,password,enableServerAggregate,syncInterval,url,username} = value;
    let param = {dbType,driverName,enableSpecialSQL ,enableSyncSchemas,password,enableServerAggregate,syncInterval,url,username};
    if(jdbcProps){
      const obj = {};
      for(let i = 0; i < jdbcProps.length; i++){
        const { key, val } = jdbcProps[i];
        obj[key] = val;
      };
      param['properties'] = obj;
    }
    const paramJson = JSON.stringify(param);
    return paramJson;
  }

  useEffect(async function () {
    setType();
    setChildType();
  },[]);

  useEffect(async function () {
    const data = await handleGetBusinessSection();
    setBusinessSections(data);
  },[]);

  useEffect(async function () {
    const data = await handleGetDriverInfos();
    setDriverInfos(data);
  },[]);

  // @ts-ignore
  return (
    <>
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.data.dataConverge.source.addForm.title',
          defaultMessage: 'New source add',
        })}
        formRef={formRef}
        modalProps={{
          destroyOnClose: true
        }}
        key="addForm"
        width="50%"
        grid={true}
        visible={createModalVisible}
        onVisibleChange={handleModalVisible}
        onFinish={async (value) => {
          if(sourceType === '2'){
            value['config'] = handleJDBCParam(value);
            value['childType'] = value['dbType'];
            const success = await handleAdd(value as SourceItem);
            if (success) {
              handleModalVisible(false);
              if (formTableRef.current) {
                formTableRef.current.submit();
              }
            }
          }

        }}
      >
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.source.name"
                  defaultMessage="source name"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.name" defaultMessage="source name"/>}
          width="md"
          name="name"
          colProps={{span: 6}}
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.source.orderNum"
                  defaultMessage="source orderNum"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.orderNum" defaultMessage="source orderNum"/>}
          width="md"
          name="orderNum"
          colProps={{span: 6}}
        />

        <ProFormSelect.SearchSelect
          name="businessSectionIds"
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.businessSectionIds" defaultMessage="source businessSectionIds"/>}
          debounceTime={300}
          request={handleGetBusinessSection}
          colProps={{span: 6}}
          fieldProps={{
            labelInValue: false
          }}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.source.businessSectionIds"
                  defaultMessage="source businessSectionIds"
                />
              ),
            },
          ]}
        />
        <ProFormSelect
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.source.status"
                  defaultMessage="source status"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.status" defaultMessage="source status"/>}
          width="md"
          name="status"
          valueEnum={{
            0: '数仓数据源',
            1: '业务数据源'
          }}
          colProps={{span: 6}}
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.source.remark"
                  defaultMessage="source remark"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.remark" defaultMessage="source remark"/>}
          width="md"
          name="remark"
          colProps={{span: 24}}
        />

        <ProFormSelect
          labelInValue
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.source.sourceType"
                  defaultMessage="source sourceType"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.sourceType" defaultMessage="source sourceType"/>}
          width="md"
          name="sourceType"
          valueEnum={sourceTypeData}
          initialValue={sourceType === undefined ? '' : sourceType}
          onChange={handleSourceTypeChange}
          fieldProps={{
            labelInValue: true
          }}
        />

        {sourceType && sourceType === '0' && <FILE />}
        {sourceType && sourceType === '1' && <HTTP />}
        {sourceType && sourceType === '2' && <JDBC childTypeData={childTypeData} intl={intl} jdbcProps={jdbcProps} setJdbcProps={setJdbcProps} driverInfos={driverInfos} formRef={formRef} sourceTypeLabel={sourceTypeLabel}/>}

      </ModalForm>
    </>
  )
}

export default Add;
