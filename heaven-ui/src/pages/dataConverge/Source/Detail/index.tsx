import React from "react";

import {Drawer} from "antd";

import ProDescriptions from "@ant-design/pro-descriptions";
import {SourceTypeEnum} from "@/pages/enums";


// @ts-ignore
const Detail: React.FC = ({ intl, setCurrentRow, setShowDetail,currentRow,showDetail}) => {

  return (
    <Drawer
      width={600}
      visible={showDetail}
      onClose={() => {
        setCurrentRow(undefined);
        setShowDetail(false);
      }}
      closable={false}
    >
      {currentRow?.id && (
        <ProDescriptions column={2} title={currentRow?.name}>
          <ProDescriptions.Item label={intl.formatMessage({
            id: 'pages.searchTable.data.dataConverge.source.businessSectionIds',
            defaultMessage: 'source businessSectionIds',
          })}>
            {currentRow?.businessSectionNamesStr}
          </ProDescriptions.Item>
          <ProDescriptions.Item label={intl.formatMessage({
            id: 'pages.searchTable.data.dataConverge.source.status',
            defaultMessage: 'source status',
          })} >
            {currentRow?.status === '0' ? '数仓数据源' : '业务数据源'}
          </ProDescriptions.Item>
          <ProDescriptions.Item
            label={intl.formatMessage({
              id: 'pages.searchTable.data.dataConverge.source.sourceType',
              defaultMessage: 'source sourceType',
            })}
            valueEnum={SourceTypeEnum}
          >
            {currentRow?.sourceType}
          </ProDescriptions.Item>
          <ProDescriptions.Item
            label={intl.formatMessage({
              id: 'pages.searchTable.data.dataConverge.source.childType',
              defaultMessage: 'source childType',
            })}
          >
            {currentRow?.childType}
          </ProDescriptions.Item>
          <ProDescriptions.Item
            label={intl.formatMessage({
              id: 'pages.searchTable.data.dataConverge.source.createTime',
              defaultMessage: 'source createTime',
            })}
          >
            {currentRow?.createTime}
          </ProDescriptions.Item>
          <ProDescriptions.Item
            label={intl.formatMessage({
              id: 'pages.searchTable.data.dataConverge.source.orderNum',
              defaultMessage: 'source orderNum',
            })}
          >
            {currentRow?.orderNum}
          </ProDescriptions.Item>
          <ProDescriptions.Item
            span={2}
            valueType="text"
            contentStyle={{
              maxWidth: '80%',
            }}
            label={intl.formatMessage({
              id: 'pages.searchTable.data.dataConverge.source.remark',
              defaultMessage: 'source remark',
            })}
          >
            {currentRow?.remark}
          </ProDescriptions.Item>
          <ProDescriptions.Item label={intl.formatMessage({
            id: 'pages.searchTable.data.dataConverge.source.config',
            defaultMessage: 'source config',
          })} valueType="jsonCode" span={2}>
            {currentRow?.config}
          </ProDescriptions.Item>
        </ProDescriptions>)}
    </Drawer>
  )
}

export default Detail;
