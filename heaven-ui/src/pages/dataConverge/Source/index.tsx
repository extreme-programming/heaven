import {
  GridContent,
  PageContainer,
  ProFormGroup,
  ProFormSelect,
  ProFormText,
  QueryFilter,
} from "@ant-design/pro-components";
import React, {useEffect, useRef, useState} from "react";
import {FormattedMessage, useIntl} from "@@/plugin-locale/localeExports";
import {ClusterOutlined, ContactsOutlined, HomeOutlined, PlusOutlined} from '@ant-design/icons';
import {Button, Card, List, Typography, Popconfirm, Drawer, Row, Col, DescriptionItem, Divider} from 'antd';
import styles from './style.less';
import {
  handleGetSource,
  removeSource,
  handleGetBusinessSection, getDetail
} from "@/services/ant-design-pro/dataConverge/Source/api";
import {pageSize, ResultCode} from "@/pages/constants";
import { SourceItem } from "@/services/ant-design-pro/dataConverge/Source/result";
import {ProFormInstance} from "@ant-design/pro-form";
import {handleDictData} from "@/services/ant-design-pro/system/Dict/api";
import { message} from "antd";
import {PageLoading} from "@ant-design/pro-components";
import Add from "@/pages/dataConverge/Source/Add";
import Update from "@/pages/dataConverge/Source/Update";
import ProDescriptions, {ProDescriptionsItemProps} from "@ant-design/pro-descriptions";
import {OperateLogItem} from "@/services/ant-design-pro/log/OperateLog/result";
import {SourceTypeEnum} from "@/pages/enums";
import centerStyles from './center.less'
import dayjs from 'dayjs';
import Detail from "./Detail";

const { Paragraph,Text } = Typography;

const Source: React.FC = () => {
  const [source , setSource] = useState([]);
  const [sourceTypeData , setSourceTypeData] = useState([]);
  const [childTypeData , setChildTypeData] = useState([]);
  const [loading , setLoading] = useState(false);
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const [showDetail , setShowDetail] = useState(false);
  const [currentRow, setCurrentRow] = useState({});


  const intl = useIntl();
  const nullData: Partial<SourceItem> = {};
  const formRef = useRef<ProFormInstance>();

  const handleDeleteSource = async (sourceId) => {
    setLoading(true);
    const params = [sourceId];
    const data = await removeSource(params);
    if(data.code = ResultCode.SUCCESS_CODE) {
      message.success(data.msg);
    } else {
      message.error(data.msg);
    }
    const res = await handleGetSource();
    setSource(res);
    setLoading(false);
  }

  const setChildType = async () => {
    const childTypeData = await handleDictData('data_source_child_type');
    setChildTypeData(childTypeData);
  }

  const setType = async () => {
    const sourceTypeData = await handleDictData('data_source_type');
    setSourceTypeData(sourceTypeData);
  }

  const handleSourceTypeChange = async (value) => {
    if(value === "2"){
      setChildType();
    } else{
      setChildTypeData([]);
    }
  }

  const handleDetail = async (id: number) => {
    setShowDetail(true);
    const result = await getDetail(id);
    if(result.code === ResultCode.SUCCESS_CODE){
      setCurrentRow(result.data)
    }
    else{
      message.error(result.msg);
    }
  }


  const DescriptionItem = ({ title, content }: DescriptionItemProps) => (
    <div className="site-description-item-profile-wrapper">
      <p className="site-description-item-profile-p-label">{title}:</p>
      {content}
    </div>
  );

  useEffect(async function () {
    setLoading(true);
    const data = await handleGetSource();
    setSource(data);
    setType();
    setLoading(false);
  },[]);

  return (
    <>
      <PageContainer>
        <QueryFilter<SourceItem>
          formRef={formRef}
          onFinish={async (values) => {
            setSource([]);
            setLoading(true);
            const data = await handleGetSource(values);
            setSource(data);
            setLoading(false);
          }}
        >
          <ProFormGroup label={intl.formatMessage({
            id: 'pages.searchTable.data.dataConverge.source.search',
            defaultMessage: 'source search',
          })}>
            <ProFormText name="name" label={intl.formatMessage({
              id: 'pages.searchTable.data.dataConverge.source.name',
              defaultMessage: 'source name',
            })} />
            <ProFormSelect
              rules={[
                {
                  message: (
                    <FormattedMessage
                      id="pages.searchTable.data.dataConverge.source.status"
                      defaultMessage="source status"
                    />
                  ),
                },
              ]}
              label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.status" defaultMessage="source status"/>}
              width="sm"
              name="status"
              valueEnum={{
                0: '数仓数据源',
                1: '业务数据源'
              }}
            />
            <ProFormSelect.SearchSelect
              name="businessSectionIds"
              label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.businessSectionIds" defaultMessage="source businessSectionIds"/>}
              debounceTime={300}
              request={handleGetBusinessSection}
              colProps={{span: 6}}
              fieldProps={{
                labelInValue: false
              }}
            />
            <ProFormSelect
              rules={[
                {
                  message: (
                    <FormattedMessage
                      id="pages.searchTable.data.dataConverge.source.sourceType"
                      defaultMessage="source sourceType"
                    />
                  ),
                },
              ]}
              label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.sourceType"
                                       defaultMessage="source sourceType"/>}
              name="sourceType"
              onChange={handleSourceTypeChange}
              valueEnum={sourceTypeData}
            />
            <ProFormSelect
              rules={[
                {
                  message: (
                    <FormattedMessage
                      id="pages.searchTable.data.dataConverge.source.childType"
                      defaultMessage="source childType"
                    />
                  ),
                },
              ]}
              label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.childType"
                                       defaultMessage="source childType"/>}
              name="childType"
              valueEnum={childTypeData}
            />
          </ProFormGroup>
        </QueryFilter>
        <div className={styles.cardList}>
          <List<Partial<SourceItem>>
            rowKey="id"
            grid={{
              gutter: 16,
              xs: 1,
              sm: 2,
              md: 3,
              lg: 3,
              xl: 4,
              xxl: 4,
            }}
            dataSource={[nullData, ...source]}
            renderItem={(item) => {
              if (item && item.id) {
                const imageSrc = `/icons/source/${item.childType}.png`;
                return (
                  <List.Item key={item.id} >
                    <Card
                      hoverable
                      className={styles.card}
                      actions={[
                        <a key="select" onClick={() => handleDetail(item.id)}>
                          <FormattedMessage id="pages.searchTable.detail"
                                            defaultMessage="source detail"/>
                        </a>,
                        // <a key="update">
                        //   <FormattedMessage id="pages.searchTable.update"
                        //                     defaultMessage="source update"/>
                        // </a>,

                        <Popconfirm title={intl.formatMessage({
                          id: 'pages.searchTable.confirmDelete',
                          defaultMessage: 'confirm source delete',
                        })} onConfirm={()=>handleDeleteSource(item.id)}
                                    okText={intl.formatMessage({
                                      id: 'pages.searchTable.yes',
                                      defaultMessage: 'yes',
                                    })}
                                    cancelText={intl.formatMessage({
                                      id: 'pages.searchTable.no',
                                      defaultMessage: 'no',
                                    })}>
                          <a key="delete">
                            <FormattedMessage id="pages.searchTable.delete"
                                              defaultMessage="source delete"/>
                          </a>
                        </Popconfirm>
                      ]}
                    >
                      <Card.Meta
                        avatar={<img alt="" className={styles.cardAvatar} src={imageSrc} />}
                        title={<Text strong>{item.name}{item.status==='0' ? '(数仓数据源)':'(业务数据源)'}</Text>}
                        description={
                          <Paragraph className={styles.item} ellipsis={{ rows: 5 }}>
                            <Text type={"success"}>
                              {item.businessSectionNamesStr}
                            </Text>
                            <br/>
                            <Text underline>
                              {item.remark}
                            </Text>
                          </Paragraph>
                        }
                      />
                    </Card>
                  </List.Item>
                );
              }
              return (
                <List.Item>
                  <Button type="dashed" className={styles.newButton}
                          onClick={() => {
                            handleModalVisible(true);
                          }}
                  >
                    <PlusOutlined />
                    <FormattedMessage
                      id="pages.searchTable.data.dataConverge.source.addForm.title"
                      defaultMessage="source addForm"
                    />
                  </Button>
                </List.Item>
              );
            }}
          />
        </div>
      </PageContainer>

      <Detail intl={intl} setCurrentRow={setCurrentRow} setShowDetail={setShowDetail} showDetail={showDetail} currentRow={currentRow}/>
      {/*<Drawer*/}
      {/*  width={600}*/}
      {/*  visible={showDetail}*/}
      {/*  onClose={() => {*/}
      {/*    setCurrentRow(undefined);*/}
      {/*    setShowDetail(false);*/}
      {/*  }}*/}
      {/*  closable={false}*/}
      {/*>*/}
      {/*  {currentRow?.id && (*/}
      {/*      <ProDescriptions column={2} title={currentRow?.name}>*/}
      {/*        <ProDescriptions.Item label={intl.formatMessage({*/}
      {/*          id: 'pages.searchTable.data.dataConverge.source.businessSectionIds',*/}
      {/*          defaultMessage: 'source businessSectionIds',*/}
      {/*        })}>*/}
      {/*          {currentRow?.businessSectionNamesStr}*/}
      {/*        </ProDescriptions.Item>*/}
      {/*        <ProDescriptions.Item label={intl.formatMessage({*/}
      {/*          id: 'pages.searchTable.data.dataConverge.source.status',*/}
      {/*          defaultMessage: 'source status',*/}
      {/*        })} >*/}
      {/*          {currentRow?.status === '0' ? '数仓数据源' : '业务数据源'}*/}
      {/*        </ProDescriptions.Item>*/}
      {/*        <ProDescriptions.Item*/}
      {/*          label={intl.formatMessage({*/}
      {/*            id: 'pages.searchTable.data.dataConverge.source.sourceType',*/}
      {/*            defaultMessage: 'source sourceType',*/}
      {/*          })}*/}
      {/*          valueEnum={SourceTypeEnum}*/}
      {/*        >*/}
      {/*          {currentRow?.sourceType}*/}
      {/*        </ProDescriptions.Item>*/}
      {/*        <ProDescriptions.Item*/}
      {/*          label={intl.formatMessage({*/}
      {/*            id: 'pages.searchTable.data.dataConverge.source.childType',*/}
      {/*            defaultMessage: 'source childType',*/}
      {/*          })}*/}
      {/*        >*/}
      {/*          {currentRow?.childType}*/}
      {/*        </ProDescriptions.Item>*/}
      {/*        <ProDescriptions.Item*/}
      {/*          label={intl.formatMessage({*/}
      {/*            id: 'pages.searchTable.data.dataConverge.source.createTime',*/}
      {/*            defaultMessage: 'source createTime',*/}
      {/*          })}*/}
      {/*        >*/}
      {/*          {currentRow?.createTime}*/}
      {/*        </ProDescriptions.Item>*/}
      {/*        <ProDescriptions.Item*/}
      {/*          label={intl.formatMessage({*/}
      {/*            id: 'pages.searchTable.data.dataConverge.source.orderNum',*/}
      {/*            defaultMessage: 'source orderNum',*/}
      {/*          })}*/}
      {/*        >*/}
      {/*          {currentRow?.orderNum}*/}
      {/*        </ProDescriptions.Item>*/}
      {/*        <ProDescriptions.Item*/}
      {/*          span={2}*/}
      {/*          valueType="text"*/}
      {/*          contentStyle={{*/}
      {/*            maxWidth: '80%',*/}
      {/*          }}*/}
      {/*          label={intl.formatMessage({*/}
      {/*            id: 'pages.searchTable.data.dataConverge.source.remark',*/}
      {/*            defaultMessage: 'source remark',*/}
      {/*          })}*/}
      {/*        >*/}
      {/*          {currentRow?.remark}*/}
      {/*        </ProDescriptions.Item>*/}
      {/*        <ProDescriptions.Item label={intl.formatMessage({*/}
      {/*          id: 'pages.searchTable.data.dataConverge.source.config',*/}
      {/*          defaultMessage: 'source config',*/}
      {/*        })} valueType="jsonCode" span={2}>*/}
      {/*          {currentRow?.config}*/}
      {/*        </ProDescriptions.Item>*/}
      {/*      </ProDescriptions>)}*/}
      {/*</Drawer>*/}

      { loading && <PageLoading/>}
      { createModalVisible && <Add intl={intl}
                                   createModalVisible={createModalVisible}
                                   handleModalVisible={handleModalVisible}
                                   formTableRef={formRef}
      />}
      { updateModalVisible && <Update />}

    </>
  )
}
export default Source;
