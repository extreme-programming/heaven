// @ts-ignore
import React, {useEffect} from "react";
import {ModalForm, ProFormSelect, ProFormText} from "@ant-design/pro-components";
// @ts-ignore
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {message} from "antd";
// @ts-ignore
import {ResultCode} from "@/pages/constants";
// @ts-ignore
import {add} from "@/services/ant-design-pro/dataConverge/Datadomain/api";
// @ts-ignore
import { BusinessDatadomainItem } from "@/services/ant-design-pro/dataConverge/Datadomain/result";
import {TableListIsSuccessEnum} from "@/pages/enums";
import {handleGetBusinessSection} from "@/services/ant-design-pro/dataConverge/Source/api";

/**
 * @en-US Add node
 * @zh-CN 添加节点
 * @param fields
 */
const handleAdd = async (fields: BusinessDatadomainItem) => {
  const hide = message.loading('正在添加');
  const result = await add({ ...fields });
  try {
    if(result.code === ResultCode.SUCCESS_CODE){
      message.success(result.msg);
    }
    hide();
    return true;
  } catch (error) {
    hide();
    message.error(result.msg);
    return false;
  }
};

// @ts-ignore
const AddForm: React.FC = ({actionRef , intl  ,createModalVisible , handleModalVisible}) => {

  return (
    <>
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.data.dataConverge.businessDatadomain.addForm.title',
          defaultMessage: 'New businessDatadomain',
        })}
        modalProps={{
          destroyOnClose: true
        }}
        key="addForm"
        width="400px"
        visible={createModalVisible}
        onVisibleChange={handleModalVisible}
        onFinish={async (value) => {
          const success = await handleAdd(value as BusinessDatadomainItem);

          if (success) {

            handleModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.businessDatadomain.name"
                  defaultMessage="businessDatadomain name"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.businessDatadomain.name" defaultMessage="businessDatadomain name"/>}
          width="md"
          name="name"
        />

        <ProFormSelect
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.businessDatadomain.status"
                  defaultMessage="businessDatadomain status"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.businessDatadomain.status" defaultMessage="businessDatadomain status"/>}
          width="md"
          name="status"
          valueEnum={TableListIsSuccessEnum}
        />

        <ProFormSelect
          name="sectionId"
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.businessSectionIds" defaultMessage="source businessSectionIds"/>}
          request={handleGetBusinessSection}
          fieldProps={{
            labelInValue: false
          }}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.source.businessSectionIds"
                  defaultMessage="source businessSectionIds"
                />
              ),
            },
          ]}
          width="md"
        />

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.businessSection.orderNum"
                  defaultMessage="businessSection orderNum"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.orderNum" defaultMessage="businessSection orderNum"/>}
          width="md"
          name="orderNum"
        />

        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.remark" defaultMessage="businessSection remark" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.remark" defaultMessage="businessSection remark"/>}
          width="md"
          name="remark"
        />
      </ModalForm>
    </>
  )
}

export default AddForm;
