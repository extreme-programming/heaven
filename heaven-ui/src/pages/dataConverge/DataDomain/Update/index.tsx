// @ts-ignore
import React from "react";
import {ModalForm, ProFormSelect, ProFormText} from "@ant-design/pro-components";
// @ts-ignore
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {message} from "antd";
// @ts-ignore// @ts-ignore
import {ResultCode} from "@/pages/constants";
// @ts-ignore
import {edit} from "@/services/ant-design-pro/dataConverge/Datadomain/api";
// @ts-ignore
import { BusinessSectionItem } from "@/services/ant-design-pro/dataConverge/BusinessSection/result";
import {TableListIsSuccessEnum} from "@/pages/enums";
import {handleGetBusinessSection} from "@/services/ant-design-pro/dataConverge/Source/api";
import {BusinessDatadomainItem} from "@/services/ant-design-pro/dataConverge/Datadomain/result";

/**
 * @en-US Update node
 * @zh-CN 更新节点
 *
 * @param fields
 */
const handleUpdate = async (fields: BusinessDatadomainItem) => {
  const hide = message.loading('正在修改');
  const result = await edit(fields);

  try {
    if(result.code === ResultCode.SUCCESS_CODE){
      message.success(result.msg);
    }
    hide();
    return true;
  } catch (error) {
    hide();
    message.error(result.msg);
    return false;
  }
}

// @ts-ignore
const UpdateForm: React.FC = ({actionRef , intl  ,updateModalVisible , handleUpdateModalVisible ,currentRow}) => {
  return (
    <>
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.data.dataConverge.businessDatadomain.updateForm.title',
          defaultMessage: 'update businessDatadomain',
        })}
        modalProps={{
          destroyOnClose: true
        }}
        key="updateForm"
        width="400px"
        visible={updateModalVisible}
        onVisibleChange={handleUpdateModalVisible}
        onFinish={async (value) => {
          const success = await handleUpdate(value as BusinessSectionItem);
          if (success) {
            handleUpdateModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.businessDatadomain.name"
                  defaultMessage="businessDatadomain name"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.businessDatadomain.name" defaultMessage="businessDatadomain name"/>}
          width="md"
          name="name"
          initialValue={"" +currentRow?.name || null}
        />

        <ProFormSelect
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.businessDatadomain.status"
                  defaultMessage="businessDatadomain status"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.businessDatadomain.status" defaultMessage="businessDatadomain status"/>}
          width="md"
          name="status"
          valueEnum={TableListIsSuccessEnum}
          initialValue={"" +currentRow?.status || null}
        />

        <ProFormSelect
          name="sectionId"
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.source.businessSectionIds" defaultMessage="source businessSectionIds"/>}
          request={handleGetBusinessSection}
          fieldProps={{
            labelInValue: false
          }}
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.source.businessSectionIds"
                  defaultMessage="source businessSectionIds"
                />
              ),
            },
          ]}
          width="md"
          initialValue={currentRow?.sectionId || null}
        />

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.businessSection.orderNum"
                  defaultMessage="businessSection orderNum"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.orderNum" defaultMessage="businessSection orderNum"/>}
          width="md"
          name="orderNum"
          initialValue={"" +currentRow?.orderNum || null}
        />

        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.remark" defaultMessage="businessSection remark" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.remark" defaultMessage="businessSection remark"/>}
          width="md"
          name="remark"
          initialValue={"" +currentRow?.remark || null}
        />
        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.data.dataConverge.businessDatadomain.id" defaultMessage="businessDatadomain id" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.businessDatadomain.id" defaultMessage="businessDatadomain id"/>}
          width="md"
          name="id"
          initialValue={"" +currentRow?.id || null}
          hidden={true}
        />
      </ModalForm>
    </>
  )
}

export default UpdateForm;
