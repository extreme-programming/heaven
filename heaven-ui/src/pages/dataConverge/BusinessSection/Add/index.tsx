// @ts-ignore
import React, {useEffect} from "react";
import {ModalForm, ProFormSelect, ProFormText} from "@ant-design/pro-components";
// @ts-ignore
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {message} from "antd";
// @ts-ignore
import {ResultCode} from "@/pages/constants";
// @ts-ignore
import {addBusinessSection} from "@/services/ant-design-pro/dataConverge/BusinessSection/api";
// @ts-ignore
import { BusinessSectionItem } from "@/services/ant-design-pro/dataConverge/BusinessSection/result";

/**
 * @en-US Add node
 * @zh-CN 添加节点
 * @param fields
 */
const handleAdd = async (fields: BusinessSectionItem) => {
  const hide = message.loading('正在添加');
  const result = await addBusinessSection({ ...fields });
  try {
    if(result.code === ResultCode.SUCCESS_CODE){
      message.success(result.msg);
    }
    hide();
    return true;
  } catch (error) {
    hide();
    message.error(result.msg);
    return false;
  }
};

// @ts-ignore
const AddForm: React.FC = ({actionRef , intl  ,createModalVisible , handleModalVisible}) => {

  return (
    <>
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.data.dataConverge.businessSection.addForm.title',
          defaultMessage: 'New businessSection',
        })}
        modalProps={{
          destroyOnClose: true
        }}
        key="addForm"
        width="400px"
        visible={createModalVisible}
        onVisibleChange={handleModalVisible}
        onFinish={async (value) => {
          const success = await handleAdd(value as BusinessSectionItem);

          if (success) {

            handleModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.businessSection.name"
                  defaultMessage="businessSection name"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.name" defaultMessage="businessSection name"/>}
          width="md"
          name="name"
        />

        {/*<ProFormSelect*/}
        {/*  rules={[*/}
        {/*    {*/}
        {/*      required: true,*/}
        {/*      message: (*/}
        {/*        <FormattedMessage*/}
        {/*          id="pages.searchTable.data.dataConverge.businessSection.status"*/}
        {/*          defaultMessage="businessSection status"*/}
        {/*        />*/}
        {/*      ),*/}
        {/*    },*/}
        {/*  ]}*/}
        {/*  label={<FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.status" defaultMessage="businessSection status"/>}*/}
        {/*  width="md"*/}
        {/*  name="status"*/}
        {/*  valueEnum={{*/}
        {/*    0: '正常',*/}
        {/*    1: '停用'*/}
        {/*  }}*/}
        {/*/>*/}

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.businessSection.orderNum"
                  defaultMessage="businessSection orderNum"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.orderNum" defaultMessage="businessSection orderNum"/>}
          width="md"
          name="orderNum"
        />

        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.remark" defaultMessage="businessSection remark" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.remark" defaultMessage="businessSection remark"/>}
          width="md"
          name="remark"
        />
      </ModalForm>
    </>
  )
}

export default AddForm;
