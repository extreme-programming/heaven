// @ts-ignore
import React from "react";
import {ModalForm, ProFormSelect, ProFormText} from "@ant-design/pro-components";
// @ts-ignore
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {message} from "antd";
// @ts-ignore
import {ResultCode} from "@/pages/constants";
// @ts-ignore
import {editBusinessSection} from "@/services/ant-design-pro/dataConverge/BusinessSection/api";
// @ts-ignore
import { BusinessSectionItem } from "@/services/ant-design-pro/dataConverge/BusinessSection/result";
import {PostItem} from "@/services/ant-design-pro/system/Post/result";

/**
 * @en-US Update node
 * @zh-CN 更新节点
 *
 * @param fields
 */
const handleUpdate = async (fields: BusinessSectionItem) => {
  const hide = message.loading('正在修改');
  const result = await editBusinessSection(fields);

  try {
    if(result.code === ResultCode.SUCCESS_CODE){
      message.success(result.msg);
    }
    hide();
    return true;
  } catch (error) {
    hide();
    message.error(result.msg);
    return false;
  }
}

// @ts-ignore
const UpdateForm: React.FC = ({actionRef , intl  ,updateModalVisible , handleUpdateModalVisible ,currentRow}) => {
  return (
    <>
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.data.dataConverge.businessSection.updateForm.title',
          defaultMessage: 'update businessSection',
        })}
        modalProps={{
          destroyOnClose: true
        }}
        key="updateForm"
        width="400px"
        visible={updateModalVisible}
        onVisibleChange={handleUpdateModalVisible}
        onFinish={async (value) => {
          const success = await handleUpdate(value as BusinessSectionItem);
          if (success) {
            handleUpdateModalVisible(false);
            if (actionRef.current) {
              actionRef.current.reload();
            }
          }
        }}
      >
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.businessSection.name"
                  defaultMessage="businessSection name"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.name" defaultMessage="businessSection name"/>}
          width="md"
          name="name"
          initialValue={currentRow ? currentRow.name : "" }
        />

        {/*<ProFormSelect*/}
        {/*  rules={[*/}
        {/*    {*/}
        {/*      required: true,*/}
        {/*      message: (*/}
        {/*        <FormattedMessage*/}
        {/*          id="pages.searchTable.data.dataConverge.businessSection.status"*/}
        {/*          defaultMessage="businessSection status"*/}
        {/*        />*/}
        {/*      ),*/}
        {/*    },*/}
        {/*  ]}*/}
        {/*  label={<FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.status" defaultMessage="businessSection status"/>}*/}
        {/*  width="md"*/}
        {/*  name="status"*/}
        {/*  valueEnum={{*/}
        {/*    0: '正常',*/}
        {/*    1: '停用'*/}
        {/*  }}*/}
        {/*  initialValue={currentRow ? currentRow.status : "" }*/}
        {/*/>*/}

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.data.dataConverge.businessSection.orderNum"
                  defaultMessage="businessSection orderNum"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.orderNum" defaultMessage="businessSection orderNum"/>}
          width="md"
          name="orderNum"
          initialValue={currentRow ? currentRow.orderNum : "" }
        />

        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.remark" defaultMessage="businessSection remark" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.remark" defaultMessage="businessSection remark"/>}
          width="md"
          name="remark"
          initialValue={currentRow ? currentRow.remark : "" }
        />

        <ProFormText
          rules={[
            {
              message: (
                <FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.id" defaultMessage="businessSection id" />
              )
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.id" defaultMessage="businessSection id"/>}
          width="md"
          name="id"
          initialValue={"" +currentRow?.id || null}
          hidden={true}
        />
      </ModalForm>
    </>
  )
}

export default UpdateForm;
