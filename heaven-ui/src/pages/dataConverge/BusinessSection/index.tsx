import { PlusOutlined , EditOutlined } from '@ant-design/icons';
import {ActionType, ProColumns } from '@ant-design/pro-components';
import {
  FooterToolbar,
  PageContainer,
  ProTable,
} from '@ant-design/pro-components';
import { Button, message } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage, useIntl } from 'umi';
import {PAGE_SIZE_ARRAY} from "@/pages/constants";
import {BusinessSectionPageList, BusinessSectionResult , BusinessSectionItem} from '@/services/ant-design-pro/dataConverge/BusinessSection/result';
import {TableListIsSuccessEnum} from "@/pages/enums";
import {
  getList,
  getBusinessSection,
  removeBusinessSection
} from '@/services/ant-design-pro/dataConverge/BusinessSection/api';
import Add from "@/pages/dataConverge/BusinessSection/Add";
import Update from "@/pages/dataConverge/BusinessSection/Update";

/**
 *  Delete node
 * @zh-CN 删除节点
 *
 * @param selectedRows
 */
const handleRemove = async (selectedRows: BusinessSectionItem[]) => {
  const hide = message.loading('正在删除');
  if (!selectedRows) return true;

  const params: number[] = [];
  selectedRows.map((item) =>{
    params.push(item.id)
  })
  try {
    const result = await removeBusinessSection(params);
    hide();
    message.success(result.msg);
    return true;
  } catch (error) {
    hide();
    message.error('Delete failed, please try again');
    return false;
  }
};


const Post: React.FC = () => {
  /**
   * @en-US Pop-up window of new window
   * @zh-CN 新建窗口的弹窗
   *  */
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  /**
   * @en-US The pop-up window of the distribution update window
   * @zh-CN 分布更新窗口的弹窗
   * */
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<BusinessSectionItem>();
  const [selectedRowsState, setSelectedRows] = useState<BusinessSectionItem[]>([]);

  /**
   * @en-US International configuration
   * @zh-CN 国际化配置
   * */
  const intl = useIntl();

  const columns: ProColumns<BusinessSectionItem>[] = [
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.data.dataConverge.businessSection.name"
          defaultMessage="businessSection name"
        />
      ),
      align: 'center',
      dataIndex: 'name',
      key: 'name',
      renderText: (val: string) => {return val}
    },
    {
      title: <FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.remark" defaultMessage="businessSection remark" />,
      dataIndex: 'remark',
      align: 'center',
      key: 'remark',
      hideInForm: true,
      hideInSearch: true
    },
    {
      title: <FormattedMessage id="pages.searchTable.data.dataConverge.businessSection.orderNum" defaultMessage="businessSection orderNum" />,
      dataIndex: 'orderNum',
      align: 'center',
      hideInSearch: true,
      key: 'orderNum',
      hideInForm: true
    }
    ,
    // {
    //   title: (
    //     <FormattedMessage
    //       id="pages.searchTable.data.dataConverge.businessSection.status"
    //       defaultMessage="businessSection status"
    //     />
    //   ),
    //   dataIndex: 'status',
    //   align: 'center',
    //   valueType: 'select',
    //   key: 'status',
    //   renderText: (val: string) => {return val}
    //   ,
    //   valueEnum: TableListIsSuccessEnum,
    // }
    // ,
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.data.dataConverge.businessSection.createTime"
          defaultMessage="businessSection createTime"
        />
      ),
      render: (_, record) => <span>{record.createTime}</span>,
      search: {
        transform: (value) => {
          return {
            'params[beginTime]': value[0],
            'params[endTime]': value[1],
          }
        }
      },
      // sorter: true,
      dataIndex: 'createTime',
      align: 'center',
      valueType: 'dateRange',
      key: 'createTime',
      renderText: (val: string) => {
        return val;
      }
      ,
    }
    ,
    {
      title: intl.formatMessage({
        id: 'pages.searchTable.titleOption',
        defaultMessage: 'Enquiry Option',
      }),
      dataIndex: 'option',
      valueType: 'option',
      key: 'update',
      align: 'center',
      render: (_, record) => [
        <Button
          type="primary"
          onClick={async () => {
            const result: BusinessSectionItemResult = await getBusinessSection(record.id);
            setCurrentRow(result.data);
            handleUpdateModalVisible(true);
          }}
          shape="round"
        >
          <EditOutlined />  <FormattedMessage id="pages.searchTable.update" defaultMessage="Update"/>
        </Button>
      ],
    },
  ];

  return (
    <PageContainer>
      <ProTable<BusinessSectionItem,  API.Param.BasePageParam>
        headerTitle={intl.formatMessage({
          id: 'pages.searchTable.title',
          defaultMessage: 'Enquiry form',
        })}
        actionRef={actionRef}
        rowKey="id"
        search={{
          labelWidth: 120,
        }}
        toolBarRender={() => [
          <Button
            type="primary"
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <PlusOutlined /> <FormattedMessage id="pages.searchTable.new" defaultMessage="New"/>
          </Button>,
        ]}
        request={getList}
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
          type :'checkbox'
        }}
        pagination={{
          showSizeChanger: true,
          showQuickJumper: true,
          pageSizeOptions: PAGE_SIZE_ARRAY
        }}
      />
      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.searchTable.chosen" defaultMessage="Chosen" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.searchTable.item" defaultMessage="项" />
            </div>
          }
        >
          <Button
            onClick={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
          >
            <FormattedMessage
              id="pages.searchTable.batchDeletion"
              defaultMessage="Batch deletion"
            />
          </Button>
        </FooterToolbar>
      )}

      <Add actionRef={actionRef}
             intl={intl}
             createModalVisible={createModalVisible}
             handleModalVisible={handleModalVisible}
      />

      <Update actionRef={actionRef}
             intl={intl}
             updateModalVisible={updateModalVisible}
             handleUpdateModalVisible={handleUpdateModalVisible}
             currentRow={currentRow}
      />

    </PageContainer>
  );
};

export default Post;
