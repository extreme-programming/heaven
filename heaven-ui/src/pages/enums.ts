export const TableListIsSuccessEnum = {
  0: {
    text: '正常',
    status: 'Success',
  },
  1: {
    text: '停用',
    status: 'Error',
  }
}

export const SourceTypeEnum = {
  1: {
    text: 'FILE',
  },
  2: {
    text: 'JDBC',
  },
  3: {
    text: 'HTTP',
  }
}

export const SourceKind = {
  0: {
    text: '业务数据源',
  },
  1: {
    text: '数仓数据源',
  }
}

export const TaskPriority = {
  0: {
    text: '最低',
  },
  1: {
    text: '低',
  },
  2: {
    text: '中',
  },
  3: {
    text: '高',
  },
  4: {
    text: '最高',
  }
}

export const TaskTimeoutNotifyStrategy = [
  {
    value: 0,
    text: '警告',
  },
  {
    value: 1,
    text: '失败',
  }
]

export const TaskTimeout = {
  0: {
    text: '是',
  },
  1: {
    text: '否',
  }
}

export const TaskRunningState = {
  0: {
    text: '正常',
  },
  1: {
    text: '禁止执行',
  }
}

export const ProcessDefinitionExecutionType = {
  0: {
    text: '并行',
  },
  1: {
    text: '串行等待',
  },
  2: {
    text: '串行抛弃',
  },
  3: {
    text: '串行优先',
  }
}

export const ProcessDefinitionStateEnum = {
  0: {
    text: '上线',
    status: 'Success',
  },
  1: {
    text: '下线',
    status: 'Error',
  }
}
