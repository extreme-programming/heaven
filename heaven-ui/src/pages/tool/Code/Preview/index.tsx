import React from "react";
import { Tabs , Typography } from 'antd';
// @ts-ignore
import Highlight from 'react-highlight'
import "highlight.js/styles/zenburn.css"
import { ReactComponent as JsIcon } from '@/icons/language/js.svg'
import { ReactComponent as XmlIcon } from '@/icons/language/xml.svg'
import { ReactComponent as JavaIcon } from '@/icons/language/java.svg'
import { ReactComponent as SqlIcon } from '@/icons/language/sql.svg'

const {Text } = Typography;

// @ts-ignore
const Preview: React.FC = ({previewData}) => {

  // @ts-ignore
  return (
    <Tabs type='card'
          tabPosition={'left'}>

      {previewData && previewData.map((item, index , arr) => {
        const obj = {};
        const label: string = `${item.key}`.substring(`${item.key}`.lastIndexOf('/') + 1 , `${item.key}`.lastIndexOf('.'));
        if(label.indexOf('java') > -1){
          obj['label'] = <a><JavaIcon style={{fontSize:'16px'}}/><Text strong>{label}</Text></a>
          obj['label'] = <a><JavaIcon style={{fontSize:'16px'}}/> <Text strong>{label}</Text></a>
        }else if(label.indexOf('sql') > -1){
          obj['label'] = <a><SqlIcon style={{fontSize:'16px'}}/><Text strong>{label}</Text></a>
          obj['label'] = <a><SqlIcon style={{fontSize:'16px'}}/> <Text strong>{label}</Text></a>
        }else if(label.indexOf('xml') > -1){
          obj['label'] = <a><XmlIcon style={{fontSize:'16px'}}/><Text strong>{label}</Text></a>
          obj['label'] = <a><XmlIcon style={{fontSize:'16px'}}/> <Text strong>{label}</Text></a>
        }else {
          obj['label'] = <a><JsIcon style={{fontSize:'16px'}}/><Text strong>{label}</Text></a>
          obj['label'] = <a><JsIcon style={{fontSize:'16px'}}/> <Text strong>{label}</Text></a>
        }
        return (
          <Tabs.TabPane tab={obj['label']} key={index}>
            <Highlight className="javascript">{item.value}</Highlight>
          </Tabs.TabPane>
        )
    })}
    </Tabs>
  )
}


export default Preview;
