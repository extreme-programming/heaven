import {ActionType, ProColumns } from '@ant-design/pro-components';
import {
  FooterToolbar,
  PageContainer,
  ProTable,
} from '@ant-design/pro-components';
import { Button, message,Space, Drawer } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage, useIntl } from 'umi';
import {PAGE_SIZE_ARRAY} from "@/pages/constants"
import {
  batchGenCode,
  getList,
  removeGenTable,
  syncGenTable,
  previewGenCode,
  getInfo
} from "@/services/ant-design-pro/tool/Code/api";
import {GenTableItem, GetInfoResult } from "@/services/ant-design-pro/tool/Code/result";
import {
  CloudDownloadOutlined, CloudSyncOutlined, CloudUploadOutlined,
  EyeTwoTone,
  HighlightTwoTone,
} from "@ant-design/icons";
import Import from "@/pages/tool/Code/Import";
import Basic from "@/pages/tool/Code/Update/components/Basic";
import Preview from './Preview';
import Column from './Update/components/Column';

const handleGetInfo = async (id: number) => {
  if (!id){
    message.success('当前id不存在');
    return false;
  }
  try {
    const result = await getInfo(id);
    return result.data;
  } catch (error) {
    message.error('Delete failed, please try again');
    return false;
  }
};

const preview = async (id: number) => {
  if (!id){
    message.success('当前id不存在');
    return false;
  }
  try {
    const result = await previewGenCode(id);
    return result.data;
  } catch (error) {
    message.error('Delete failed, please try again');
    return false;
  }
};

const download = async (currentTableName: string) => {
  if (!currentTableName){
    message.success('当前表名不存在');
    return ;
  }
  await batchGenCode(currentTableName);
};

const sync = async (currentTableName: string) => {
  if (!currentTableName){
    message.success('当前表名不存在');
    return ;
  }
  try {
    const result = await syncGenTable(currentTableName);
    message.success(result.msg);
    return true;
  } catch (error) {
    message.error('Delete failed, please try again');
    return false;
  }
};


/**
 *  Delete node
 * @zh-CN 删除节点
 *
 * @param selectedRows
 */
const handleRemove = async (selectedRows: GenTableItem[]) => {
  const hide = message.loading('正在删除');
  if (!selectedRows) return true;

  const params: number[] = [];
  selectedRows.map((item) =>{
    params.push(item.tableId)
  })
  try {
    const result = await removeGenTable(params);
    hide();
    message.success(result.msg);
    return true;
  } catch (error) {
    hide();
    message.error('Delete failed, please try again');
    return false;
  }
};

const Code: React.FC = () => {
  const [selectedRowsState , setSelectedRows] = useState<GenTableItem[]>([]);
  const [showDetail , setShowDetail] = useState<boolean>(false);
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  const actionRef = useRef<ActionType>();
  const [isModalOpen  , setIsModalOpen] = useState(false);
  const [previewData , setPreviewData] = useState();
  const [infoData , setInfoData] = useState([]);

  const [isBasicOpen  , setIsBasicOpen] = useState(false);
  const [isColumnOpen , setIsColumnOpen] = useState(false);
  const [currentRow , setCurrentRow] = useState();


  /**
   * @en-US International configuration
   * @zh-CN 国际化配置
   * */
  const intl = useIntl();

  const columns: ProColumns<GenTableItem>[] = [
    {
      title: <FormattedMessage id="pages.searchTable.tool.code.tableName" defaultMessage="code tableName" />,
      dataIndex: 'tableName',
      align: 'center',
      hideInSearch: false,
      key: 'tableName',
      hideInForm: false,
      render: (dom , entity) => {
        return (
          <a onClick={async ()=>{
            const data = await handleGetInfo(entity.tableId);
            setInfoData(data);
            setCurrentRow(entity);
            setIsColumnOpen(true);
          }}
          >
            {dom}
          </a>
        )
      }
    },
    {
      title: <FormattedMessage id="pages.searchTable.tool.code.tableComment" defaultMessage="code tableComment" />,
      dataIndex: 'tableComment',
      align: 'center',
      hideInSearch: false,
      key: 'tableComment',
      hideInForm: false,
    },
    {
      title: <FormattedMessage id="pages.searchTable.tool.code.className" defaultMessage="code className" />,
      dataIndex: 'className',
      align: 'center',
      hideInSearch: true,
      key: 'className',
      hideInForm: true
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.tool.code.createTime"
          defaultMessage="code createTime"
        />
      ),
      render: (_, record) => <span>{record.createTime}</span>,
      search: {
        transform: (value) => {
          return {
            'params[beginTime]': value[0],
            'params[endTime]': value[1],
          }
        }
      },
      dataIndex: 'createTime',
      align: 'center',
      valueType: 'dateRange',
      key: 'createTime',
      hideInSearch: false,
      hideInForm: false,
      renderText: (val: string) => {
        return val;
      }
    }
    ,
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.tool.code.updateTime"
          defaultMessage="code updateTime"
        />
      ),
      render: (_, record) => <span>{record.updateTime}</span>,
      search: {
        transform: (value) => {
          return {
            'params[beginTime]': value[0],
            'params[endTime]': value[1],
          }
        }
      },
      dataIndex: 'updateTime',
      align: 'center',
      valueType: 'dateRange',
      key: 'updateTime',
      hideInForm: true,
      hideInTable: false,
      hideInSearch: true,
      renderText: (val: string) => {
        return val;
      }
    },
    {
      title: intl.formatMessage({
        id: 'pages.searchTable.titleOption',
        defaultMessage: 'Enquiry Option',
      }),
      dataIndex: 'option',
      valueType: 'option',
      key: 'update',
      align: 'center',
      render: (_, record) => [
        <Space size="middle">
          <a onClick={async ()=>{
            const data = await preview(record.tableId);
            setShowDetail(true);
            setPreviewData(data);
          }}
          >
            <EyeTwoTone />
            {intl.formatMessage({
              id: 'pages.searchTable.preview',
              defaultMessage: 'preview',
            })}
          </a>
          &nbsp;
          <a onClick={async () => {
            const data = await handleGetInfo(record.tableId);
            setInfoData(data);
            setIsBasicOpen(true);
          }}>
            <HighlightTwoTone />
            {intl.formatMessage({
              id: 'pages.searchTable.update',
              defaultMessage: 'update',
            })}
          </a>
          &nbsp;
          <a onClick={()=>{sync(record.tableName)}}>
            <CloudSyncOutlined />
            {intl.formatMessage({
              id: 'pages.searchTable.sync',
              defaultMessage: 'sync',
            })}
          </a>
          &nbsp;
          <a onClick={()=>{download(record.tableName)}}>
            <CloudDownloadOutlined />
            {intl.formatMessage({
              id: 'pages.searchTable.download',
              defaultMessage: 'download',
            })}
          </a>
        </Space>
      ]
    },
  ];

  // @ts-ignore
  // @ts-ignore
  return (
    <PageContainer>
      <ProTable<GenTableItem, API.Param.BasePageParam>
        headerTitle={intl.formatMessage({
          id: 'pages.searchTable.title',
          defaultMessage: 'Enquiry form',
        })}
        actionRef={actionRef}
        rowKey="tableId"
        search={{
          labelWidth: 120,
        }}
        request={getList}
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
          type :'checkbox'
        }}
        toolBarRender={() => [
          <Button
            type="primary"
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <CloudUploadOutlined /> <FormattedMessage id="pages.searchTable.import" defaultMessage="import"/>
          </Button>,
        ]}
        pagination={{
          showSizeChanger: true,
          showQuickJumper: true,
          pageSizeOptions: PAGE_SIZE_ARRAY
        }}
      />

      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.searchTable.chosen" defaultMessage="Chosen" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.searchTable.item" defaultMessage="项" />
            </div>
          }
        >
          <Button
            onClick={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
          >
            <FormattedMessage
              id="pages.searchTable.batchDeletion"
              defaultMessage="Batch deletion"
            />
          </Button>
        </FooterToolbar>
      )}

      <Drawer
        width={'60%'}
        visible={showDetail}
        key={"showDetailKey"}
        onClose={() => {
          setPreviewData(undefined);
          setShowDetail(false);
        }}
        closable={false}
      >
        {previewData && (
          <Preview previewData={previewData}/>
        )}
      </Drawer>

      {/*<Drawer*/}
      {/*  key={"showColumnKey"}*/}
      {/*  width={'50%'}*/}
      {/*  visible={showColumn}*/}
      {/*  onClose={() => {*/}
      {/*    setCurrentRow(undefined);*/}
      {/*    setShowColumn(false);*/}
      {/*  }}*/}
      {/*  closable={false}*/}
      {/*>*/}
      {/*  {currentRow?.tableId && (*/}
      {/*    <ProDescriptions<OperateLogItem>*/}
      {/*      tooltip={intl.formatMessage({*/}
      {/*        id: 'pages.searchTable.log.operateLog.businessType',*/}
      {/*        defaultMessage: 'operateLog businessType',*/}
      {/*      })}*/}
      {/*      column={2}*/}
      {/*      title={currentRow?.title}*/}
      {/*      request={async () => {*/}
      {/*        return {data: currentRow || {}}*/}
      {/*      }}*/}
      {/*      columns={columns as ProDescriptionsItemProps<OperateLogItem>[]}*/}
      {/*    />*/}
      {/*  )}*/}
      {/*</Drawer>*/}

      <Import actionRef={actionRef}
              intl={intl}
              createModalVisible={createModalVisible}
              handleModalVisible={handleModalVisible}
      />

      <Basic  actionRef={actionRef}
              isBasicOpen={isBasicOpen}
              setIsBasicOpen={setIsBasicOpen}
              intl={intl}
              infoData={infoData}
      />

      <Column actionRef={actionRef}
              isColumnOpen={isColumnOpen}
              setIsColumnOpen={setIsColumnOpen}
              intl={intl}
              infoData={infoData}
      />

    </PageContainer>
  );
};

export default Code;
