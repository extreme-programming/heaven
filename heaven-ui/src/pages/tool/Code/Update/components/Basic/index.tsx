import React, {useEffect, useRef, useState} from "react";
import {ProFormGroup, ProFormRadio, ProFormSelect,ModalForm, ProFormText, ProFormTreeSelect} from "@ant-design/pro-components";
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {getTreeList} from "@/services/ant-design-pro/system/Menu/api";
import {handleTree} from "@/services/ant-design-pro/system/Dept/api";
import {ProFormInstance} from "@ant-design/pro-form";
import {handleBasic} from "@/services/ant-design-pro/tool/Code/api";
import {message} from "antd";

// @ts-ignore
const Basic: React.FC = ({intl, infoData,isBasicOpen, setIsBasicOpen }) => {
  const formRef = useRef<ProFormInstance>();

  // const  useState();
  const [tplType, setTplType] = useState(infoData.info === undefined ? "" : infoData.info['tplCategory']);

  useEffect(function (){
    if(infoData.info){
      setTplType( infoData.info['tplCategory']);
    }
  },[infoData])

  const initSelect = () => {
    const data = {};
    if(infoData.rows){
      infoData.rows.map((item) => {
        const key = item.columnName;
        const val = item.columnName + " : " + item.columnComment;
        data[key] = val;
      });
    }
    return data;
  }

  // @ts-ignore
  return (
      <ModalForm
        title={intl.formatMessage({
          id: 'pages.searchTable.tool.code.updateBasic',
          defaultMessage: 'code updateBasic',
        })}
        modalProps={{
          destroyOnClose: true
        }}
        width={'90%'}
        formRef={formRef}
        key="UpdateForm"
        visible={isBasicOpen}
        onVisibleChange={setIsBasicOpen}
        onFinish={async (value) => {
          if(value['params.treeCode'] && value['params.treeParentCode'] && value['params.treeName'] && value['params.parentMenuId']){
            const params = {treeCode:value['params.treeCode'] ,treeParentCode:value['params.treeParentCode'] ,treeName:value['params.treeName'], parentMenuId: value['params.parentMenuId']}
            value.params = params;
          }
          const result = await handleBasic(value);
          if (result && result.code === 200) {
            message.success(result.msg);
          } else {
            message.error(result.msg);
          }
          setIsBasicOpen(false);
        }}
      >
      <ProFormGroup>
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.tool.code.tableName"
                  defaultMessage="code tableName"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.tool.code.tableName" defaultMessage="code tableName"/>}
          name="tableName"
          width={'md'}
          initialValue={infoData.info ? infoData.info['tableName'] : ""}
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="pages.searchTable.tool.code.tableComment" defaultMessage="code tableComment"/>
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.tool.code.tableComment" defaultMessage="code tableComment"/>}
          name="tableComment"
          width={'md'}
          initialValue={infoData.info ? infoData.info['tableComment'] : ""}
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="pages.searchTable.tool.code.className" defaultMessage="code className"/>
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.tool.code.className" defaultMessage="code className"/>}
          name="className"
          width={'md'}
          initialValue={infoData.info ? infoData.info['className'] : ""}
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage id="pages.searchTable.tool.code.functionAuthor" defaultMessage="code functionAuthor"/>
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.tool.code.functionAuthor"
                                   defaultMessage="code functionAuthor"/>}
          name="functionAuthor"
          width={'md'}
          initialValue={infoData.info ? infoData.info['functionAuthor'] : ""}
        />
      </ProFormGroup>
      <ProFormGroup>
      <ProFormText
        rules={[
          {
            message: (
              <FormattedMessage id="pages.searchTable.tool.code.remark" defaultMessage="code remark"/>
            )
          },
        ]}
        label={<FormattedMessage id="pages.searchTable.tool.code.remark" defaultMessage="code remark"/>}
        name="remark"
        width={'md'}
        initialValue={infoData.info ? infoData.info['remark'] : ""}

      />

    </ProFormGroup>
      {/*<Divider orientation="center">*/}
      {/*  <FormattedMessage id="pages.searchTable.tool.code.generate" defaultMessage="code generate"/>*/}
      {/*</Divider>*/}
      <ProFormGroup>
        <ProFormSelect
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.tool.code.tplCategory"
                  defaultMessage="code tplCategory"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.tool.code.tplCategory"
                                   defaultMessage="code tplCategory"/>}
          name="tplCategory"
          valueEnum={{
            'crud': '单表(增删改查)',
            'tree': '树表(增删改查)',
          }}
          width={'md'}
          initialValue={infoData.info ? infoData.info['tplCategory'] : ""}
          fieldProps={{
            onChange: (value,option)=>{
              setTplType(value);
            }
          }}
        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.tool.code.packageName"
                  defaultMessage="code packageName"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.tool.code.packageName" defaultMessage="code packageName"/>}
          name="packageName"
          width={'md'}
          initialValue={infoData.info ? infoData.info['packageName'] : ""}

        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.tool.code.moduleName"
                  defaultMessage="code moduleName"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.tool.code.moduleName" defaultMessage="code moduleName"/>}
          name="moduleName"
          width={'md'}
          initialValue={infoData.info ? infoData.info['moduleName'] : ""}

        />
        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.tool.code.businessName"
                  defaultMessage="code businessName"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.tool.code.businessName" defaultMessage="code businessName"/>}
          name="businessName"
          width={'md'}
          initialValue={infoData.info ? infoData.info['businessName'] : ""}

        />
      </ProFormGroup>
      <ProFormGroup>
        <ProFormText
          rules={[
            {
              required: false,
              message: (
                <FormattedMessage
                  id="pages.searchTable.tool.code.functionName"
                  defaultMessage="code functionName"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.tool.code.functionName" defaultMessage="code functionName"/>}
          name="functionName"
          width={'md'}
          initialValue={infoData.info ? infoData.info['functionName'] : ""}
        />
        <ProFormTreeSelect
          label={<FormattedMessage id="pages.searchTable.system.menu.SuperiorMenu"
                                   defaultMessage="Superior Menu"/>}
          name="params.parentMenuId"
          request={async  () => {
            const response = await getTreeList()
            const {  data } = response;
            const deptList = handleTree(data, "menuId" , "menuName");
            return deptList;
          }}
          width={'md'}
          required={false}
          initialValue={infoData.info ? infoData.info['parentMenuId'] : ""}
        />
        <ProFormRadio.Group
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.tool.code.genType"
                  defaultMessage="code genType"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.tool.code.genType" defaultMessage="code genType"/>}
          width="md"
          name="genType"
          options={[
            {
              label: "压缩包",
              value: "0"
            }
          ]}
          // initialValue={'0'}
          initialValue={infoData.info ? infoData.info['genType'] : ""}
        />
      </ProFormGroup>
        {(tplType === 'tree') && (
          <ProFormGroup>
              <ProFormSelect
                rules={[
                  {
                    required: true,
                    message: (
                      <FormattedMessage
                        id="pages.searchTable.tool.code.tplCategory"
                        defaultMessage="code tplCategory"
                      />
                    ),
                  },
                ]}
                label={<FormattedMessage id="pages.searchTable.tool.code.treeCode"
                                         defaultMessage="code treeCode"/>}
                name="params.treeCode"
                valueEnum={initSelect}
                width={'md'}
                initialValue={infoData.info ? infoData.info['treeCode'] : ""}
              />
              <ProFormSelect
                rules={[
                  {
                    required: true,
                    message: (
                      <FormattedMessage
                        id="pages.searchTable.tool.code.tplCategory"
                        defaultMessage="code tplCategory"
                      />
                    ),
                  },
                ]}
                label={<FormattedMessage id="pages.searchTable.tool.code.treeParentCode"
                                         defaultMessage="code treeParentCode"/>}
                name="params.treeParentCode"
                width={'md'}
                valueEnum={initSelect}
                initialValue={infoData.info ? infoData.info['treeParentCode'] : ""}
              />
              <ProFormSelect
                rules={[
                  {
                    required: true,
                    message: (
                      <FormattedMessage
                        id="pages.searchTable.tool.code.tplCategory"
                        defaultMessage="code tplCategory"
                      />
                    ),
                  },
                ]}
                label={<FormattedMessage id="pages.searchTable.tool.code.treeName"
                                         defaultMessage="code treeName"/>}
                name="params.treeName"
                valueEnum={initSelect}
                width={'md'}
                initialValue={infoData.info ? infoData.info['treeName'] : ""}
              />
            </ProFormGroup>
        )}

        <ProFormText
          rules={[
            {
              required: true,
              message: (
                <FormattedMessage
                  id="pages.searchTable.tool.code.tableId"
                  defaultMessage="code tableId"
                />
              ),
            },
          ]}
          label={<FormattedMessage id="pages.searchTable.tool.code.tableName" defaultMessage="code tableId"/>}
          name="tableId"
          width={'md'}
          initialValue={infoData.info ? infoData.info['tableId'] : ""}
          hidden={true}
        />
      </ModalForm>
  )
}


export default Basic;
