import type { ProColumns } from '@ant-design/pro-components';
import { EditableProTable } from '@ant-design/pro-components';
import {message, Modal} from 'antd';
import React, {useEffect, useState} from 'react';
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import { EditTwoTone, CloseCircleTwoTone , StopTwoTone , SaveTwoTone} from "@ant-design/icons";
import {GenColumnItem} from "@/services/ant-design-pro/tool/Code/result";
import { RecordKey} from "@ant-design/pro-utils/lib/useEditableArray";
import {getDictTypeList} from "@/services/ant-design-pro/system/Dict/api";
import {handleBasic, handleBasicColumn} from "@/services/ant-design-pro/tool/Code/api";
import {ResultCode} from "@/pages/constants";


const Column: React.FC = ({intl, infoData,isColumnOpen, setIsColumnOpen }) => {
  const [editableKeys, setEditableRowKeys] = useState<React.Key[]>(() =>
    infoData.map((item) => item.rows.columnId),
  );

  const onChange = async () => {

  }

  const handleDictTypeList = async () => {
    const data = await getDictTypeList();
    const dictTypes = {};
    data.rows.map((item) => {
      dictTypes[item.dictType] = item.dictType;
    });
    setDictTypes(dictTypes);
  }

  useEffect(function(){
    handleDictTypeList();
    setData(infoData === undefined ? [] : infoData.rows);
  },[infoData]);

  const [data , setData] = useState(infoData.rows);
  const [dictTypes , setDictTypes] = useState({});

  const handleModalOk = async () => {

    const result = await handleBasicColumn(data);
    if(result.code === ResultCode.SUCCESS_CODE){
      message.success(result.msg);
      setData([]);
    }else{
      message.error(result.msg);
    }
    setIsColumnOpen(false);
  }

  const handleCancel = () =>{
    setIsColumnOpen(false);
  }

  const handleDelete = (key: RecordKey, row: (GenColumnItem & {index?: number | undefined})) =>{
    // setData([...row , ])
    let arr = JSON.parse(
      JSON.stringify(data)
    )
    arr.filter(item => {
      item.columnId === key});
    setData(arr)
  }

  const handleSave = (key, record) =>{
    setData([...data , record])
  }

  const handleTableCancel = (key, record, originRow) => {
  }

  const columns: ProColumns<GenColumnItem>[] = [
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.tool.code.column.columnName"
          defaultMessage="column columnName"
        />
      ),
      align: 'center',
      dataIndex: 'columnName',
      key: 'columnName'
    },
    ,
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.tool.code.column.columnComment"
          defaultMessage="column columnComment"
        />
      ),
      align: 'center',
      dataIndex: 'columnComment',
      key: 'columnComment'
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.tool.code.column.columnType"
          defaultMessage="column columnType"
        />
      ),
      align: 'center',
      dataIndex: 'columnType',
      key: 'columnType'
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.tool.code.column.javaType"
          defaultMessage="column javaType"
        />
      ),
      align: 'center',
      dataIndex: 'javaType',
      key: 'javaType',
      valueType: 'select',
      valueEnum: {
        Long: {
          text: 'Long'
        },
        String: {
          text: 'String'
        },
        Integer: {
          text: 'Integer'
        },
        Double: {
          text: 'Double'
        },
        BigDecimal: {
          text: 'BigDecimal'
        },
        Date: {
          text: 'Date'
        },
        Boolean: {
          text: 'Boolean'
        }
      }
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.tool.code.column.javaField"
          defaultMessage="column javaField"
        />
      ),
      align: 'center',
      dataIndex: 'javaField',
      key: 'javaField',
      renderText: (val: string) => {return val}
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.tool.code.column.isInsert"
          defaultMessage="column isInsert"
        />
      ),
      align: 'center',
      dataIndex: 'isInsert',
      key: 'isInsert',
      valueType: 'radio',
      valueEnum: {
        '1': '是',
        '0': '否'
      },
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.tool.code.column.isEdit"
          defaultMessage="column isEdit"
        />
      ),
      align: 'center',
      dataIndex: 'isEdit',
      key: 'isEdit',
      valueType: 'radio',
      valueEnum: {
        '1': '是',
        '0': '否'
      },
      fieldProps: {
        onChange: {onChange}
      }
      // valueEnum: {
      //   0: {
      //     text: 'Date'
      //   }
      //   1: {
      //     text: 'Boolean'
      //   }
      // }
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.tool.code.column.isList"
          defaultMessage="column isList"
        />
      ),
      align: 'center',
      dataIndex: 'isList',
      key: 'isList',
      valueType: 'radio',
      valueEnum: {
        '1': '是',
        '0': '否'
      },
    }
    ,
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.tool.code.column.isQuery"
          defaultMessage="column isQuery"
        />
      ),
      align: 'center',
      dataIndex: 'isQuery',
      key: 'isQuery',
      valueType: 'radio',
      valueEnum: {
        '1': '是',
        '0': '否'
      },
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.tool.code.column.queryType"
          defaultMessage="column queryType"
        />
      ),
      align: 'center',
      dataIndex: 'queryType',
      key: 'queryType',
      valueType: 'select',
      //EQ等于、NE不等于、GT大于、LT小于、LIKE模糊、BETWEEN范围
      valueEnum: {
        'EQ': {
          text: '='
        },
        'NE': {
          text: '!='
        },
        'GT': {
          text: '>'
        },
        'GTE': {
          text: '>='
        },
        'LT': {
          text: '<'
        },
        'LTE': {
          text: '<='
        },
        'LIKE': {
          text: 'LIKE'
        },
        'BETWEEN': {
          text: 'BETWEEN'
        }
      }
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.tool.code.column.isRequired"
          defaultMessage="column isRequired"
        />
      ),
      align: 'center',
      dataIndex: 'isRequired',
      key: 'isRequired',
      valueType: 'radio',
      valueEnum: {
        '1': '是',
        '0': '否'
      },
    },
    //显示类型（input文本框、textarea文本域、select下拉框、checkbox复选框、radio单选框、datetime日期控件、image图片上传控件、upload文件上传控件、）
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.tool.code.column.htmlType"
          defaultMessage="column htmlType"
        />
      ),
      align: 'center',
      dataIndex: 'htmlType',
      key: 'htmlType',
      valueType: 'select',
      valueEnum: {
        'input': {
          text: '文本框'
        },
        'textarea': {
          text: '文本域'
        },
        'select': {
          text: '下拉框'
        },
        'checkbox': {
          text: '复选框'
        },
        'radio': {
          text: '单选框'
        },
        'datetime': {
          text: '日期控件'
        },
        'image': {
          text: '图片上传控件'
        },
        'upload': {
          text: '文件上传控件'
        },
        'editor': {
          text: '富文本控件'
        }
      }
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.tool.code.column.dictType"
          defaultMessage="column dictType"
        />
      ),
      align: 'center',
      dataIndex: 'dictType',
      key: 'dictType',
      valueType: 'select',
      valueEnum: dictTypes
    },
    {
      title: intl.formatMessage({
        id: 'pages.searchTable.titleOption',
        defaultMessage: 'code updateColumn',
      }),
      valueType: 'option',
      align: 'center',
      render: (text, record, _, action) => [
        <a
          key="editable"
          onClick={() => {
            action?.startEditable?.(record.columnId);
          }}
        >
          <EditTwoTone />
        </a>
      ],
    },
  ];

  // @ts-ignore
  return (
    <Modal
      visible={isColumnOpen}
      //open={isColumnOpen}
      onCancel={handleCancel}
      width={'100%'}
      onOk={handleModalOk}
    >
      <EditableProTable<GenColumnItem>
        headerTitle={intl.formatMessage({
          id: 'pages.searchTable.tool.code.updateColumn',
          defaultMessage: 'code updateColumn',
        })}
        columns={columns}
        rowKey="columnId"
        value={data}
        // onChange={setDataSource}
        recordCreatorProps={false}
        editable={{
          type: 'multiple',
          editableKeys,
          actionRender: (row, config, defaultDoms) => {
            return [
              defaultDoms.delete, defaultDoms.save ,defaultDoms.cancel
            ];
          },
          onValuesChange: (record, recordList) => {
            setData(recordList);
          },
          onChange: setEditableRowKeys,
          cancelText:  <StopTwoTone />,
          saveText: <SaveTwoTone />,
          deleteText: <CloseCircleTwoTone />,
          onDelete: handleDelete,
          onSave: handleSave,
          onCancel: handleTableCancel
        }}
      />
    </Modal>
  );
};

export default Column;
