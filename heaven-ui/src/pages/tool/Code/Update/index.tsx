import React, {useState} from "react";
import { Tabs  , Modal } from 'antd';
// @ts-ignore
import Basic from  './components/Basic';
import Column from './components/Column';
import {ModalForm} from "@ant-design/pro-components";


// @ts-ignore
const Update: React.FC = ({intl ,  isModalOpen , setIsModalOpen , infoData}) => {

  const handleOpen = () => {
    setIsModalOpen(false)
  }

  const arr = [
    {label: intl.formatMessage({
        id: 'pages.searchTable.tool.code.basic',
        defaultMessage: 'code basic',
      }) , key:1 , children:<Basic intl={intl} infoData={infoData}/>},
    {label:  intl.formatMessage({
        id: 'pages.searchTable.tool.code.folumn',
        defaultMessage: 'code folumn',
      }) , key:2 , children:<Column intl={intl} infoData={infoData}/>},
    {label:  intl.formatMessage({
        id: 'pages.searchTable.tool.code.create',
        defaultMessage: 'code create',
      }) , key:3 , children:<Generate intl={intl} infoData={infoData}/>}
  ]

  // @ts-ignore
  return (
    <ModalForm
      title={intl.formatMessage({
        id: 'pages.searchTable.tool.code.updateBasic',
        defaultMessage: 'code updateBasic',
      })}
      modalProps={{
        destroyOnClose: true
      }}
      width={'90%'}
      key="UpdateForm"
      visible={isModalOpen}
      onVisibleChange={setIsModalOpen}
      onFinish={async (value) => {
        // const success = await handleAdd(value as DictItem);
        //
        // if (success) {
        //   handleModalVisible(false);
        //   if (actionRef.current) {
        //     actionRef.current.reload();
        //   }
        // }
        setIsModalOpen(false);
      }}
    >
      <Tabs
        type='card'
        tabPosition={'top'}
        items={arr.map((item, index , arr) => {
          return item;
        })}
      />
    </ModalForm>
  )
}


export default Update;
