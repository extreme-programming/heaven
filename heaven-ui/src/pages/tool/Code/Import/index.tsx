import React, {useState} from "react";
import {ModalForm, ProTable, ProColumns} from "@ant-design/pro-components";
import {FormattedMessage} from "@@/plugin-locale/localeExports";
import {PAGE_SIZE_ARRAY, ResultCode} from "@/pages/constants";
import {GenTableItem} from "@/services/ant-design-pro/tool/Code/result";
import {getDbList, importGenTable} from "@/services/ant-design-pro/tool/Code/api";
import { message} from "antd";


const handleImport = async (fields: string) => {
  const hide = message.loading('正在导入');
  const result = await importGenTable(fields);
  try {
    if(result.code === ResultCode.SUCCESS_CODE){
      message.success(result.msg);
    }
    hide();
    return true;
  } catch (error) {
    hide();
    message.error(result.msg);
    return false;
  }
};

// @ts-ignore
const Import: React.FC = ({actionRef , intl  ,createModalVisible , handleModalVisible }) => {
  const [selectedRowsState, setSelectedRows] = useState<GenTableItem[]>([]);

  const columns: ProColumns<GenTableItem>[] = [
    {
      title: <FormattedMessage id="pages.searchTable.tool.code.tableId" defaultMessage="code tableId" />,
      dataIndex: 'tableId',
      align: 'center',
      key: 'tableIdImport',
      hideInForm: true,
      hideInSearch: true,
      hideInDescriptions: true,
      hideInTable: true
    },
    {
      title: <FormattedMessage id="pages.searchTable.tool.code.tableName" defaultMessage="code tableName" />,
      dataIndex: 'tableName',
      hideInSearch: false,
      key: 'tableName',
      hideInForm: false,
      align: 'center',
    },
    {
      title: <FormattedMessage id="pages.searchTable.tool.code.tableComment" defaultMessage="code tableComment" />,
      dataIndex: 'tableComment',
      hideInSearch: false,
      key: 'tableComment',
      hideInForm: false,
      align: 'center',
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.tool.code.createTime"
          defaultMessage="code createTime"
        />
      ),
      render: (_, record) => <span>{record.createTime}</span>,
      search: {
        transform: (value) => {
          return {
            'params[beginTime]': value[0],
            'params[endTime]': value[1],
          }
        }
      },
      dataIndex: 'createTime',
      align: 'center',
      valueType: 'dateRange',
      key: 'createTime',
      hideInSearch: true,
      hideInForm: true,
      renderText: (val: string) => {
        return val;
      }
    }
    ,
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.tool.code.updateTime"
          defaultMessage="code updateTime"
        />
      ),
      render: (_, record) => <span>{record.updateTime}</span>,
      search: {
        transform: (value) => {
          return {
            'params[beginTime]': value[0],
            'params[endTime]': value[1],
          }
        }
      },
      dataIndex: 'updateTime',
      align: 'center',
      valueType: 'dateRange',
      key: 'updateTime',
      hideInForm: true,
      hideInTable: false,
      hideInSearch: true,
      renderText: (val: string) => {
        return val;
      }
    }
  ];

  // @ts-ignore
  return (
    <ModalForm
      modalProps={{
        destroyOnClose: true
      }}
      key="import"
      width="1000px"
      visible={createModalVisible}
      onVisibleChange={handleModalVisible}
      onFinish={async () => {
        if(selectedRowsState.length <= 0){
          message.success('请勾选数据');
          return ;
        }
        let param = "";
        selectedRowsState.map((item)=>{
          param += item.tableName + ",";
        })
        if(param[ param.length - 1] === ",")
          param = param.substring(0,param.length - 1);
        const success = await handleImport(param as string);
        if (success) {
          handleModalVisible(false);
          if (actionRef.current) {
            actionRef.current.reload();
          }
        }
      }}
    >
      <ProTable<GenTableItem, API.Param.BasePageParam>
        headerTitle={intl.formatMessage({
          id: 'pages.searchTable.tool.code.import.title',
          defaultMessage: 'code import',
        })}
        actionRef={actionRef}
        rowKey={(record: any, index: number) => (`${record?.key ?? ''}${index.toString()}`)}
        search={{
          labelWidth: 120,
          filterType: 'light'
        }}
        request={getDbList}
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
          type :'checkbox'
        }}
        pagination={{
          showSizeChanger: true,
          showQuickJumper: true,
          pageSizeOptions: PAGE_SIZE_ARRAY
        }}
      />
    </ModalForm>
  )
}


export default Import;
