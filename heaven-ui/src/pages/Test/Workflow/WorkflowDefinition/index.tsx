import { PlusOutlined , EditOutlined } from '@ant-design/icons';
import {ActionType, ProColumns } from '@ant-design/pro-components';
import {
  FooterToolbar,
  PageContainer,
  ProTable,
} from '@ant-design/pro-components';
import { Button, message } from 'antd';
import React, { useRef, useState } from 'react';
import { FormattedMessage, useIntl } from 'umi';
import { getList, remove } from "@/services/ant-design-pro/dataScheduler/WorkFlow/api";
import {PAGE_SIZE_ARRAY} from "@/pages/constants"
import {WorkFlowItem, WorkFlowPageList} from "@/services/ant-design-pro/dataScheduler/WorkFlow/result";
import {TableListIsSuccessEnum} from "@/pages/enums";
import Add from './Add';



/**
 *  Delete node
 * @zh-CN 删除节点
 *
 * @param selectedRows
 */
const handleRemove = async (selectedRows: WorkFlowItem[]) => {
  const hide = message.loading('正在删除');
  if (!selectedRows) return true;

  const params: number[] = [];
  selectedRows.map((item) =>{
    params.push(item.dictId)
  })
  try {
    const result = await remove(params);
    hide();
    message.success(result.msg);
    return true;
  } catch (error) {
    hide();
    message.error('Delete failed, please try again');
    return false;
  }
};


const Dict: React.FC = () => {
  /**
   * @en-US Pop-up window of new window
   * @zh-CN 新建窗口的弹窗
   *  */
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  /**
   * @en-US The pop-up window of the distribution update window
   * @zh-CN 分布更新窗口的弹窗
   * */
  const [updateModalVisible, handleUpdateModalVisible] = useState<boolean>(false);
  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<WorkFlowItem>();
  const [selectedRowsState, setSelectedRows] = useState<WorkFlowItem[]>([]);

  /**
   * @en-US International configuration
   * @zh-CN 国际化配置
   * */
  const intl = useIntl();

  const columns: ProColumns<WorkFlowItem>[] = [
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.data.dataSchedule.processDefinition.name"
          defaultMessage="processDefinition name"
        />
      ),
      align: 'center',
      dataIndex: 'name',
      key: 'name',
      renderText: (val: string) => {return val},
      width: 'md'
    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.data.dataSchedule.processDefinition.releaseState"
          defaultMessage="processDefinition releaseState"
        />
      ),
      dataIndex: 'status',
      align: 'center',
      valueType: 'select',
      key: 'status',
      hideInForm: true,
      hideInSearch: true,
      renderText: (val: string) => {return val}
      ,
      valueEnum: TableListIsSuccessEnum,
      width: 80
    }
    ,
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.data.dataSchedule.processDefinition.createBy"
          defaultMessage="processDefinition createBy"
        />
      ),
      align: 'center',
      dataIndex: 'createBy',
      key: 'createBy',
      hideInForm: true,
      hideInSearch: true,
      renderText: (val: string) => {return val},
      width: 160

    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.data.dataSchedule.processDefinition.createTime"
          defaultMessage="processDefinition createTime"
        />
      ),
      render: (_, record) => <span>{record.createTime}</span>,
      search: {
        transform: (value) => {
          return {
            'params[beginTime]': value[0],
            'params[endTime]': value[1],
          }
        }
      },
      // sorter: true,
      dataIndex: 'createTime',
      align: 'center',
      valueType: 'dateRange',
      key: 'createTime',
      renderText: (val: string) => {
        return val;
      }
      ,
    }
    ,
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.data.dataSchedule.processDefinition.updateBy"
          defaultMessage="processDefinition updateBy"
        />
      ),
      align: 'center',
      dataIndex: 'updateBy',
      key: 'updateBy',
      hideInForm: true,
      hideInSearch: true,
      renderText: (val: string) => {return val},
      width: 160

    },
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.data.dataSchedule.processDefinition.updateTime"
          defaultMessage="processDefinition updateTime"
        />
      ),
      render: (_, record) => <span>{record.updateTime}</span>,
      dataIndex: 'updateTime',
      align: 'center',
      valueType: 'dateRange',
      key: 'updateTime',
      hideInForm: true,
      hideInSearch: true,
      renderText: (val: string) => {
        return val;
      }
      ,
    }
    ,
    {
      title: (
        <FormattedMessage
          id="pages.searchTable.data.dataSchedule.processDefinition.timeState"
          defaultMessage="processDefinition timeState"
        />
      ),
      dataIndex: 'timeState',
      align: 'center',
      valueType: 'select',
      key: 'timeState',
      hideInForm: true,
      hideInSearch: true,
      renderText: (val: string) => {return val}
      ,
      valueEnum: TableListIsSuccessEnum,
      width: 80
    }
    ,
    {
      title: intl.formatMessage({
        id: 'pages.searchTable.titleOption',
        defaultMessage: 'Enquiry Option',
      }),
      dataIndex: 'option',
      valueType: 'option',
      key: 'update',
      align: 'center',
      width: 300,
      render: (_, record) => [
        <Button
          type="primary"
          onClick={async () => {
            const result: WorkFlowItemResult = await getDictType(record.dictId);
            setCurrentRow(result.data);
            handleUpdateModalVisible(true);
          }}
          shape="round"
        >
          <EditOutlined />  <FormattedMessage id="pages.searchTable.update" defaultMessage="Update"/>
        </Button>
      ],
    },
  ];


  const cache = React.useMemo<{ app: IApplication } | null>(
    () => ({
      app: null,
    }),
    [],
  )

  // @ts-ignore
  return (
    <PageContainer>
      <ProTable<WorkFlowItem, API.Param.BasePageParam>
        headerTitle={intl.formatMessage({
          id: 'pages.searchTable.title',
          defaultMessage: 'Enquiry form',
        })}
        actionRef={actionRef}
        rowKey="id"
        search={{
          labelWidth: 120,
        }}
        toolBarRender={() => [
          <Button
            type="primary"
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <PlusOutlined /> <FormattedMessage id="pages.searchTable.new" defaultMessage="New"/>
          </Button>,
        ]}
        request={getList}
        columns={columns}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
          type :'checkbox'
        }}
        pagination={{
          showSizeChanger: true,
          showQuickJumper: true,
          pageSizeOptions: PAGE_SIZE_ARRAY
        }}
      />
      {selectedRowsState?.length > 0 && (
        <FooterToolbar
          extra={
            <div>
              <FormattedMessage id="pages.searchTable.chosen" defaultMessage="Chosen" />{' '}
              <a style={{ fontWeight: 600 }}>{selectedRowsState.length}</a>{' '}
              <FormattedMessage id="pages.searchTable.item" defaultMessage="项" />
            </div>
          }
        >
          <Button
            onClick={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
          >
            <FormattedMessage
              id="pages.searchTable.batchDeletion"
              defaultMessage="Batch deletion"
            />
          </Button>
        </FooterToolbar>
      )}

      <Add actionRef={actionRef}
           intl={intl}
           createModalVisible={createModalVisible}
           handleModalVisible={handleModalVisible}
           cache={cache}
      />

      {/*<UpdateForm actionRef={actionRef}*/}
      {/*            intl={intl}*/}
      {/*            updateModalVisible={updateModalVisible}*/}
      {/*            handleUpdateModalVisible={handleUpdateModalVisible}*/}
      {/*            currentRow={currentRow}*/}
      {/*/>*/}
    </PageContainer>
  );
};

export default Dict;
