﻿export default [
  {
    path: '/user',
    layout: false,
    routes: [
      {
        name: 'login',
        path: '/user/login',
        component: './system/Login',
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/welcome',
    name: 'welcome',
    icon: 'smile',
    component: './Welcome',
  },
  {
    path: '/admin',
    name: 'admin',
    icon: 'crown',
    access: 'canAdmin',
    routes: [
      {
        path: '/admin/sub-page',
        name: 'sub-page',
        icon: 'smile',
        component: './Welcome',
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/system',
    name: 'system',
    icon: 'crown',
    routes: [
      {
        path: '/system/user',
        name: 'system.user',
        icon: 'smile',
        component: './system/User',
      },
      {
        path: '/system/role',
        name: 'system.role',
        icon: 'smile',
        component: './system/Role',
      },
      {
        path: '/system/menu',
        name: 'system.menu',
        icon: 'smile',
        component: './system/Menu',
      },
      {
        path: '/system/dept',
        name: 'system.dept',
        icon: 'smile',
        component: './system/Dept',
      },
      {
        path: '/system/post',
        name: 'system.post',
        icon: 'smile',
        component: './system/Post',
      },
      {
        path: '/system/dict',
        name: 'system.dict',
        icon: 'smile',
        component: './system/Dict',
      },
      {
        path: '/system/config',
        name: 'system.config',
        icon: 'smile',
        component: './system/Config',
      },
      {
        path: '/system/log',
        name: 'system.log',
        icon: 'log',
        routes: [
          {
            path: '/system/user',
            name: 'system.user',
            icon: 'smile',
            component: './system/User',
          },
        ]
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/log',
    name: 'log',
    icon: 'log',
    routes: [
      {
        path: '/log/logininfor',
        name: 'log.logininfor',
        icon: 'smile',
        component: './log/Logininfor',
      },
      {
        path: '/log/operateLog',
        name: 'log.operateLog',
        icon: 'smile',
        component: './log/OperateLog',
      }
    ]
  },
  {
    path: '/tool',
    name: 'tool',
    icon: 'tool',
    routes: [
      {
        path: '/tool/code',
        name: 'tool.code',
        icon: 'smile',
        component: './tool/Code',
      }
    ]
  },
  {
    path: '/dataConverge',
    name: 'dataConverge',
    icon: 'dataConverge',
    routes: [
      {
        path: '/dataConverge/source',
        name: 'dataConverge.source',
        icon: 'smile',
        component: './dataConverge/Source',
      },
      {
        path: '/dataConverge/businessSection',
        name: 'dataConverge.businessSection',
        icon: 'smile',
        component: './dataConverge/BusinessSection',
      },
      {
        path: '/dataConverge/dataDomain',
        name: 'dataConverge.dataDomain',
        icon: 'smile',
        component: './dataConverge/DataDomain',
      }
    ]
  },
  {
    path: '/dataScheduler',
    name: 'dataScheduler',
    icon: 'dataScheduler',
    routes: [
      {
        path: '/dataScheduler/workflowDefinition',
        name: 'dataScheduler.workflowDefinition',
        icon: 'smile',
        component: './dataScheduler/Workflow/WorkflowDefinition',
      },
      {
        path: '/dataScheduler/dag',
        name: 'dataScheduler.dag',
        icon: 'smile',
        component: './dataScheduler/Dag/WorkflowDefinition',
      }
    ]
  },
  {
    name: 'list.table-list',
    icon: 'table',
    path: '/list',
    component: './TableList',
  },
  {
    path: '/',
    redirect: '/welcome',
  },
  {
    component: './404',
  },
];
