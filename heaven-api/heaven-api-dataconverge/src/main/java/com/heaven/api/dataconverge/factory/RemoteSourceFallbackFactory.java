package com.heaven.api.dataconverge.factory;

import com.heaven.api.dataconverge.RemoteSourceService;
import com.heaven.api.dataconverge.domain.DataConvergeSource;
import com.heaven.common.core.domain.R;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户服务降级处理
 * 
 * @author Vicene
 */
@Component
public class RemoteSourceFallbackFactory implements FallbackFactory<RemoteSourceService>
{
    private static final Logger log = LoggerFactory.getLogger(RemoteSourceFallbackFactory.class);

    @Override
    public RemoteSourceService create(Throwable throwable)
    {
        log.error("用户服务调用失败:{}", throwable.getMessage());
        return new RemoteSourceService()
        {
            @Override
            public R<List<DataConvergeSource>> getDataSourceInstance(String dbType, String source)
            {
                return R.fail("获取数据源实例失败:" + throwable.getMessage());
            }
        };
    }
}
