package com.heaven.api.dataconverge;
import com.heaven.common.core.constant.SecurityConstants;
import com.heaven.common.core.constant.ServiceNameConstants;
import com.heaven.common.core.domain.R;
import com.heaven.api.dataconverge.domain.DataConvergeSource;
import com.heaven.api.dataconverge.factory.RemoteSourceFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(contextId = "remoteSourceService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteSourceFallbackFactory.class)
public interface RemoteSourceService {

    /**
     * 获取数据源类型实例
     * @param dbType
     * @return
     */
    @GetMapping("/source/getDataSourceInstance/{dbType}")
    public R<List<DataConvergeSource>> getDataSourceInstance(@PathVariable("dbType") String dbType, @RequestHeader(SecurityConstants.FROM_SOURCE) String source);
}
