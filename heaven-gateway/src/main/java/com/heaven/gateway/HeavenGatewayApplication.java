package com.heaven.gateway;

import com.heaven.common.core.constant.Constants;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 网关启动程序
 * 
 * @author Vicene
 */
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class }
        ,scanBasePackages = Constants.FEIGN_SCAN_BASE_PACKAGE_NAME
)
public class HeavenGatewayApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(HeavenGatewayApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  苍穹-网关启动成功   ლ(´ڡ`ლ)ﾞ ");
    }
}
