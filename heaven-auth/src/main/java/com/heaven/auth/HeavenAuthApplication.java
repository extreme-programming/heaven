package com.heaven.auth;

import com.heaven.common.core.constant.Constants;
import com.heaven.common.security.annotation.EnableRyFeignClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import com.heaven.common.core.constant.Constants;

/**
 * 认证授权中心
 * 
 * @author Vicene
 */
@EnableRyFeignClients
@SpringBootApplication(
        exclude = {DataSourceAutoConfiguration.class }
        ,scanBasePackages = Constants.FEIGN_SCAN_BASE_PACKAGE_NAME
)
public class HeavenAuthApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(HeavenAuthApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  苍穹-鉴权模块启动成功   ლ(´ڡ`ლ)ﾞ ");
    }
}
