package com.heaven.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.heaven.system.api.domain.SysDept;
import com.heaven.system.domain.SysMenu;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

public class AntDesignProTreeSelect implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 节点ID */
    private Long id;

    /** 节点key */
    private Long key;

    /** 节点名称 */
    private String name;

    /** 节点title */
    private String title;

    /** 节点value */
    private Long value;

    /** 子节点 */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<AntDesignProTreeSelect> children;

    public AntDesignProTreeSelect()
    {

    }

    public AntDesignProTreeSelect(SysDept dept)
    {
        this.id = dept.getDeptId();
        this.name = dept.getDeptName();
        this.title = dept.getDeptName();
        this.key = dept.getDeptId();
        this.children = dept.getChildren().stream().map(AntDesignProTreeSelect::new).collect(Collectors.toList());
    }

    public AntDesignProTreeSelect(SysMenu menu)
    {
        this.id = menu.getMenuId();
        this.name = menu.getMenuName();
        this.title = menu.getMenuName();
        this.key = menu.getMenuId();
        this.value = menu.getMenuId();
        this.children = menu.getChildren().stream().map(AntDesignProTreeSelect::new).collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getKey() {
        return key;
    }

    public void setKey(Long key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<AntDesignProTreeSelect> getChildren() {
        return children;
    }

    public void setChildren(List<AntDesignProTreeSelect> children) {
        this.children = children;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }
}
