package com.heaven.system;

import com.heaven.common.security.annotation.EnableCustomConfig;
import com.heaven.common.security.annotation.EnableRyFeignClients;
import com.heaven.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 系统模块
 * 
 * @author Vicene
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class HeavenSystemApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(HeavenSystemApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  苍穹-系统模块启动成功   ლ(´ڡ`ლ)ﾞ ");
    }
}
