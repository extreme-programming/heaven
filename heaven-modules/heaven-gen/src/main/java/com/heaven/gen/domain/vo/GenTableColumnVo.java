package com.heaven.gen.domain.vo;

import com.heaven.gen.domain.GenTableColumn;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenTableColumnVo {
//    private Long tableId;
    private List<GenTableColumn> list;
}
