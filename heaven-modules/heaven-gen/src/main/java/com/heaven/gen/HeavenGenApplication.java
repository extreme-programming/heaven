package com.heaven.gen;

import com.heaven.common.security.annotation.EnableCustomConfig;
import com.heaven.common.security.annotation.EnableRyFeignClients;
import com.heaven.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class HeavenGenApplication
{
	public static void main(String[] args)
	{
		SpringApplication.run(HeavenGenApplication.class, args);
		System.out.println("(♥◠‿◠)ﾉﾞ  苍穹-代码生成模块启动成功   ლ(´ڡ`ლ)ﾞ ");
	}
}
