<p align="center">
	 **Heaven(苍穹)是一个数据中台** 
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">Heaven v1.0.0</h1>

## 技术栈
1. 后端-JAVA:Spring Cloud & Alibaba微服务架构，nacos，sentinel，seata，redis等
2. 前端-React:Ant Design Pro等
3. 大数据存储:Hadoop、Hive、Spark、Flink、ClickHouse等
4. 数据挖掘-python:Pandas Numpy tensorflow Keras Scrapy等

## 平台简介
Heaven(苍穹)是一个数据中台，旨在解决企业在发展的过程中,由于数据的激增与业务的扩大而出现的统计口径不一致、重复开发、指标开发需求响应慢、数据质量低、数据成本高等问题。 通过开发一系列数据工具(元数据中心、数据指标中心、数仓模型中心、数据资产中心、数据服务中心),规范数据供应链的各个环节,以一种标准的、安全的、统一的、共享的、服务化的方式支撑前端的数据应用。业务内包含数据汇聚、数据调度、数据质量、数据标签、数据资产、数据指标、数据血缘、数据建模等功能

## 代码模块
web端
~~~
敬请期待
~~~

大数据存储
~~~
敬请期待
~~~

数据挖掘
~~~
敬请期待
~~~

## 架构图

#### 业务架构
~~~
敬请期待
~~~
#### 应用架构
~~~
敬请期待
~~~
#### 产品架构
~~~
敬请期待
~~~
#### 数据架构
~~~
敬请期待
~~~
#### 技术架构
<table>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-6ff3ad251f3e188081a5bce03e6c6047fea.png"/></td>
    </tr>
</table>

## 内置功能

1. 系统管理
2. 数据汇聚
3. 数据调度
4. 数据血缘
5. 数据质量
6. 数据标签
7. 数据指标
8. 数据建模

## 在线体验
~~~
敬请期待
~~~

文档地址：
~~~
敬请期待
~~~

## 演示图
### 中间件
#### Nacos
<table>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-4eca91a643c904d1d591b4d992986f3c8fb.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-4b6606084e7909cb1c324d8f2eb2c58913d.png"/></td>
    </tr>
</table>

#### SpringAdmin
<table>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-96631ec98efac61793c7dcda6a5f78de6ee.jpg"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-176016707c0728d871713239865c2e615fd.png"/></td>
    </tr>
</table>

#### Skywalking
<table>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-5810a76affdf17bc960342982728cb230a6.jpg"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-a677641ec8b953b58c35a394a8ae9913039.jpg"/></td>
    </tr>
</table>

### 平台
#### 系统管理
<table>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-54600cf049ad2c8a011c524b080cea10f7c.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-c8c4bb01ade6444488edcadfc6ab4b3dea2.png"/></td>
    </tr>   
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-39f7116725dfa9c3c4ac20f9ffb2e9e29aa.jpg"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-9c1aa054ae6a046fc3a9115079e0b308798.jpg"/></td>
    </tr> 
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-19b939fcd61ff4e8a37903f22123b5f6987.jpg"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-35e1659ec32d11809f6fcccb551190c9732.jpg"/></td>
    </tr>
</table>

#### 系统工具
<table>
    <tr>
        <td colspan="2"><img src="https://oscimg.oschina.net/oscnet/up-a4427b118e30dd359670923a371d87887c9.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-e5d344e334c5fad3d3a58b7d2b05ee33669.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-610c2fde3d81506b45fbd877e0a4986df65.png"/></td>
    </tr>
</table>

#### 日志管理
<table>
    <tr>
        <td colspan="2"><img src="https://oscimg.oschina.net/oscnet/up-618b0386820196c78ea580c985b01b51991.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-c6cd0277e490c9093732a52ff7d58f5c004.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-aa9dc6059bb92a3dc67652fda346690d003.png"/></td>
    </tr>
</table>

#### 数据汇聚
<table>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-aae6df36d1a48f8e830ab017bebb04f247a.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-0a0e4786d63de600997e2841a9308b0abc2.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-934d540da318129081d137259edaa543171.png"/></td>
    </tr>
</table>

#### 数据调度(考虑直接使用dolphinscheduler)
<table>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-3a7de10bb5350f33739125dd3b519882468.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-7d372af0bb57f13ae0909918205b8a62291.png"/></td>
    </tr>
</table>

#### 数据建模
~~~
敬请期待
~~~

#### 数据血缘
~~~
敬请期待
~~~

#### 数据质量
~~~
敬请期待
~~~

#### 数据标签
~~~
敬请期待
~~~

## (苍穹)交流群

QQ群：敬请期待