package com.heaven.dataschedule.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class ProcessDefinitionAdddInVo {
    private Integer executionType;
    private String remark;
    private String name;
    private String globalParams;
    private String timeout;
    private ProcessDefinitionGraphJsonInVo graphData;
    private TaskDefinitionJsonInVo[] nodeData;
}
