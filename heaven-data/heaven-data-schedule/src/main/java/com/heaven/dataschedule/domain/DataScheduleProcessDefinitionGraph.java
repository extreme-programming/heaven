package com.heaven.dataschedule.domain;

import lombok.Builder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.heaven.common.core.annotation.Excel;
import com.heaven.common.core.web.domain.BaseEntity;

/**
 * 数据调度-流程定义流程图对象 data_schedule_process_definition_graph
 * 
 * @author Vicene
 * @date 2022-12-07
 */
@Builder
public class DataScheduleProcessDefinitionGraph extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long processDefinitionId;

    /** 流程图json */
    @Excel(name = "流程图json")
    private String json;

    public void setProcessDefinitionId(Long processDefinitionId) 
    {
        this.processDefinitionId = processDefinitionId;
    }

    public Long getProcessDefinitionId() 
    {
        return processDefinitionId;
    }
    public void setJson(String json) 
    {
        this.json = json;
    }

    public String getJson() 
    {
        return json;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("processDefinitionId", getProcessDefinitionId())
            .append("json", getJson())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
