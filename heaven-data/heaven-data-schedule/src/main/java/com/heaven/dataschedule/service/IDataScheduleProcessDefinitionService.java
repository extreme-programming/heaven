package com.heaven.dataschedule.service;

import java.util.List;
import com.heaven.dataschedule.domain.DataScheduleProcessDefinition;
import com.heaven.dataschedule.vo.ProcessDefinitionAdddInVo;

/**
 * 数据调度-流程定义Service接口
 * 
 * @author Vicene
 * @date 2022-12-02
 */
public interface IDataScheduleProcessDefinitionService 
{
    /**
     * 查询数据调度-流程定义
     * 
     * @param id 数据调度-流程定义主键
     * @return 数据调度-流程定义
     */
    public DataScheduleProcessDefinition selectDataScheduleProcessDefinitionById(Long id);

    /**
     * 查询数据调度-流程定义列表
     * 
     * @param dataScheduleProcessDefinition 数据调度-流程定义
     * @return 数据调度-流程定义集合
     */
    public List<DataScheduleProcessDefinition> selectDataScheduleProcessDefinitionList(DataScheduleProcessDefinition dataScheduleProcessDefinition);

    /**
     * 新增数据调度-流程定义
     * 
     * @param dataScheduleProcessDefinition 数据调度-流程定义
     * @return 结果
     */
    public int insertDataScheduleProcessDefinition(DataScheduleProcessDefinition dataScheduleProcessDefinition);

    public int insertDataScheduleProcessDefinition(
            ProcessDefinitionAdddInVo processDefinitionAdddInVo
    );


    /**
     * 修改数据调度-流程定义
     * 
     * @param dataScheduleProcessDefinition 数据调度-流程定义
     * @return 结果
     */
    public int updateDataScheduleProcessDefinition(DataScheduleProcessDefinition dataScheduleProcessDefinition);

    /**
     * 批量删除数据调度-流程定义
     * 
     * @param ids 需要删除的数据调度-流程定义主键集合
     * @return 结果
     */
    public int deleteDataScheduleProcessDefinitionByIds(Long[] ids);

    /**
     * 删除数据调度-流程定义信息
     * 
     * @param id 数据调度-流程定义主键
     * @return 结果
     */
    public int deleteDataScheduleProcessDefinitionById(Long id);
}
