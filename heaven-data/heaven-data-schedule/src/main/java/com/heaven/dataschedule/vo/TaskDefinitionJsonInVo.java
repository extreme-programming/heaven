package com.heaven.dataschedule.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TaskDefinitionJsonInVo {
    private String id;
    private String code;
    private String name;
    private String version;
    private String taskType;
    private String taskParams;
    private Integer flag;
    private Integer taskPriority;
    private Integer failRetryTimes;
    private Integer failRetryInterval;
    private Boolean timeoutFlag;
    private String[] timeoutNotifyStrategy;
    private Integer timeout;
    private Integer delayTime;
    private String remark;

//    private String sql;
//    private String type;
//    private String datasource;
//    private String localParams;

}
