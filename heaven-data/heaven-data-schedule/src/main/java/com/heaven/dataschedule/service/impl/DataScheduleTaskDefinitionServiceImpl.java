package com.heaven.dataschedule.service.impl;

import java.util.List;
import com.heaven.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.heaven.dataschedule.mapper.DataScheduleTaskDefinitionMapper;
import com.heaven.dataschedule.domain.DataScheduleTaskDefinition;
import com.heaven.dataschedule.service.IDataScheduleTaskDefinitionService;

/**
 * taskDefinitionService业务层处理
 * 
 * @author Vicene
 * @date 2022-12-08
 */
@Service
public class DataScheduleTaskDefinitionServiceImpl implements IDataScheduleTaskDefinitionService 
{
    @Autowired
    private DataScheduleTaskDefinitionMapper dataScheduleTaskDefinitionMapper;

    /**
     * 查询taskDefinition
     * 
     * @param id taskDefinition主键
     * @return taskDefinition
     */
    @Override
    public DataScheduleTaskDefinition selectDataScheduleTaskDefinitionById(Long id)
    {
        return dataScheduleTaskDefinitionMapper.selectDataScheduleTaskDefinitionById(id);
    }

    /**
     * 查询taskDefinition列表
     * 
     * @param dataScheduleTaskDefinition taskDefinition
     * @return taskDefinition
     */
    @Override
    public List<DataScheduleTaskDefinition> selectDataScheduleTaskDefinitionList(DataScheduleTaskDefinition dataScheduleTaskDefinition)
    {
        return dataScheduleTaskDefinitionMapper.selectDataScheduleTaskDefinitionList(dataScheduleTaskDefinition);
    }

    /**
     * 新增taskDefinition
     * 
     * @param dataScheduleTaskDefinition taskDefinition
     * @return 结果
     */
    @Override
    public int insertDataScheduleTaskDefinition(DataScheduleTaskDefinition dataScheduleTaskDefinition)
    {
        dataScheduleTaskDefinition.setCreateTime(DateUtils.getNowDate());
        return dataScheduleTaskDefinitionMapper.insertDataScheduleTaskDefinition(dataScheduleTaskDefinition);
    }

    /**
     * 修改taskDefinition
     * 
     * @param dataScheduleTaskDefinition taskDefinition
     * @return 结果
     */
    @Override
    public int updateDataScheduleTaskDefinition(DataScheduleTaskDefinition dataScheduleTaskDefinition)
    {
        dataScheduleTaskDefinition.setUpdateTime(DateUtils.getNowDate());
        return dataScheduleTaskDefinitionMapper.updateDataScheduleTaskDefinition(dataScheduleTaskDefinition);
    }

    /**
     * 批量删除taskDefinition
     * 
     * @param ids 需要删除的taskDefinition主键
     * @return 结果
     */
    @Override
    public int deleteDataScheduleTaskDefinitionByIds(Long[] ids)
    {
        return dataScheduleTaskDefinitionMapper.deleteDataScheduleTaskDefinitionByIds(ids);
    }

    /**
     * 删除taskDefinition信息
     * 
     * @param id taskDefinition主键
     * @return 结果
     */
    @Override
    public int deleteDataScheduleTaskDefinitionById(Long id)
    {
        return dataScheduleTaskDefinitionMapper.deleteDataScheduleTaskDefinitionById(id);
    }
}
