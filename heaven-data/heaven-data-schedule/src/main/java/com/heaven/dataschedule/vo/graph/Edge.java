package com.heaven.dataschedule.vo.graph;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Edge{
    private Object attrs;
    private String id;
    private Object data;
    private Object source;
    private String sourcePort;
    private String sourcePortId;
    private Object target;
    private String targetPort;
    private String targetPortId;
    private String zIndex;
    private Object[] vertices;
}
