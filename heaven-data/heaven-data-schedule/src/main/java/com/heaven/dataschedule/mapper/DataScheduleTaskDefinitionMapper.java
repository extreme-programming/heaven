package com.heaven.dataschedule.mapper;

import java.util.List;
import com.heaven.dataschedule.domain.DataScheduleTaskDefinition;

/**
 * taskDefinitionMapper接口
 * 
 * @author Vicene
 * @date 2022-12-08
 */
public interface DataScheduleTaskDefinitionMapper 
{
    /**
     * 查询taskDefinition
     * 
     * @param id taskDefinition主键
     * @return taskDefinition
     */
    public DataScheduleTaskDefinition selectDataScheduleTaskDefinitionById(Long id);

    /**
     * 查询taskDefinition列表
     * 
     * @param dataScheduleTaskDefinition taskDefinition
     * @return taskDefinition集合
     */
    public List<DataScheduleTaskDefinition> selectDataScheduleTaskDefinitionList(DataScheduleTaskDefinition dataScheduleTaskDefinition);

    /**
     * 新增taskDefinition
     * 
     * @param dataScheduleTaskDefinition taskDefinition
     * @return 结果
     */
    public int insertDataScheduleTaskDefinition(DataScheduleTaskDefinition dataScheduleTaskDefinition);

    /**
     * 修改taskDefinition
     * 
     * @param dataScheduleTaskDefinition taskDefinition
     * @return 结果
     */
    public int updateDataScheduleTaskDefinition(DataScheduleTaskDefinition dataScheduleTaskDefinition);

    /**
     * 删除taskDefinition
     * 
     * @param id taskDefinition主键
     * @return 结果
     */
    public int deleteDataScheduleTaskDefinitionById(Long id);

    /**
     * 批量删除taskDefinition
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDataScheduleTaskDefinitionByIds(Long[] ids);
}
