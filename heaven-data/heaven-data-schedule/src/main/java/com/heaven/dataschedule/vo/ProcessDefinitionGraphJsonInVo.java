package com.heaven.dataschedule.vo;

import com.heaven.dataschedule.vo.graph.Edge;
import com.heaven.dataschedule.vo.graph.Node;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProcessDefinitionGraphJsonInVo {
    private Edge[] edges;
    private Node[] nodes;
}
