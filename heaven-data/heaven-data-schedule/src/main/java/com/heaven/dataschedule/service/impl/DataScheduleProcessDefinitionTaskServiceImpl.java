package com.heaven.dataschedule.service.impl;

import java.util.List;
import com.heaven.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.heaven.dataschedule.mapper.DataScheduleProcessDefinitionTaskMapper;
import com.heaven.dataschedule.domain.DataScheduleProcessDefinitionTask;
import com.heaven.dataschedule.service.IDataScheduleProcessDefinitionTaskService;

/**
 * processDefinitionTaskService业务层处理
 * 
 * @author Vicene
 * @date 2022-12-07
 */
@Service
public class DataScheduleProcessDefinitionTaskServiceImpl implements IDataScheduleProcessDefinitionTaskService 
{
    @Autowired
    private DataScheduleProcessDefinitionTaskMapper dataScheduleProcessDefinitionTaskMapper;

    /**
     * 查询processDefinitionTask
     * 
     * @param id processDefinitionTask主键
     * @return processDefinitionTask
     */
    @Override
    public DataScheduleProcessDefinitionTask selectDataScheduleProcessDefinitionTaskById(Long id)
    {
        return dataScheduleProcessDefinitionTaskMapper.selectDataScheduleProcessDefinitionTaskById(id);
    }

    /**
     * 查询processDefinitionTask列表
     * 
     * @param dataScheduleProcessDefinitionTask processDefinitionTask
     * @return processDefinitionTask
     */
    @Override
    public List<DataScheduleProcessDefinitionTask> selectDataScheduleProcessDefinitionTaskList(DataScheduleProcessDefinitionTask dataScheduleProcessDefinitionTask)
    {
        return dataScheduleProcessDefinitionTaskMapper.selectDataScheduleProcessDefinitionTaskList(dataScheduleProcessDefinitionTask);
    }

    /**
     * 新增processDefinitionTask
     * 
     * @param dataScheduleProcessDefinitionTask processDefinitionTask
     * @return 结果
     */
    @Override
    public int insertDataScheduleProcessDefinitionTask(DataScheduleProcessDefinitionTask dataScheduleProcessDefinitionTask)
    {
        dataScheduleProcessDefinitionTask.setCreateTime(DateUtils.getNowDate());
        return dataScheduleProcessDefinitionTaskMapper.insertDataScheduleProcessDefinitionTask(dataScheduleProcessDefinitionTask);
    }

    /**
     * 修改processDefinitionTask
     * 
     * @param dataScheduleProcessDefinitionTask processDefinitionTask
     * @return 结果
     */
    @Override
    public int updateDataScheduleProcessDefinitionTask(DataScheduleProcessDefinitionTask dataScheduleProcessDefinitionTask)
    {
        dataScheduleProcessDefinitionTask.setUpdateTime(DateUtils.getNowDate());
        return dataScheduleProcessDefinitionTaskMapper.updateDataScheduleProcessDefinitionTask(dataScheduleProcessDefinitionTask);
    }

    /**
     * 批量删除processDefinitionTask
     * 
     * @param ids 需要删除的processDefinitionTask主键
     * @return 结果
     */
    @Override
    public int deleteDataScheduleProcessDefinitionTaskByIds(Long[] ids)
    {
        return dataScheduleProcessDefinitionTaskMapper.deleteDataScheduleProcessDefinitionTaskByIds(ids);
    }

    /**
     * 删除processDefinitionTask信息
     * 
     * @param id processDefinitionTask主键
     * @return 结果
     */
    @Override
    public int deleteDataScheduleProcessDefinitionTaskById(Long id)
    {
        return dataScheduleProcessDefinitionTaskMapper.deleteDataScheduleProcessDefinitionTaskById(id);
    }

    @Override
    public int insertDataScheduleProcessDefinitionTasks(List<DataScheduleProcessDefinitionTask> dataScheduleProcessDefinitionTasks) {
        for(DataScheduleProcessDefinitionTask task : dataScheduleProcessDefinitionTasks){
            this.insertDataScheduleProcessDefinitionTask(task);
        }
        return dataScheduleProcessDefinitionTasks.size();
    }
}
