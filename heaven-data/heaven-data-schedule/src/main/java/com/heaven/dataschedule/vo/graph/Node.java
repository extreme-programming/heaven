package com.heaven.dataschedule.vo.graph;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Node{
    private String id;
    private String alignItems;
    private Object attrs;
    private String backgroundImage;
    private String backgroundPosition;
    private String backgroundRepeat;
    private String backgroundSize;
    private String backgroundColor;
    private String borderRadius;
    private String borderWidth;
    private String boxSizing;
    private String cursor;
    private String fontSize;
    private String height;
    private Object[] incomingEdges;
    private String label;
    private String marginBottom;
    private Object[] outgoingEdges;
    private String padding;
    private Object ports;
    private String renderKey;
    private String transform;
    private String width;
    private String x;
    private String y;
}
