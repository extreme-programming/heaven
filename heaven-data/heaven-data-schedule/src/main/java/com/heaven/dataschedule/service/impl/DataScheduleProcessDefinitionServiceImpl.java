package com.heaven.dataschedule.service.impl;

import java.util.*;

import cn.hutool.json.JSONUtil;
import com.heaven.common.core.utils.DateUtils;
import com.heaven.common.core.utils.StringUtils;
import com.heaven.common.security.utils.SecurityUtils;
import com.heaven.dataschedule.constant.Constants;
import com.heaven.dataschedule.domain.DataScheduleProcessDefinitionGraph;
import com.heaven.dataschedule.domain.DataScheduleProcessDefinitionTask;
import com.heaven.dataschedule.domain.DataScheduleTaskDefinition;
import com.heaven.dataschedule.enums.ReleaseStateEnum;
import com.heaven.dataschedule.service.IDataScheduleProcessDefinitionGraphService;
import com.heaven.dataschedule.service.IDataScheduleProcessDefinitionTaskService;
import com.heaven.dataschedule.service.IDataScheduleTaskDefinitionService;
import com.heaven.dataschedule.vo.ProcessDefinitionAdddInVo;
import com.heaven.dataschedule.vo.ProcessDefinitionGraphJsonInVo;
import com.heaven.dataschedule.vo.TaskDefinitionJsonInVo;
import com.heaven.dataschedule.vo.graph.Edge;
import com.heaven.dataschedule.vo.graph.Node;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.heaven.dataschedule.mapper.DataScheduleProcessDefinitionMapper;
import com.heaven.dataschedule.domain.DataScheduleProcessDefinition;
import com.heaven.dataschedule.service.IDataScheduleProcessDefinitionService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 数据调度-流程定义Service业务层处理
 * 
 * @author Vicene
 * @date 2022-12-02
 */
@Service
public class DataScheduleProcessDefinitionServiceImpl implements IDataScheduleProcessDefinitionService 
{
    @Autowired
    private DataScheduleProcessDefinitionMapper dataScheduleProcessDefinitionMapper;

    @Autowired
    private IDataScheduleProcessDefinitionGraphService dataScheduleProcessDefinitionGraphService;

    @Autowired
    private IDataScheduleProcessDefinitionTaskService dataScheduleProcessDefinitionTaskService;

    @Autowired
    private IDataScheduleTaskDefinitionService dataScheduleTaskDefinitionService;


    /**
     * 查询数据调度-流程定义
     * 
     * @param id 数据调度-流程定义主键
     * @return 数据调度-流程定义
     */
    @Override
    public DataScheduleProcessDefinition selectDataScheduleProcessDefinitionById(Long id)
    {
        DataScheduleProcessDefinition processDefinition = dataScheduleProcessDefinitionMapper.selectDataScheduleProcessDefinitionById(id);
        DataScheduleProcessDefinitionGraph graph = dataScheduleProcessDefinitionGraphService.selectDataScheduleProcessDefinitionGraphByProcessDefinitionId(id);
        processDefinition.setGraph(graph.getJson());
        return processDefinition;
    }

    /**
     * 查询数据调度-流程定义列表
     * 
     * @param dataScheduleProcessDefinition 数据调度-流程定义
     * @return 数据调度-流程定义
     */
    @Override
    public List<DataScheduleProcessDefinition> selectDataScheduleProcessDefinitionList(DataScheduleProcessDefinition dataScheduleProcessDefinition)
    {
        return dataScheduleProcessDefinitionMapper.selectDataScheduleProcessDefinitionList(dataScheduleProcessDefinition);
    }

    /**
     * 新增数据调度-流程定义
     * 
     * @param dataScheduleProcessDefinition 数据调度-流程定义
     * @return 结果
     */
    @Override
    public int insertDataScheduleProcessDefinition(DataScheduleProcessDefinition dataScheduleProcessDefinition)
    {
        dataScheduleProcessDefinition.setCreateBy(SecurityUtils.getUsername());
        dataScheduleProcessDefinition.setCreateTime(DateUtils.getNowDate());
        return dataScheduleProcessDefinitionMapper.insertDataScheduleProcessDefinition(dataScheduleProcessDefinition);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public int insertDataScheduleProcessDefinition(ProcessDefinitionAdddInVo processDefinitionAdddInVo)
    {
        ProcessDefinitionGraphJsonInVo graphJsonInVo = processDefinitionAdddInVo.getGraphData();
        TaskDefinitionJsonInVo[] taskDefinitionJsonInVos = processDefinitionAdddInVo.getNodeData();

        // 1:保存流程定义数据
        DataScheduleProcessDefinition processDefinition = new DataScheduleProcessDefinition();
        processDefinition.setName(processDefinitionAdddInVo.getName());
        processDefinition.setExecutionType(processDefinitionAdddInVo.getExecutionType());
        processDefinition.setReleaseState(ReleaseStateEnum.OFFLINE.getCode());
        if(StringUtils.isNotEmpty(processDefinitionAdddInVo.getGlobalParams())){
            processDefinition.setGlobalParams(processDefinitionAdddInVo.getGlobalParams());
        }
        if(StringUtils.isNotEmpty(processDefinitionAdddInVo.getTimeout())){
            processDefinition.setTimeout(Long.valueOf(processDefinitionAdddInVo.getTimeout()));
        }

        this.insertDataScheduleProcessDefinition(processDefinition);

        // 2:保存图json数据
        dataScheduleProcessDefinitionGraphService.
                insertDataScheduleProcessDefinitionGraph(
                        DataScheduleProcessDefinitionGraph.builder()
                                .json(JSONUtil.toJsonStr(graphJsonInVo))
                                .processDefinitionId(processDefinition.getId())
                                    .build());

        // 3:保存任务定义json数据
        for (TaskDefinitionJsonInVo taskDefinitionJsonInVo: taskDefinitionJsonInVos) {
            DataScheduleTaskDefinition definitionTask = new DataScheduleTaskDefinition(taskDefinitionJsonInVo);
            dataScheduleTaskDefinitionService.insertDataScheduleTaskDefinition(definitionTask);
        }

        // 4:保存流程定义图数据
        List<DataScheduleProcessDefinitionTask> processDefinitionTasks = parseProcessTaskRelation(processDefinition.getId() , graphJsonInVo , taskDefinitionJsonInVos);
        dataScheduleProcessDefinitionTaskService.insertDataScheduleProcessDefinitionTasks(processDefinitionTasks);
        return 1;
    }

    private List<DataScheduleProcessDefinitionTask> parseProcessTaskRelation(Long processDefinitionId ,ProcessDefinitionGraphJsonInVo graphJsonInVo,TaskDefinitionJsonInVo[] taskDefinitionJsonInVos) {
        Edge [] edges = graphJsonInVo.getEdges();
        List<DataScheduleProcessDefinitionTask> dataScheduleProcessDefinitionTasks = new LinkedList<>();

        Node[] nodes = graphJsonInVo.getNodes();
        Map<String , String> targetIdsMap = new HashMap<>();
        for(Edge edge: edges){
            String ids = edge.getId();
            String sourceNodeId = ids.substring(0 , Constants.EDGE_SPLIT_LENGTH);
            String targetNodeId = ids.substring(Constants.EDGE_SPLIT_LENGTH * 2 + 2, ids.length() - Constants.EDGE_SPLIT_LENGTH - 1);
            targetIdsMap.put(targetNodeId , null);
            DataScheduleProcessDefinitionTask processDefinitionTask = new DataScheduleProcessDefinitionTask(processDefinitionId,sourceNodeId,targetNodeId );
            dataScheduleProcessDefinitionTasks.add(processDefinitionTask);
        }

        for(Node node : nodes){
            if(!targetIdsMap.containsKey(node.getId())){
                DataScheduleProcessDefinitionTask processDefinitionTask = new DataScheduleProcessDefinitionTask(processDefinitionId,"0",node.getId() );
                dataScheduleProcessDefinitionTasks.add(processDefinitionTask);
            }
        }
        return dataScheduleProcessDefinitionTasks;
    }

    /**
     * 修改数据调度-流程定义
     * 
     * @param dataScheduleProcessDefinition 数据调度-流程定义
     * @return 结果
     */
    @Override
    public int updateDataScheduleProcessDefinition(DataScheduleProcessDefinition dataScheduleProcessDefinition)
    {
        dataScheduleProcessDefinition.setUpdateTime(DateUtils.getNowDate());
        return dataScheduleProcessDefinitionMapper.updateDataScheduleProcessDefinition(dataScheduleProcessDefinition);
    }

    /**
     * 批量删除数据调度-流程定义
     * 
     * @param ids 需要删除的数据调度-流程定义主键
     * @return 结果
     */
    @Override
    public int deleteDataScheduleProcessDefinitionByIds(Long[] ids)
    {
        return dataScheduleProcessDefinitionMapper.deleteDataScheduleProcessDefinitionByIds(ids);
    }

    /**
     * 删除数据调度-流程定义信息
     * 
     * @param id 数据调度-流程定义主键
     * @return 结果
     */
    @Override
    public int deleteDataScheduleProcessDefinitionById(Long id)
    {
        return dataScheduleProcessDefinitionMapper.deleteDataScheduleProcessDefinitionById(id);
    }
}
