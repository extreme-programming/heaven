package com.heaven.dataschedule.domain;

import com.heaven.common.security.utils.SecurityUtils;
import com.heaven.dataschedule.vo.ProcessDefinitionAdddInVo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.heaven.common.core.annotation.Excel;
import com.heaven.common.core.web.domain.BaseEntity;

import java.util.Date;

/**
 * processDefinitionTask对象 data_schedule_process_definition_task
 * 
 * @author Vicene
 * @date 2022-12-07
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DataScheduleProcessDefinitionTask extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 关联名称 */
    @Excel(name = "关联名称")
    private String name;

    /** 流程定义id */
    @Excel(name = "流程定义id")
    private Long processDefinitionId;

    /** 流程定义版本 */
    @Excel(name = "流程定义版本")
    private Long processDefinitionVersion;

    /** 前置任务code */
    @Excel(name = "前置任务code")
    private String preTaskId;

    /** 前置任务version */
    @Excel(name = "前置任务version")
    private Long preTaskVersion;

    /** 执行任务code */
    @Excel(name = "执行任务code")
    private String postTaskId;

    /** 执行任务version */
    @Excel(name = "执行任务version")
    private Long postTaskVersion;

    /** condition type : 0 none, 1 judge 2 delay */
    @Excel(name = "condition type : 0 none, 1 judge 2 delay")
    private Integer conditionType;

    /** condition params(json) */
    @Excel(name = "condition params(json)")
    private String conditionParams;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setProcessDefinitionId(Long processDefinitionId) 
    {
        this.processDefinitionId = processDefinitionId;
    }

    public Long getProcessDefinitionId() 
    {
        return processDefinitionId;
    }
    public void setProcessDefinitionVersion(Long processDefinitionVersion) 
    {
        this.processDefinitionVersion = processDefinitionVersion;
    }

    public Long getProcessDefinitionVersion() 
    {
        return processDefinitionVersion;
    }
    public void setPreTaskId(String preTaskId) 
    {
        this.preTaskId = preTaskId;
    }

    public String getPreTaskId() 
    {
        return preTaskId;
    }
    public void setPreTaskVersion(Long preTaskVersion) 
    {
        this.preTaskVersion = preTaskVersion;
    }

    public Long getPreTaskVersion() 
    {
        return preTaskVersion;
    }
    public void setPostTaskId(String postTaskId) 
    {
        this.postTaskId = postTaskId;
    }

    public String getPostTaskId() 
    {
        return postTaskId;
    }
    public void setPostTaskVersion(Long postTaskVersion) 
    {
        this.postTaskVersion = postTaskVersion;
    }

    public Long getPostTaskVersion() 
    {
        return postTaskVersion;
    }
    public void setConditionType(Integer conditionType) 
    {
        this.conditionType = conditionType;
    }

    public Integer getConditionType() 
    {
        return conditionType;
    }
    public void setConditionParams(String conditionParams) 
    {
        this.conditionParams = conditionParams;
    }

    public String getConditionParams() 
    {
        return conditionParams;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("processDefinitionId", getProcessDefinitionId())
            .append("processDefinitionVersion", getProcessDefinitionVersion())
            .append("preTaskId", getPreTaskId())
            .append("preTaskVersion", getPreTaskVersion())
            .append("postTaskId", getPostTaskId())
            .append("postTaskVersion", getPostTaskVersion())
            .append("conditionType", getConditionType())
            .append("conditionParams", getConditionParams())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }

    public DataScheduleProcessDefinitionTask(Long processDefinitionId, String sourceNodeId,String targetNodeId){
        this.processDefinitionId = processDefinitionId;
        this.preTaskId = sourceNodeId;
        this.postTaskId = targetNodeId;
        this.setCreateTime(new Date());
        this.setCreateBy(SecurityUtils.getUsername());
    }

}
