package com.heaven.dataschedule.domain;

import com.heaven.dataschedule.vo.ProcessDefinitionAdddInVo;
import com.heaven.dataschedule.vo.TaskDefinitionJsonInVo;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.heaven.common.core.annotation.Excel;
import com.heaven.common.core.web.domain.BaseEntity;

/**
 * taskDefinition对象 data_schedule_task_definition
 * 
 * @author Vicene
 * @date 2022-12-08
 */
public class DataScheduleTaskDefinition extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** code值唯一 */
    @Excel(name = "code值唯一")
    private String code;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 版本 */
    @Excel(name = "版本")
    private Long version;

    /** 任务类型 */
    @Excel(name = "任务类型")
    private String taskType;

    /** job custom parameters */
    @Excel(name = "job custom parameters")
    private String taskParams;

    /** 是否可用 */
    @Excel(name = "是否可用")
    private Integer flag;

    /** 任务优先级 */
    @Excel(name = "任务优先级")
    private Integer taskPriority;

    /** worker grouping */
    @Excel(name = "worker grouping")
    private String workerGroup;

    /** 失败策略 */
    @Excel(name = "失败策略")
    private Long failRetryTimes;

    /** 重试次数 */
    @Excel(name = "重试次数")
    private Long failRetryInterval;

    /** 是否过期 */
    @Excel(name = "是否过期")
    private Integer timeoutFlag;

    /** 失败通知策略: 0 warning, 1 fail */
    @Excel(name = "失败通知策略: 0 warning, 1 fail")
    private Integer timeoutNotifyStrategy;

    /** 超时时间,unit: minute */
    @Excel(name = "超时时间,unit: minute")
    private Long timeout;

    /** 延迟执行时间,unit: minute */
    @Excel(name = "延迟执行时间,unit: minute")
    private Long delayTime;

    /** 资源id */
    @Excel(name = "资源id")
    private String resourceIds;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setVersion(Long version) 
    {
        this.version = version;
    }

    public Long getVersion() 
    {
        return version;
    }
    public void setTaskType(String taskType) 
    {
        this.taskType = taskType;
    }

    public String getTaskType() 
    {
        return taskType;
    }
    public void setTaskParams(String taskParams) 
    {
        this.taskParams = taskParams;
    }

    public String getTaskParams() 
    {
        return taskParams;
    }
    public void setFlag(Integer flag) 
    {
        this.flag = flag;
    }

    public Integer getFlag() 
    {
        return flag;
    }
    public void setTaskPriority(Integer taskPriority) 
    {
        this.taskPriority = taskPriority;
    }

    public Integer getTaskPriority() 
    {
        return taskPriority;
    }
    public void setWorkerGroup(String workerGroup) 
    {
        this.workerGroup = workerGroup;
    }

    public String getWorkerGroup() 
    {
        return workerGroup;
    }
    public void setFailRetryTimes(Long failRetryTimes) 
    {
        this.failRetryTimes = failRetryTimes;
    }

    public Long getFailRetryTimes() 
    {
        return failRetryTimes;
    }
    public void setFailRetryInterval(Long failRetryInterval) 
    {
        this.failRetryInterval = failRetryInterval;
    }

    public Long getFailRetryInterval() 
    {
        return failRetryInterval;
    }
    public void setTimeoutFlag(Integer timeoutFlag) 
    {
        this.timeoutFlag = timeoutFlag;
    }

    public Integer getTimeoutFlag() 
    {
        return timeoutFlag;
    }
    public void setTimeoutNotifyStrategy(Integer timeoutNotifyStrategy) 
    {
        this.timeoutNotifyStrategy = timeoutNotifyStrategy;
    }

    public Integer getTimeoutNotifyStrategy() 
    {
        return timeoutNotifyStrategy;
    }
    public void setTimeout(Long timeout) 
    {
        this.timeout = timeout;
    }

    public Long getTimeout() 
    {
        return timeout;
    }
    public void setDelayTime(Long delayTime) 
    {
        this.delayTime = delayTime;
    }

    public Long getDelayTime() 
    {
        return delayTime;
    }
    public void setResourceIds(String resourceIds) 
    {
        this.resourceIds = resourceIds;
    }

    public String getResourceIds() 
    {
        return resourceIds;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("code", getCode())
            .append("name", getName())
            .append("version", getVersion())
            .append("taskType", getTaskType())
            .append("taskParams", getTaskParams())
            .append("flag", getFlag())
            .append("taskPriority", getTaskPriority())
            .append("workerGroup", getWorkerGroup())
            .append("failRetryTimes", getFailRetryTimes())
            .append("failRetryInterval", getFailRetryInterval())
            .append("timeoutFlag", getTimeoutFlag())
            .append("timeoutNotifyStrategy", getTimeoutNotifyStrategy())
            .append("timeout", getTimeout())
            .append("delayTime", getDelayTime())
            .append("resourceIds", getResourceIds())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }


    public DataScheduleTaskDefinition(TaskDefinitionJsonInVo taskDefinitionJsonInVo){
        this.name = taskDefinitionJsonInVo.getName();
        this.code = taskDefinitionJsonInVo.getId();
        this.delayTime = Long.valueOf(taskDefinitionJsonInVo.getDelayTime()) ;
        this.failRetryInterval = Long.valueOf(taskDefinitionJsonInVo.getFailRetryInterval());
        this.failRetryTimes =  Long.valueOf(taskDefinitionJsonInVo.getFailRetryTimes());
        this.flag = taskDefinitionJsonInVo.getFlag();
        this.taskParams = taskDefinitionJsonInVo.getTaskParams();
        this.taskPriority = taskDefinitionJsonInVo.getTaskPriority();
        this.timeoutFlag = (taskDefinitionJsonInVo.getTimeoutFlag() == null || taskDefinitionJsonInVo.getTimeoutFlag()) ? 0 : 1;
        this.taskType = taskDefinitionJsonInVo.getTaskType();
        if(taskDefinitionJsonInVo.getTimeoutNotifyStrategy().length == 2){
            this.timeoutNotifyStrategy = 2;
        } else if(taskDefinitionJsonInVo.getTimeoutNotifyStrategy().length == 1){
            this.timeoutNotifyStrategy = Integer.valueOf(taskDefinitionJsonInVo.getTimeoutNotifyStrategy()[0]);
        } else {
            this.timeoutNotifyStrategy = null;
        }
    }
}
