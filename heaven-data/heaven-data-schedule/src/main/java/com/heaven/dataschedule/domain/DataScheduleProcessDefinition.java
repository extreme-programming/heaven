package com.heaven.dataschedule.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.heaven.common.core.annotation.Excel;
import com.heaven.common.core.web.domain.BaseEntity;

/**
 * 数据调度-流程定义对象 data_schedule_process_definition
 * 
 * @author Vicene
 * @date 2022-12-02
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DataScheduleProcessDefinition extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 流程定义名称 */
    @Excel(name = "流程定义名称")
    private String name;

    /** 流程定义版本 */
    @Excel(name = "流程定义版本")
    private Long version;

    /** project code */
    @Excel(name = "project code")
    private Long projectCode;

    /** 0:上线,1:下线 */
    @Excel(name = "0:上线,1:下线")
    private Integer releaseState;

    /** 全局参数 */
    @Excel(name = "全局参数")
    private String globalParams;

    /** 是否可用0:可用, 1:不可用 */
    @Excel(name = "是否可用0:可用, 1:不可用")
    private String flag;

    /** Node节点位置 */
    @Excel(name = "Node节点位置")
    private String locations;

    /** alert group id */
    @Excel(name = "alert group id")
    private Long warningGroupId;

    /** 超时时间 单位:分 */
    @Excel(name = "超时时间 单位:分")
    private Long timeout;

    /** 执行类型: 0:并行,1:串行等待,2:串行抛弃,3:串行优先 */
    @Excel(name = "执行类型: 0:并行,1:串行等待,2:串行抛弃,3:串行优先")
    private Integer executionType;

    private String graph;

    public String getGraph() {
        return graph;
    }

    public void setGraph(String graph) {
        this.graph = graph;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setVersion(Long version) 
    {
        this.version = version;
    }

    public Long getVersion() 
    {
        return version;
    }
    public void setProjectCode(Long projectCode) 
    {
        this.projectCode = projectCode;
    }

    public Long getProjectCode() 
    {
        return projectCode;
    }
    public void setReleaseState(Integer releaseState) 
    {
        this.releaseState = releaseState;
    }

    public Integer getReleaseState() 
    {
        return releaseState;
    }
    public void setGlobalParams(String globalParams) 
    {
        this.globalParams = globalParams;
    }

    public String getGlobalParams() 
    {
        return globalParams;
    }
    public void setFlag(String flag) 
    {
        this.flag = flag;
    }

    public String getFlag() 
    {
        return flag;
    }
    public void setLocations(String locations) 
    {
        this.locations = locations;
    }

    public String getLocations() 
    {
        return locations;
    }
    public void setWarningGroupId(Long warningGroupId) 
    {
        this.warningGroupId = warningGroupId;
    }

    public Long getWarningGroupId() 
    {
        return warningGroupId;
    }
    public void setTimeout(Long timeout) 
    {
        this.timeout = timeout;
    }

    public Long getTimeout() 
    {
        return timeout;
    }
    public void setExecutionType(Integer executionType) 
    {
        this.executionType = executionType;
    }

    public Integer getExecutionType() 
    {
        return executionType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("version", getVersion())
            .append("projectCode", getProjectCode())
            .append("releaseState", getReleaseState())
            .append("globalParams", getGlobalParams())
            .append("flag", getFlag())
            .append("locations", getLocations())
            .append("warningGroupId", getWarningGroupId())
            .append("timeout", getTimeout())
            .append("executionType", getExecutionType())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
