package com.heaven.dataschedule.mapper;

import java.util.List;
import com.heaven.dataschedule.domain.DataScheduleProcessDefinitionGraph;

/**
 * 数据调度-流程定义流程图Mapper接口
 * 
 * @author Vicene
 * @date 2022-12-07
 */
public interface DataScheduleProcessDefinitionGraphMapper 
{
    /**
     * 查询数据调度-流程定义流程图
     * 
     * @param processDefinitionId 数据调度-流程定义流程图主键
     * @return 数据调度-流程定义流程图
     */
    public DataScheduleProcessDefinitionGraph selectDataScheduleProcessDefinitionGraphByProcessDefinitionId(Long processDefinitionId);

    /**
     * 查询数据调度-流程定义流程图列表
     * 
     * @param dataScheduleProcessDefinitionGraph 数据调度-流程定义流程图
     * @return 数据调度-流程定义流程图集合
     */
    public List<DataScheduleProcessDefinitionGraph> selectDataScheduleProcessDefinitionGraphList(DataScheduleProcessDefinitionGraph dataScheduleProcessDefinitionGraph);

    /**
     * 新增数据调度-流程定义流程图
     * 
     * @param dataScheduleProcessDefinitionGraph 数据调度-流程定义流程图
     * @return 结果
     */
    public int insertDataScheduleProcessDefinitionGraph(DataScheduleProcessDefinitionGraph dataScheduleProcessDefinitionGraph);

    /**
     * 修改数据调度-流程定义流程图
     * 
     * @param dataScheduleProcessDefinitionGraph 数据调度-流程定义流程图
     * @return 结果
     */
    public int updateDataScheduleProcessDefinitionGraph(DataScheduleProcessDefinitionGraph dataScheduleProcessDefinitionGraph);

    /**
     * 删除数据调度-流程定义流程图
     * 
     * @param processDefinitionId 数据调度-流程定义流程图主键
     * @return 结果
     */
    public int deleteDataScheduleProcessDefinitionGraphByProcessDefinitionId(Long processDefinitionId);

    /**
     * 批量删除数据调度-流程定义流程图
     * 
     * @param processDefinitionIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDataScheduleProcessDefinitionGraphByProcessDefinitionIds(Long[] processDefinitionIds);
}
