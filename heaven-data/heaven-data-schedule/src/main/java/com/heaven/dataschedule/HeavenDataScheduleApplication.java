package com.heaven.dataschedule;

import com.heaven.common.security.annotation.EnableCustomConfig;
import com.heaven.common.security.annotation.EnableRyFeignClients;
import com.heaven.common.swagger.annotation.EnableCustomSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class HeavenDataScheduleApplication {

    public static void main(String[] args) {
        SpringApplication.run(HeavenDataScheduleApplication.class, args);
    }

}
