package com.heaven.dataschedule.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.heaven.dataschedule.vo.ProcessDefinitionAdddInVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.heaven.common.log.annotation.Log;
import com.heaven.common.log.enums.BusinessType;
import com.heaven.common.security.annotation.RequiresPermissions;
import com.heaven.dataschedule.domain.DataScheduleProcessDefinition;
import com.heaven.dataschedule.service.IDataScheduleProcessDefinitionService;
import com.heaven.common.core.web.controller.BaseController;
import com.heaven.common.core.web.domain.AjaxResult;
import com.heaven.common.core.utils.poi.ExcelUtil;
import com.heaven.common.core.web.page.TableDataInfo;

/**
 * 数据调度-流程定义Controller
 * 
 * @author Vicene
 * @date 2022-12-02
 */
@RestController
@RequestMapping("/processDefinition")
public class DataScheduleProcessDefinitionController extends BaseController
{
    @Autowired
    private IDataScheduleProcessDefinitionService dataScheduleProcessDefinitionService;

    /**
     * 查询数据调度-流程定义列表
     */
    @RequiresPermissions("schedule:processDefinition:list")
    @GetMapping("/list")
    public TableDataInfo list(DataScheduleProcessDefinition dataScheduleProcessDefinition)
    {
        startPage();
        List<DataScheduleProcessDefinition> list = dataScheduleProcessDefinitionService.selectDataScheduleProcessDefinitionList(dataScheduleProcessDefinition);
        return getDataTable(list);
    }

    /**
     * 导出数据调度-流程定义列表
     */
    @RequiresPermissions("schedule:processDefinition:export")
    @Log(title = "数据调度-流程定义", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DataScheduleProcessDefinition dataScheduleProcessDefinition)
    {
        List<DataScheduleProcessDefinition> list = dataScheduleProcessDefinitionService.selectDataScheduleProcessDefinitionList(dataScheduleProcessDefinition);
        ExcelUtil<DataScheduleProcessDefinition> util = new ExcelUtil<DataScheduleProcessDefinition>(DataScheduleProcessDefinition.class);
        util.exportExcel(response, list, "数据调度-流程定义数据");
    }

    /**
     * 获取数据调度-流程定义详细信息
     */
    @RequiresPermissions("schedule:processDefinition:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(dataScheduleProcessDefinitionService.selectDataScheduleProcessDefinitionById(id));
    }

    /**
     * 新增数据调度-流程定义
     */
//    @RequiresPermissions("schedule:processDefinition:add")
//    @Log(title = "数据调度-流程定义", businessType = BusinessType.INSERT)
//    @PostMapping
//    public AjaxResult add(@RequestBody DataScheduleProcessDefinition dataScheduleProcessDefinition)
//    {
//        return toAjax(dataScheduleProcessDefinitionService.insertDataScheduleProcessDefinition(dataScheduleProcessDefinition));
//    }

    /**
     * 新增数据调度-流程定义
     */
    @RequiresPermissions("schedule:processDefinition:add")
    @Log(title = "数据调度-流程定义", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add( @RequestBody ProcessDefinitionAdddInVo processDefinitionAdddInVo)
    {
        return toAjax(dataScheduleProcessDefinitionService.insertDataScheduleProcessDefinition(
                processDefinitionAdddInVo)
        );
    }

    /**
     * 修改数据调度-流程定义
     */
    @RequiresPermissions("schedule:processDefinition:edit")
    @Log(title = "数据调度-流程定义", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DataScheduleProcessDefinition dataScheduleProcessDefinition)
    {
        return toAjax(dataScheduleProcessDefinitionService.updateDataScheduleProcessDefinition(dataScheduleProcessDefinition));
    }

    /**
     * 删除数据调度-流程定义
     */
    @RequiresPermissions("schedule:processDefinition:remove")
    @Log(title = "数据调度-流程定义", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dataScheduleProcessDefinitionService.deleteDataScheduleProcessDefinitionByIds(ids));
    }
}
