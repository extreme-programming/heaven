package com.heaven.dataschedule.mapper;

import java.util.List;
import com.heaven.dataschedule.domain.DataScheduleProcessDefinitionTask;

/**
 * processDefinitionTaskMapper接口
 * 
 * @author Vicene
 * @date 2022-12-07
 */
public interface DataScheduleProcessDefinitionTaskMapper 
{
    /**
     * 查询processDefinitionTask
     * 
     * @param id processDefinitionTask主键
     * @return processDefinitionTask
     */
    public DataScheduleProcessDefinitionTask selectDataScheduleProcessDefinitionTaskById(Long id);

    /**
     * 查询processDefinitionTask列表
     * 
     * @param dataScheduleProcessDefinitionTask processDefinitionTask
     * @return processDefinitionTask集合
     */
    public List<DataScheduleProcessDefinitionTask> selectDataScheduleProcessDefinitionTaskList(DataScheduleProcessDefinitionTask dataScheduleProcessDefinitionTask);

    /**
     * 新增processDefinitionTask
     * 
     * @param dataScheduleProcessDefinitionTask processDefinitionTask
     * @return 结果
     */
    public int insertDataScheduleProcessDefinitionTask(DataScheduleProcessDefinitionTask dataScheduleProcessDefinitionTask);

    /**
     * 修改processDefinitionTask
     * 
     * @param dataScheduleProcessDefinitionTask processDefinitionTask
     * @return 结果
     */
    public int updateDataScheduleProcessDefinitionTask(DataScheduleProcessDefinitionTask dataScheduleProcessDefinitionTask);

    /**
     * 删除processDefinitionTask
     * 
     * @param id processDefinitionTask主键
     * @return 结果
     */
    public int deleteDataScheduleProcessDefinitionTaskById(Long id);

    /**
     * 批量删除processDefinitionTask
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDataScheduleProcessDefinitionTaskByIds(Long[] ids);
}
