package com.heaven.dataschedule.service.impl;

import java.util.List;
import com.heaven.common.core.utils.DateUtils;
import com.heaven.common.security.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.heaven.dataschedule.mapper.DataScheduleProcessDefinitionGraphMapper;
import com.heaven.dataschedule.domain.DataScheduleProcessDefinitionGraph;
import com.heaven.dataschedule.service.IDataScheduleProcessDefinitionGraphService;

/**
 * 数据调度-流程定义流程图Service业务层处理
 * 
 * @author Vicene
 * @date 2022-12-07
 */
@Service
public class DataScheduleProcessDefinitionGraphServiceImpl implements IDataScheduleProcessDefinitionGraphService 
{
    @Autowired
    private DataScheduleProcessDefinitionGraphMapper dataScheduleProcessDefinitionGraphMapper;

    /**
     * 查询数据调度-流程定义流程图
     * 
     * @param processDefinitionId 数据调度-流程定义流程图主键
     * @return 数据调度-流程定义流程图
     */
    @Override
    public DataScheduleProcessDefinitionGraph selectDataScheduleProcessDefinitionGraphByProcessDefinitionId(Long processDefinitionId)
    {
        return dataScheduleProcessDefinitionGraphMapper.selectDataScheduleProcessDefinitionGraphByProcessDefinitionId(processDefinitionId);
    }

    /**
     * 查询数据调度-流程定义流程图列表
     * 
     * @param dataScheduleProcessDefinitionGraph 数据调度-流程定义流程图
     * @return 数据调度-流程定义流程图
     */
    @Override
    public List<DataScheduleProcessDefinitionGraph> selectDataScheduleProcessDefinitionGraphList(DataScheduleProcessDefinitionGraph dataScheduleProcessDefinitionGraph)
    {
        return dataScheduleProcessDefinitionGraphMapper.selectDataScheduleProcessDefinitionGraphList(dataScheduleProcessDefinitionGraph);
    }

    /**
     * 新增数据调度-流程定义流程图
     * 
     * @param dataScheduleProcessDefinitionGraph 数据调度-流程定义流程图
     * @return 结果
     */
    @Override
    public int insertDataScheduleProcessDefinitionGraph(DataScheduleProcessDefinitionGraph dataScheduleProcessDefinitionGraph)
    {
        dataScheduleProcessDefinitionGraph.setCreateBy(SecurityUtils.getUsername());
        dataScheduleProcessDefinitionGraph.setCreateTime(DateUtils.getNowDate());
        return dataScheduleProcessDefinitionGraphMapper.insertDataScheduleProcessDefinitionGraph(dataScheduleProcessDefinitionGraph);
    }

    /**
     * 修改数据调度-流程定义流程图
     * 
     * @param dataScheduleProcessDefinitionGraph 数据调度-流程定义流程图
     * @return 结果
     */
    @Override
    public int updateDataScheduleProcessDefinitionGraph(DataScheduleProcessDefinitionGraph dataScheduleProcessDefinitionGraph)
    {
        dataScheduleProcessDefinitionGraph.setUpdateTime(DateUtils.getNowDate());
        return dataScheduleProcessDefinitionGraphMapper.updateDataScheduleProcessDefinitionGraph(dataScheduleProcessDefinitionGraph);
    }

    /**
     * 批量删除数据调度-流程定义流程图
     * 
     * @param processDefinitionIds 需要删除的数据调度-流程定义流程图主键
     * @return 结果
     */
    @Override
    public int deleteDataScheduleProcessDefinitionGraphByProcessDefinitionIds(Long[] processDefinitionIds)
    {
        return dataScheduleProcessDefinitionGraphMapper.deleteDataScheduleProcessDefinitionGraphByProcessDefinitionIds(processDefinitionIds);
    }

    /**
     * 删除数据调度-流程定义流程图信息
     * 
     * @param processDefinitionId 数据调度-流程定义流程图主键
     * @return 结果
     */
    @Override
    public int deleteDataScheduleProcessDefinitionGraphByProcessDefinitionId(Long processDefinitionId)
    {
        return dataScheduleProcessDefinitionGraphMapper.deleteDataScheduleProcessDefinitionGraphByProcessDefinitionId(processDefinitionId);
    }
}
