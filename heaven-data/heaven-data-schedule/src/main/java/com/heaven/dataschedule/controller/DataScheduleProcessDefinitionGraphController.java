package com.heaven.dataschedule.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.heaven.common.log.annotation.Log;
import com.heaven.common.log.enums.BusinessType;
import com.heaven.common.security.annotation.RequiresPermissions;
import com.heaven.dataschedule.domain.DataScheduleProcessDefinitionGraph;
import com.heaven.dataschedule.service.IDataScheduleProcessDefinitionGraphService;
import com.heaven.common.core.web.controller.BaseController;
import com.heaven.common.core.web.domain.AjaxResult;
import com.heaven.common.core.utils.poi.ExcelUtil;
import com.heaven.common.core.web.page.TableDataInfo;

/**
 * 数据调度-流程定义流程图Controller
 * 
 * @author Vicene
 * @date 2022-12-07
 */
@RestController
@RequestMapping("/graph")
public class DataScheduleProcessDefinitionGraphController extends BaseController
{
    @Autowired
    private IDataScheduleProcessDefinitionGraphService dataScheduleProcessDefinitionGraphService;

    /**
     * 查询数据调度-流程定义流程图列表
     */
    @RequiresPermissions("ProcessDefinitionGraph:graph:list")
    @GetMapping("/list")
    public TableDataInfo list(DataScheduleProcessDefinitionGraph dataScheduleProcessDefinitionGraph)
    {
        startPage();
        List<DataScheduleProcessDefinitionGraph> list = dataScheduleProcessDefinitionGraphService.selectDataScheduleProcessDefinitionGraphList(dataScheduleProcessDefinitionGraph);
        return getDataTable(list);
    }

    /**
     * 导出数据调度-流程定义流程图列表
     */
    @RequiresPermissions("ProcessDefinitionGraph:graph:export")
    @Log(title = "数据调度-流程定义流程图", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DataScheduleProcessDefinitionGraph dataScheduleProcessDefinitionGraph)
    {
        List<DataScheduleProcessDefinitionGraph> list = dataScheduleProcessDefinitionGraphService.selectDataScheduleProcessDefinitionGraphList(dataScheduleProcessDefinitionGraph);
        ExcelUtil<DataScheduleProcessDefinitionGraph> util = new ExcelUtil<DataScheduleProcessDefinitionGraph>(DataScheduleProcessDefinitionGraph.class);
        util.exportExcel(response, list, "数据调度-流程定义流程图数据");
    }

    /**
     * 获取数据调度-流程定义流程图详细信息
     */
    @RequiresPermissions("ProcessDefinitionGraph:graph:query")
    @GetMapping(value = "/{processDefinitionId}")
    public AjaxResult getInfo(@PathVariable("processDefinitionId") Long processDefinitionId)
    {
        return AjaxResult.success(dataScheduleProcessDefinitionGraphService.selectDataScheduleProcessDefinitionGraphByProcessDefinitionId(processDefinitionId));
    }

    /**
     * 新增数据调度-流程定义流程图
     */
    @RequiresPermissions("ProcessDefinitionGraph:graph:add")
    @Log(title = "数据调度-流程定义流程图", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DataScheduleProcessDefinitionGraph dataScheduleProcessDefinitionGraph)
    {
        return toAjax(dataScheduleProcessDefinitionGraphService.insertDataScheduleProcessDefinitionGraph(dataScheduleProcessDefinitionGraph));
    }

    /**
     * 修改数据调度-流程定义流程图
     */
    @RequiresPermissions("ProcessDefinitionGraph:graph:edit")
    @Log(title = "数据调度-流程定义流程图", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DataScheduleProcessDefinitionGraph dataScheduleProcessDefinitionGraph)
    {
        return toAjax(dataScheduleProcessDefinitionGraphService.updateDataScheduleProcessDefinitionGraph(dataScheduleProcessDefinitionGraph));
    }

    /**
     * 删除数据调度-流程定义流程图
     */
    @RequiresPermissions("ProcessDefinitionGraph:graph:remove")
    @Log(title = "数据调度-流程定义流程图", businessType = BusinessType.DELETE)
	@DeleteMapping("/{processDefinitionIds}")
    public AjaxResult remove(@PathVariable Long[] processDefinitionIds)
    {
        return toAjax(dataScheduleProcessDefinitionGraphService.deleteDataScheduleProcessDefinitionGraphByProcessDefinitionIds(processDefinitionIds));
    }
}
