package com.heaven.dataconverge.service;

import java.util.List;
import com.heaven.dataconverge.domain.DataConvergeBusinessDatadomain;

/**
 * 数据模块-数据汇聚-数据域Service接口
 * 
 * @author Vicene
 * @date 2022-11-29
 */
public interface IDataConvergeBusinessDatadomainService 
{
    /**
     * 查询数据模块-数据汇聚-数据域
     * 
     * @param id 数据模块-数据汇聚-数据域主键
     * @return 数据模块-数据汇聚-数据域
     */
    public DataConvergeBusinessDatadomain selectDataConvergeBusinessDatadomainById(Long id);

    /**
     * 查询数据模块-数据汇聚-数据域列表
     * 
     * @param dataConvergeBusinessDatadomain 数据模块-数据汇聚-数据域
     * @return 数据模块-数据汇聚-数据域集合
     */
    public List<DataConvergeBusinessDatadomain> selectDataConvergeBusinessDatadomainList(DataConvergeBusinessDatadomain dataConvergeBusinessDatadomain);

    /**
     * 新增数据模块-数据汇聚-数据域
     * 
     * @param dataConvergeBusinessDatadomain 数据模块-数据汇聚-数据域
     * @return 结果
     */
    public int insertDataConvergeBusinessDatadomain(DataConvergeBusinessDatadomain dataConvergeBusinessDatadomain);

    /**
     * 修改数据模块-数据汇聚-数据域
     * 
     * @param dataConvergeBusinessDatadomain 数据模块-数据汇聚-数据域
     * @return 结果
     */
    public int updateDataConvergeBusinessDatadomain(DataConvergeBusinessDatadomain dataConvergeBusinessDatadomain);

    /**
     * 批量删除数据模块-数据汇聚-数据域
     * 
     * @param ids 需要删除的数据模块-数据汇聚-数据域主键集合
     * @return 结果
     */
    public int deleteDataConvergeBusinessDatadomainByIds(Long[] ids);

    /**
     * 删除数据模块-数据汇聚-数据域信息
     * 
     * @param id 数据模块-数据汇聚-数据域主键
     * @return 结果
     */
    public int deleteDataConvergeBusinessDatadomainById(Long id);
}
