package com.heaven.dataconverge.vo.source;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SourceTestConnInVo implements Serializable {
    private String sourceId;

    private String sourceType;

    private String name;

    private Map<String, Object> properties;
}
