package com.heaven.dataconverge.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.heaven.dataconverge.dataprovider.base.jdbc.JdbcDriverInfo;
import com.heaven.dataconverge.domain.DataConvergeSource;
import com.heaven.dataconverge.vo.source.SourceTestConnInVo;

/**
 * 数据源Service接口
 * 
 * @author Vicene
 * @date 2022-11-21
 */
public interface IDataConvergeSourceService 
{
    /**
     * 查询数据源
     * 
     * @param id 数据源主键
     * @return 数据源
     */
    DataConvergeSource selectDataConvergeSourceById(Long id);

    /**
     * 查询数据源列表
     * 
     * @param dataConvergeSource 数据源
     * @return 数据源集合
     */
    List<DataConvergeSource> selectDataConvergeSourceList(DataConvergeSource dataConvergeSource);

    /**
     * 新增数据源
     * 
     * @param dataConvergeSource 数据源
     * @return 结果
     */
    int insertDataConvergeSource(DataConvergeSource dataConvergeSource);

    /**
     * 修改数据源
     * 
     * @param dataConvergeSource 数据源
     * @return 结果
     */
    int updateDataConvergeSource(DataConvergeSource dataConvergeSource);

    /**
     * 批量删除数据源
     * 
     * @param ids 需要删除的数据源主键集合
     * @return 结果
     */
    int deleteDataConvergeSourceByIds(Long[] ids);

    /**
     * 删除数据源信息
     * 
     * @param id 数据源主键
     * @return 结果
     */
    int deleteDataConvergeSourceById(Long id);

    /**
     * 测试数据源
     *
     * @param SourceTestConnInVo
     * @return
     */
    Object testConn(SourceTestConnInVo inVo) throws Exception;

    /**
     * 获取JDBC数据源额外的信息,例如driverClass,url等
     * @return
     */
    Map<String, Map<String, String>> getExtDriverInfos();

    /**
     * 获取数据源列表（包含业务板块等）
     * @param dataConvergeSource
     * @return
     */
    List<DataConvergeSource> selectSourceBusinessSectionList(DataConvergeSource dataConvergeSource);

    /**
     * 数据源详情
     * @param id
     * @return
     */
    DataConvergeSource selectDetailById(Long id);

    /**
     * 获取数据源类型实例
     * @param dbType
     * @return
     */
    List<DataConvergeSource> getDataSourceInstance(String dbType);
}
