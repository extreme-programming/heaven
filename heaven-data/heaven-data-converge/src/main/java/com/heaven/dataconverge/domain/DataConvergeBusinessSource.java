package com.heaven.dataconverge.domain;

import lombok.Builder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.heaven.common.core.annotation.Excel;
import com.heaven.common.core.web.domain.BaseEntity;

/**
 * 业务板块与数据源关联对象 data_converge_business_source
 * 
 * @author Vicene
 * @date 2022-11-25
 */
@Builder
public class DataConvergeBusinessSource extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 业务板块id */
    private Long businessSectionId;

    /** 数据源id */
    private Long sourceId;

    public void setBusinessSectionId(Long businessSectionId) 
    {
        this.businessSectionId = businessSectionId;
    }

    public Long getBusinessSectionId() 
    {
        return businessSectionId;
    }
    public void setSourceId(Long sourceId) 
    {
        this.sourceId = sourceId;
    }

    public Long getSourceId() 
    {
        return sourceId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("businessSectionId", getBusinessSectionId())
            .append("sourceId", getSourceId())
            .toString();
    }
}
