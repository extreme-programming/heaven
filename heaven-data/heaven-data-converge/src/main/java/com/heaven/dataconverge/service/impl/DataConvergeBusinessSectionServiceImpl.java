package com.heaven.dataconverge.service.impl;

import java.util.List;
import com.heaven.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.heaven.dataconverge.mapper.DataConvergeBusinessSectionMapper;
import com.heaven.dataconverge.domain.DataConvergeBusinessSection;
import com.heaven.dataconverge.service.IDataConvergeBusinessSectionService;

/**
 * 数据模块-数据汇聚-业务板块Service业务层处理
 *
 * @author Vicene
 * @date 2022-11-25
 */
@Service
public class DataConvergeBusinessSectionServiceImpl implements IDataConvergeBusinessSectionService
{
    @Autowired
    private DataConvergeBusinessSectionMapper dataConvergeBusinessSectionMapper;

    /**
     * 查询数据模块-数据汇聚-业务板块
     *
     * @param id 数据模块-数据汇聚-业务板块主键
     * @return 数据模块-数据汇聚-业务板块
     */
    @Override
    public DataConvergeBusinessSection selectDataConvergeBusinessSectionById(Long id)
    {
        return dataConvergeBusinessSectionMapper.selectDataConvergeBusinessSectionById(id);
    }

    /**
     * 查询数据模块-数据汇聚-业务板块列表
     *
     * @param dataConvergeBusinessSection 数据模块-数据汇聚-业务板块
     * @return 数据模块-数据汇聚-业务板块
     */
    @Override
    public List<DataConvergeBusinessSection> selectDataConvergeBusinessSectionList(DataConvergeBusinessSection dataConvergeBusinessSection)
    {
        return dataConvergeBusinessSectionMapper.selectDataConvergeBusinessSectionList(dataConvergeBusinessSection);
    }

    /**
     * 新增数据模块-数据汇聚-业务板块
     *
     * @param dataConvergeBusinessSection 数据模块-数据汇聚-业务板块
     * @return 结果
     */
    @Override
    public int insertDataConvergeBusinessSection(DataConvergeBusinessSection dataConvergeBusinessSection)
    {
        dataConvergeBusinessSection.setCreateTime(DateUtils.getNowDate());
        return dataConvergeBusinessSectionMapper.insertDataConvergeBusinessSection(dataConvergeBusinessSection);
    }

    /**
     * 修改数据模块-数据汇聚-业务板块
     *
     * @param dataConvergeBusinessSection 数据模块-数据汇聚-业务板块
     * @return 结果
     */
    @Override
    public int updateDataConvergeBusinessSection(DataConvergeBusinessSection dataConvergeBusinessSection)
    {
        dataConvergeBusinessSection.setUpdateTime(DateUtils.getNowDate());
        return dataConvergeBusinessSectionMapper.updateDataConvergeBusinessSection(dataConvergeBusinessSection);
    }

    /**
     * 批量删除数据模块-数据汇聚-业务板块
     *
     * @param ids 需要删除的数据模块-数据汇聚-业务板块主键
     * @return 结果
     */
    @Override
    public int deleteDataConvergeBusinessSectionByIds(Long[] ids)
    {
        return dataConvergeBusinessSectionMapper.deleteDataConvergeBusinessSectionByIds(ids);
    }

    /**
     * 删除数据模块-数据汇聚-业务板块信息
     *
     * @param id 数据模块-数据汇聚-业务板块主键
     * @return 结果
     */
    @Override
    public int deleteDataConvergeBusinessSectionById(Long id)
    {
        return dataConvergeBusinessSectionMapper.deleteDataConvergeBusinessSectionById(id);
    }
}