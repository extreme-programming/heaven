package com.heaven.dataconverge.service;

import java.util.List;
import com.heaven.dataconverge.domain.DataConvergeBusinessSource;

/**
 * 业务板块与数据源关联Service接口
 * 
 * @author Vicene
 * @date 2022-11-25
 */
public interface IDataConvergeBusinessSourceService 
{
    /**
     * 查询业务板块与数据源关联
     * 
     * @param businessSectionId 业务板块与数据源关联主键
     * @return 业务板块与数据源关联
     */
    public DataConvergeBusinessSource selectDataConvergeBusinessSourceByBusinessSectionId(Long businessSectionId);

    /**
     * 查询业务板块与数据源关联列表
     * 
     * @param dataConvergeBusinessSource 业务板块与数据源关联
     * @return 业务板块与数据源关联集合
     */
    public List<DataConvergeBusinessSource> selectDataConvergeBusinessSourceList(DataConvergeBusinessSource dataConvergeBusinessSource);

    /**
     * 新增业务板块与数据源关联
     * 
     * @param dataConvergeBusinessSource 业务板块与数据源关联
     * @return 结果
     */
    public int insertDataConvergeBusinessSource(DataConvergeBusinessSource dataConvergeBusinessSource);

    /**
     * 修改业务板块与数据源关联
     * 
     * @param dataConvergeBusinessSource 业务板块与数据源关联
     * @return 结果
     */
    public int updateDataConvergeBusinessSource(DataConvergeBusinessSource dataConvergeBusinessSource);

    /**
     * 批量删除业务板块与数据源关联
     * 
     * @param businessSectionIds 需要删除的业务板块与数据源关联主键集合
     * @return 结果
     */
    public int deleteDataConvergeBusinessSourceByBusinessSectionIds(Long[] businessSectionIds);

    /**
     * 删除业务板块与数据源关联信息
     * 
     * @param businessSectionId 业务板块与数据源关联主键
     * @return 结果
     */
    public int deleteDataConvergeBusinessSourceByBusinessSectionId(Long businessSectionId);
}
