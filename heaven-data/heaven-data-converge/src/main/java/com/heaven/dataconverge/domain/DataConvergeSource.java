package com.heaven.dataconverge.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.heaven.common.core.annotation.Excel;
import com.heaven.common.core.web.domain.BaseEntity;

import java.util.List;

/**
 * 数据源对象 data_converge_source
 * 
 * @author Vicene
 * @date 2022-11-21
 */
public class DataConvergeSource extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 配置 */
    @Excel(name = "配置")
    private String config;

    /** 数据源类型:1:FILE 2:JDBC 3:HTTP */
    @Excel(name = "数据源类型:1:FILE 2:JDBC 3:HTTP")
    private String sourceType;

    /** 例如JDBC:MYSQL */
    @Excel(name = "例如JDBC:MYSQL")
    private String childType;

    /** 显示顺序 */
    @Excel(name = "显示顺序")
    private Integer orderNum;

    public String getBusinessSectionNamesStr() {
        return businessSectionNamesStr;
    }

    public void setBusinessSectionNamesStr(String businessSectionNamesStr) {
        this.businessSectionNamesStr = businessSectionNamesStr;
    }

    public String getBusinessSectionIdsStr() {
        return businessSectionIdsStr;
    }

    public void setBusinessSectionIdsStr(String businessSectionIdsStr) {
        this.businessSectionIdsStr = businessSectionIdsStr;
    }

    /** 数据源类别（0数仓数据源 1业务数据源） */
    @Excel(name = "状态", readConverterExp = "0=正常,1=停用")
    private String status;

    private List<Long> businessSectionIds;

    private String businessSectionNamesStr;

    private String businessSectionIdsStr;

    public List<Long> getBusinessSectionIds() {
        return businessSectionIds;
    }

    public void setBusinessSectionIds( List<Long> businessSectionIds) {
        this.businessSectionIds = businessSectionIds;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setConfig(String config) 
    {
        this.config = config;
    }

    public String getConfig() 
    {
        return config;
    }
    public void setSourceType(String sourceType) 
    {
        this.sourceType = sourceType;
    }

    public String getSourceType() 
    {
        return sourceType;
    }
    public void setChildType(String childType) 
    {
        this.childType = childType;
    }

    public String getChildType() 
    {
        return childType;
    }
    public void setOrderNum(Integer orderNum) 
    {
        this.orderNum = orderNum;
    }

    public Integer getOrderNum() 
    {
        return orderNum;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

}
