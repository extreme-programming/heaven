package com.heaven.dataconverge.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.heaven.dataconverge.vo.source.SourceTestConnInVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.heaven.common.log.annotation.Log;
import com.heaven.common.log.enums.BusinessType;
import com.heaven.common.security.annotation.RequiresPermissions;
import com.heaven.dataconverge.domain.DataConvergeSource;
import com.heaven.dataconverge.service.IDataConvergeSourceService;
import com.heaven.common.core.web.controller.BaseController;
import com.heaven.common.core.web.domain.AjaxResult;
import com.heaven.common.core.utils.poi.ExcelUtil;
import com.heaven.common.core.web.page.TableDataInfo;

/**
 * 数据源Controller
 * 
 * @author Vicene
 * @date 2022-11-21
 */
@RestController
@RequestMapping("/source")
public class DataConvergeSourceController extends BaseController
{
    @Autowired
    private IDataConvergeSourceService dataConvergeSourceService;

    /**
     * 查询数据源列表
     */
    @RequiresPermissions("dataConverge:source:list")
    @GetMapping("/list")
    public AjaxResult list(DataConvergeSource dataConvergeSource)
    {
        List<DataConvergeSource> list = dataConvergeSourceService.selectSourceBusinessSectionList(dataConvergeSource);
        return AjaxResult.success(list);
    }

    /**
     * 导出数据源列表
     */
    @RequiresPermissions("dataConverge:source:export")
    @Log(title = "数据源", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DataConvergeSource dataConvergeSource)
    {
        List<DataConvergeSource> list = dataConvergeSourceService.selectDataConvergeSourceList(dataConvergeSource);
        ExcelUtil<DataConvergeSource> util = new ExcelUtil<DataConvergeSource>(DataConvergeSource.class);
        util.exportExcel(response, list, "数据源数据");
    }

    /**
     * 获取数据源详细信息
     */
    @RequiresPermissions("dataConverge:source:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(dataConvergeSourceService.selectDataConvergeSourceById(id));
    }

    /**
     * 新增数据源
     */
    @RequiresPermissions("dataConverge:source:add")
    @Log(title = "数据源", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DataConvergeSource dataConvergeSource)
    {
        return toAjax(dataConvergeSourceService.insertDataConvergeSource(dataConvergeSource));
    }

    /**
     * 修改数据源
     */
    @RequiresPermissions("dataConverge:source:edit")
    @Log(title = "数据源", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DataConvergeSource dataConvergeSource)
    {
        return toAjax(dataConvergeSourceService.updateDataConvergeSource(dataConvergeSource));
    }

    /**
     * 删除数据源
     */
    @RequiresPermissions("dataConverge:source:remove")
    @Log(title = "数据源", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dataConvergeSourceService.deleteDataConvergeSourceByIds(ids));
    }

    /**
     * 删除数据源
     */
    @RequiresPermissions("dataConverge:source:testConn")
    @Log(title = "数据源", businessType = BusinessType.SOURCE_TEST_CONN)
    @PostMapping("/testConn")
    public AjaxResult testConn(@RequestBody SourceTestConnInVo inVo) throws Exception {
        return AjaxResult.success(dataConvergeSourceService.testConn(inVo));
    }

    /**
     * 获取数据源字典
     */
    @RequiresPermissions("dataConverge:source:getExtDriverInfos")
    @GetMapping("/getExtDriverInfos")
    public AjaxResult getExtDriverInfos() {
        return AjaxResult.success(dataConvergeSourceService.getExtDriverInfos());
    }

    /**
     * 获取数据源详细信息
     */
    @RequiresPermissions("dataConverge:source:detail")
    @GetMapping(value = "/detail/{id}")
    public AjaxResult detail(@PathVariable("id") Long id)
    {
        return AjaxResult.success(dataConvergeSourceService.selectDetailById(id));
    }

    @RequiresPermissions("dataConverge:source:getDataSourceInstance")
    @GetMapping(value = "/getDataSourceInstance/{dbType}")
    public AjaxResult getDataSourceInstance(@PathVariable("dbType") String dbType)
    {
        return AjaxResult.success(dataConvergeSourceService.getDataSourceInstance(dbType));
    }


}
