package com.heaven.dataconverge.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.heaven.common.log.annotation.Log;
import com.heaven.common.log.enums.BusinessType;
import com.heaven.common.security.annotation.RequiresPermissions;
import com.heaven.dataconverge.domain.DataConvergeBusinessSection;
import com.heaven.dataconverge.service.IDataConvergeBusinessSectionService;
import com.heaven.common.core.web.controller.BaseController;
import com.heaven.common.core.web.domain.AjaxResult;
import com.heaven.common.core.utils.poi.ExcelUtil;
import com.heaven.common.core.web.page.TableDataInfo;

/**
 * 数据模块-数据汇聚-业务板块Controller
 *
 * @author Vicene
 * @date 2022-11-25
 */
@RestController
@RequestMapping("/businessSection")
public class DataConvergeBusinessSectionController extends BaseController
{
    @Autowired
    private IDataConvergeBusinessSectionService dataConvergeBusinessSectionService;

    /**
     * 查询数据模块-数据汇聚-业务板块列表
     */
    @RequiresPermissions("dataConverge:businessSection:list")
    @GetMapping("/list")
    public TableDataInfo list(DataConvergeBusinessSection dataConvergeBusinessSection)
    {
        startPage();
        List<DataConvergeBusinessSection> list = dataConvergeBusinessSectionService.selectDataConvergeBusinessSectionList(dataConvergeBusinessSection);
        return getDataTable(list);
    }

    /**
     * 导出数据模块-数据汇聚-业务板块列表
     */
    @RequiresPermissions("dataConverge:businessSection:export")
    @Log(title = "数据模块-数据汇聚-业务板块", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DataConvergeBusinessSection dataConvergeBusinessSection)
    {
        List<DataConvergeBusinessSection> list = dataConvergeBusinessSectionService.selectDataConvergeBusinessSectionList(dataConvergeBusinessSection);
        ExcelUtil<DataConvergeBusinessSection> util = new ExcelUtil<DataConvergeBusinessSection>(DataConvergeBusinessSection.class);
        util.exportExcel(response, list, "数据模块-数据汇聚-业务板块数据");
    }

    /**
     * 获取数据模块-数据汇聚-业务板块详细信息
     */
    @RequiresPermissions("dataConverge:businessSection:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(dataConvergeBusinessSectionService.selectDataConvergeBusinessSectionById(id));
    }

    /**
     * 新增数据模块-数据汇聚-业务板块
     */
    @RequiresPermissions("dataConverge:BusinessSection:add")
    @Log(title = "数据模块-数据汇聚-业务板块", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DataConvergeBusinessSection dataConvergeBusinessSection)
    {
        return toAjax(dataConvergeBusinessSectionService.insertDataConvergeBusinessSection(dataConvergeBusinessSection));
    }

    /**
     * 修改数据模块-数据汇聚-业务板块
     */
    @RequiresPermissions("dataConverge:businessSection:edit")
    @Log(title = "数据模块-数据汇聚-业务板块", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DataConvergeBusinessSection dataConvergeBusinessSection)
    {
        return toAjax(dataConvergeBusinessSectionService.updateDataConvergeBusinessSection(dataConvergeBusinessSection));
    }

    /**
     * 删除数据模块-数据汇聚-业务板块
     */
    @RequiresPermissions("dataConverge:businessSection:remove")
    @Log(title = "数据模块-数据汇聚-业务板块", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dataConvergeBusinessSectionService.deleteDataConvergeBusinessSectionByIds(ids));
    }
}