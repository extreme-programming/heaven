package com.heaven.dataconverge.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.json.JSONUtil;
import com.heaven.common.core.utils.DateUtils;
import com.heaven.common.data.constant.Constants;
import com.heaven.common.data.info.DataSourceInfo;
import com.heaven.common.data.provider.DataProviderSource;
import com.heaven.common.data.util.AESUtil;
import com.heaven.common.security.utils.SecurityUtils;
import com.heaven.dataconverge.domain.DataConvergeSource;
import com.heaven.dataconverge.dataprovider.base.ProviderManager;
import com.heaven.dataconverge.dataprovider.jdbc.JdbcDataProvider;
import com.heaven.dataconverge.domain.DataConvergeBusinessSection;
import com.heaven.dataconverge.domain.DataConvergeBusinessSource;
import com.heaven.dataconverge.enums.SourceTypeEnum;
import com.heaven.dataconverge.mapper.DataConvergeBusinessSectionMapper;
import com.heaven.dataconverge.mapper.DataConvergeBusinessSourceMapper;
import com.heaven.dataconverge.vo.source.SourceTestConnInVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.heaven.dataconverge.mapper.DataConvergeSourceMapper;
import com.heaven.dataconverge.service.IDataConvergeSourceService;
import org.springframework.util.CollectionUtils;

/**
 * 数据源Service业务层处理
 *
 * @author Vicene
 * @date 2022-11-21
 */
@Service
public class DataConvergeSourceServiceImpl implements IDataConvergeSourceService
{
    @Autowired
    private DataConvergeSourceMapper dataConvergeSourceMapper;

    @Autowired
    private ProviderManager providerManager;

    @Autowired
    private DataConvergeBusinessSourceMapper dataConvergeBusinessSourceMapper;

    @Autowired
    private DataConvergeBusinessSectionMapper dataConvergeBusinessSectionMapper;


    /**
     * 查询数据源
     *
     * @param id 数据源主键
     * @return 数据源
     */
    @Override
    public DataConvergeSource selectDataConvergeSourceById(Long id)
    {
        return dataConvergeSourceMapper.selectDataConvergeSourceById(id);
    }

    /**
     * 查询数据源列表
     *
     * @param dataConvergeSource 数据源
     * @return 数据源
     */
    @Override
    public List<DataConvergeSource> selectDataConvergeSourceList(DataConvergeSource dataConvergeSource)
    {
        return dataConvergeSourceMapper.selectDataConvergeSourceList(dataConvergeSource);
    }

    /**
     * 新增数据源
     *
     * @param dataConvergeSource 数据源
     * @return 结果
     */
    @Override
    public int insertDataConvergeSource(DataConvergeSource dataConvergeSource)
    {
        if(Integer.valueOf(dataConvergeSource.getSourceType()).intValue() == SourceTypeEnum.JDBC.ordinal()){
            DataSourceInfo dataSourceInfo = JSONUtil.toBean(dataConvergeSource.getConfig() , DataSourceInfo.class);
            if(!StringUtils.isEmpty(dataSourceInfo.getPassword())){
                dataSourceInfo.setPassword(Constants.ENCRYPT_FLAG.concat(AESUtil.encrypt(dataSourceInfo.getPassword())));
            }
            dataConvergeSource.setConfig(JSONUtil.toJsonStr(dataSourceInfo));
            dataConvergeSource.setCreateTime(DateUtils.getNowDate());
            dataConvergeSource.setCreateBy(SecurityUtils.getUsername());
            int count = dataConvergeSourceMapper.insertDataConvergeSource(dataConvergeSource);
            for(Long businessSectionId : dataConvergeSource.getBusinessSectionIds()){
                dataConvergeBusinessSourceMapper
                        .insertDataConvergeBusinessSource(
                                DataConvergeBusinessSource.builder().sourceId(dataConvergeSource.getId())
                                        .businessSectionId(businessSectionId).build());
            }

            return count;
        }
        return 0;
    }

    /**
     * 修改数据源
     *
     * @param dataConvergeSource 数据源
     * @return 结果
     */
    @Override
    public int updateDataConvergeSource(DataConvergeSource dataConvergeSource)
    {
        dataConvergeSource.setUpdateTime(DateUtils.getNowDate());
        return dataConvergeSourceMapper.updateDataConvergeSource(dataConvergeSource);
    }

    /**
     * 批量删除数据源
     *
     * @param ids 需要删除的数据源主键
     * @return 结果
     */
    @Override
    public int deleteDataConvergeSourceByIds(Long[] ids)
    {
        dataConvergeBusinessSourceMapper.deleteDataConvergeBusinessSourceBySourceIds(ids);
        return dataConvergeSourceMapper.deleteDataConvergeSourceByIds(ids);
    }

    /**
     * 删除数据源信息
     *
     * @param id 数据源主键
     * @return 结果
     */
    @Override
    public int deleteDataConvergeSourceById(Long id)
    {
        return dataConvergeSourceMapper.deleteDataConvergeSourceById(id);
    }

    /**
     * 测试数据源
     *
     * @param SourceTestConnInVo
     * @return
     */
    @Override
    public Object testConn(SourceTestConnInVo inVo) throws Exception {
        DataProviderSource dataProviderSource = BeanUtil.copyProperties(inVo , DataProviderSource.class );
        Map<String, Object> properties = dataProviderSource.getProperties();
        if (!CollectionUtils.isEmpty(properties)) {
            for (String key : properties.keySet()) {
                Object val = properties.get(key);
                if (val instanceof String) {
                    properties.put(key, decryptValue(val.toString()));
                }
            }
        }
        return providerManager.testConnection(dataProviderSource);
    }

    @Override
    public  Map<String, Map<String, String>>  getExtDriverInfos() {
        return JdbcDataProvider.ProviderFactory.CACHE_DRIVER_INFOS;
    }


    public String decryptValue(String value) {
        if (StringUtils.isEmpty(value)) {
            return value;
        }
        if (!value.startsWith(Constants.ENCRYPT_FLAG)) {
            return value;
        }
        try {
            return AESUtil.decrypt(value.replaceFirst(Constants.ENCRYPT_FLAG, ""));
        } catch (Exception e) {
            return value;
        }
    }

    /**
     * 获取数据源列表（包含业务板块等）
     * @param dataConvergeSource
     * @return
     */
    @Override
    public List<DataConvergeSource> selectSourceBusinessSectionList(DataConvergeSource dataConvergeSource) {
        if(CollectionUtils.isEmpty(dataConvergeSource.getBusinessSectionIds())){

            Map<String ,Object> param = new HashMap();
            param.put("pageSize" ,com.heaven.common.core.constant.Constants.LIST_WITHPAGE_PAGE_SIZE);
            DataConvergeBusinessSection params = DataConvergeBusinessSection.builder().build();
            params.setParams(param);
            dataConvergeSource.setBusinessSectionIds(
                    dataConvergeBusinessSectionMapper.selectDataConvergeBusinessSectionList(params)
                            .stream().map(n->n.getId()).collect(Collectors.toList()));
        }
        return dataConvergeSourceMapper.selectSourceBusinessSectionList(dataConvergeSource);
    }

    /**
     * 数据源详情
     * @param id
     * @return
     */
    @Override
    public DataConvergeSource selectDetailById(Long id) {
        return dataConvergeSourceMapper.selectDetailById(id);
    }

    /**
     * 获取数据源类型实例
     * @param dbType
     * @return
     */
    @Override
    public List<DataConvergeSource> getDataSourceInstance(String dbType) {
        return dataConvergeSourceMapper.getDataSourceInstance(dbType);
    }
}
