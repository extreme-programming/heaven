package com.heaven.dataconverge.controller;

import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.heaven.common.log.annotation.Log;
import com.heaven.common.log.enums.BusinessType;
import com.heaven.common.security.annotation.RequiresPermissions;
import com.heaven.dataconverge.domain.DataConvergeBusinessDatadomain;
import com.heaven.dataconverge.service.IDataConvergeBusinessDatadomainService;
import com.heaven.common.core.web.controller.BaseController;
import com.heaven.common.core.web.domain.AjaxResult;
import com.heaven.common.core.utils.poi.ExcelUtil;
import com.heaven.common.core.web.page.TableDataInfo;

/**
 * 数据模块-数据汇聚-数据域Controller
 * 
 * @author Vicene
 * @date 2022-11-29
 */
@RestController
@RequestMapping("/datadomain")
public class DataConvergeBusinessDatadomainController extends BaseController
{
    @Autowired
    private IDataConvergeBusinessDatadomainService dataConvergeBusinessDatadomainService;

    /**
     * 查询数据模块-数据汇聚-数据域列表
     */
    @RequiresPermissions("Datadomain:datadomain:list")
    @GetMapping("/list")
    public TableDataInfo list(DataConvergeBusinessDatadomain dataConvergeBusinessDatadomain)
    {
        startPage();
        List<DataConvergeBusinessDatadomain> list = dataConvergeBusinessDatadomainService.selectDataConvergeBusinessDatadomainList(dataConvergeBusinessDatadomain);
        return getDataTable(list);
    }

    /**
     * 导出数据模块-数据汇聚-数据域列表
     */
    @RequiresPermissions("Datadomain:datadomain:export")
    @Log(title = "数据模块-数据汇聚-数据域", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, DataConvergeBusinessDatadomain dataConvergeBusinessDatadomain)
    {
        List<DataConvergeBusinessDatadomain> list = dataConvergeBusinessDatadomainService.selectDataConvergeBusinessDatadomainList(dataConvergeBusinessDatadomain);
        ExcelUtil<DataConvergeBusinessDatadomain> util = new ExcelUtil<DataConvergeBusinessDatadomain>(DataConvergeBusinessDatadomain.class);
        util.exportExcel(response, list, "数据模块-数据汇聚-数据域数据");
    }

    /**
     * 获取数据模块-数据汇聚-数据域详细信息
     */
    @RequiresPermissions("Datadomain:datadomain:query")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(dataConvergeBusinessDatadomainService.selectDataConvergeBusinessDatadomainById(id));
    }

    /**
     * 新增数据模块-数据汇聚-数据域
     */
    @RequiresPermissions("Datadomain:datadomain:add")
    @Log(title = "数据模块-数据汇聚-数据域", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody DataConvergeBusinessDatadomain dataConvergeBusinessDatadomain)
    {
        return toAjax(dataConvergeBusinessDatadomainService.insertDataConvergeBusinessDatadomain(dataConvergeBusinessDatadomain));
    }

    /**
     * 修改数据模块-数据汇聚-数据域
     */
    @RequiresPermissions("Datadomain:datadomain:edit")
    @Log(title = "数据模块-数据汇聚-数据域", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody DataConvergeBusinessDatadomain dataConvergeBusinessDatadomain)
    {
        return toAjax(dataConvergeBusinessDatadomainService.updateDataConvergeBusinessDatadomain(dataConvergeBusinessDatadomain));
    }

    /**
     * 删除数据模块-数据汇聚-数据域
     */
    @RequiresPermissions("Datadomain:datadomain:remove")
    @Log(title = "数据模块-数据汇聚-数据域", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(dataConvergeBusinessDatadomainService.deleteDataConvergeBusinessDatadomainByIds(ids));
    }
}
