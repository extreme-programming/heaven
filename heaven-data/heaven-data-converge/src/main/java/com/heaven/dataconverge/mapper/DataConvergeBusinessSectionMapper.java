package com.heaven.dataconverge.mapper;

import java.util.List;
import com.heaven.dataconverge.domain.DataConvergeBusinessSection;

/**
 * 数据模块-数据汇聚-业务板块Mapper接口
 *
 * @author Vicene
 * @date 2022-11-25
 */
public interface DataConvergeBusinessSectionMapper
{
    /**
     * 查询数据模块-数据汇聚-业务板块
     *
     * @param id 数据模块-数据汇聚-业务板块主键
     * @return 数据模块-数据汇聚-业务板块
     */
    public DataConvergeBusinessSection selectDataConvergeBusinessSectionById(Long id);

    /**
     * 查询数据模块-数据汇聚-业务板块列表
     *
     * @param dataConvergeBusinessSection 数据模块-数据汇聚-业务板块
     * @return 数据模块-数据汇聚-业务板块集合
     */
    public List<DataConvergeBusinessSection> selectDataConvergeBusinessSectionList(DataConvergeBusinessSection dataConvergeBusinessSection);

    /**
     * 新增数据模块-数据汇聚-业务板块
     *
     * @param dataConvergeBusinessSection 数据模块-数据汇聚-业务板块
     * @return 结果
     */
    public int insertDataConvergeBusinessSection(DataConvergeBusinessSection dataConvergeBusinessSection);

    /**
     * 修改数据模块-数据汇聚-业务板块
     *
     * @param dataConvergeBusinessSection 数据模块-数据汇聚-业务板块
     * @return 结果
     */
    public int updateDataConvergeBusinessSection(DataConvergeBusinessSection dataConvergeBusinessSection);

    /**
     * 删除数据模块-数据汇聚-业务板块
     *
     * @param id 数据模块-数据汇聚-业务板块主键
     * @return 结果
     */
    public int deleteDataConvergeBusinessSectionById(Long id);

    /**
     * 批量删除数据模块-数据汇聚-业务板块
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDataConvergeBusinessSectionByIds(Long[] ids);
}