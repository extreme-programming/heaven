package com.heaven.dataconverge.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.heaven.dataconverge.mapper.DataConvergeBusinessSourceMapper;
import com.heaven.dataconverge.domain.DataConvergeBusinessSource;
import com.heaven.dataconverge.service.IDataConvergeBusinessSourceService;

/**
 * 业务板块与数据源关联Service业务层处理
 * 
 * @author Vicene
 * @date 2022-11-25
 */
@Service
public class DataConvergeBusinessSourceServiceImpl implements IDataConvergeBusinessSourceService 
{
    @Autowired
    private DataConvergeBusinessSourceMapper dataConvergeBusinessSourceMapper;

    /**
     * 查询业务板块与数据源关联
     * 
     * @param businessSectionId 业务板块与数据源关联主键
     * @return 业务板块与数据源关联
     */
    @Override
    public DataConvergeBusinessSource selectDataConvergeBusinessSourceByBusinessSectionId(Long businessSectionId)
    {
        return dataConvergeBusinessSourceMapper.selectDataConvergeBusinessSourceByBusinessSectionId(businessSectionId);
    }

    /**
     * 查询业务板块与数据源关联列表
     * 
     * @param dataConvergeBusinessSource 业务板块与数据源关联
     * @return 业务板块与数据源关联
     */
    @Override
    public List<DataConvergeBusinessSource> selectDataConvergeBusinessSourceList(DataConvergeBusinessSource dataConvergeBusinessSource)
    {
        return dataConvergeBusinessSourceMapper.selectDataConvergeBusinessSourceList(dataConvergeBusinessSource);
    }

    /**
     * 新增业务板块与数据源关联
     * 
     * @param dataConvergeBusinessSource 业务板块与数据源关联
     * @return 结果
     */
    @Override
    public int insertDataConvergeBusinessSource(DataConvergeBusinessSource dataConvergeBusinessSource)
    {
        return dataConvergeBusinessSourceMapper.insertDataConvergeBusinessSource(dataConvergeBusinessSource);
    }

    /**
     * 修改业务板块与数据源关联
     * 
     * @param dataConvergeBusinessSource 业务板块与数据源关联
     * @return 结果
     */
    @Override
    public int updateDataConvergeBusinessSource(DataConvergeBusinessSource dataConvergeBusinessSource)
    {
        return dataConvergeBusinessSourceMapper.updateDataConvergeBusinessSource(dataConvergeBusinessSource);
    }

    /**
     * 批量删除业务板块与数据源关联
     * 
     * @param businessSectionIds 需要删除的业务板块与数据源关联主键
     * @return 结果
     */
    @Override
    public int deleteDataConvergeBusinessSourceByBusinessSectionIds(Long[] businessSectionIds)
    {
        return dataConvergeBusinessSourceMapper.deleteDataConvergeBusinessSourceByBusinessSectionIds(businessSectionIds);
    }

    /**
     * 删除业务板块与数据源关联信息
     * 
     * @param businessSectionId 业务板块与数据源关联主键
     * @return 结果
     */
    @Override
    public int deleteDataConvergeBusinessSourceByBusinessSectionId(Long businessSectionId)
    {
        return dataConvergeBusinessSourceMapper.deleteDataConvergeBusinessSourceByBusinessSectionId(businessSectionId);
    }
}
