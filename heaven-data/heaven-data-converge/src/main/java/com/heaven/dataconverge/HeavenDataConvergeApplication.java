package com.heaven.dataconverge;

import com.heaven.common.security.annotation.EnableCustomConfig;
import com.heaven.common.security.annotation.EnableRyFeignClients;
import com.heaven.common.swagger.annotation.EnableCustomSwagger2;
import com.heaven.dataconverge.dataprovider.jdbc.JdbcDataProvider;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 数据汇聚
 *
 * @author Vicene
 */
@EnableCustomConfig
@EnableCustomSwagger2
@EnableRyFeignClients
@SpringBootApplication
public class HeavenDataConvergeApplication implements ApplicationRunner
{
    public static void main(String[] args)
    {
        SpringApplication.run(HeavenDataConvergeApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  苍穹-数据汇聚模块启动成功   ლ(´ڡ`ლ)ﾞ ");
    }

    @Override
    public void run(ApplicationArguments args) {
        JdbcDataProvider.ProviderFactory.loadDriverInfo();
    }
}
