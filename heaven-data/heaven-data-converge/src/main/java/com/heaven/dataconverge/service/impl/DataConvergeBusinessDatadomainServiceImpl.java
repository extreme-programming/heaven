package com.heaven.dataconverge.service.impl;

import java.util.List;
import com.heaven.common.core.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.heaven.dataconverge.mapper.DataConvergeBusinessDatadomainMapper;
import com.heaven.dataconverge.domain.DataConvergeBusinessDatadomain;
import com.heaven.dataconverge.service.IDataConvergeBusinessDatadomainService;

/**
 * 数据模块-数据汇聚-数据域Service业务层处理
 * 
 * @author Vicene
 * @date 2022-11-29
 */
@Service
public class DataConvergeBusinessDatadomainServiceImpl implements IDataConvergeBusinessDatadomainService 
{
    @Autowired
    private DataConvergeBusinessDatadomainMapper dataConvergeBusinessDatadomainMapper;

    /**
     * 查询数据模块-数据汇聚-数据域
     * 
     * @param id 数据模块-数据汇聚-数据域主键
     * @return 数据模块-数据汇聚-数据域
     */
    @Override
    public DataConvergeBusinessDatadomain selectDataConvergeBusinessDatadomainById(Long id)
    {
        return dataConvergeBusinessDatadomainMapper.selectDataConvergeBusinessDatadomainById(id);
    }

    /**
     * 查询数据模块-数据汇聚-数据域列表
     * 
     * @param dataConvergeBusinessDatadomain 数据模块-数据汇聚-数据域
     * @return 数据模块-数据汇聚-数据域
     */
    @Override
    public List<DataConvergeBusinessDatadomain> selectDataConvergeBusinessDatadomainList(DataConvergeBusinessDatadomain dataConvergeBusinessDatadomain)
    {
        return dataConvergeBusinessDatadomainMapper.selectDataConvergeBusinessDatadomainList(dataConvergeBusinessDatadomain);
    }

    /**
     * 新增数据模块-数据汇聚-数据域
     * 
     * @param dataConvergeBusinessDatadomain 数据模块-数据汇聚-数据域
     * @return 结果
     */
    @Override
    public int insertDataConvergeBusinessDatadomain(DataConvergeBusinessDatadomain dataConvergeBusinessDatadomain)
    {
        dataConvergeBusinessDatadomain.setCreateTime(DateUtils.getNowDate());
        return dataConvergeBusinessDatadomainMapper.insertDataConvergeBusinessDatadomain(dataConvergeBusinessDatadomain);
    }

    /**
     * 修改数据模块-数据汇聚-数据域
     * 
     * @param dataConvergeBusinessDatadomain 数据模块-数据汇聚-数据域
     * @return 结果
     */
    @Override
    public int updateDataConvergeBusinessDatadomain(DataConvergeBusinessDatadomain dataConvergeBusinessDatadomain)
    {
        dataConvergeBusinessDatadomain.setUpdateTime(DateUtils.getNowDate());
        return dataConvergeBusinessDatadomainMapper.updateDataConvergeBusinessDatadomain(dataConvergeBusinessDatadomain);
    }

    /**
     * 批量删除数据模块-数据汇聚-数据域
     * 
     * @param ids 需要删除的数据模块-数据汇聚-数据域主键
     * @return 结果
     */
    @Override
    public int deleteDataConvergeBusinessDatadomainByIds(Long[] ids)
    {
        return dataConvergeBusinessDatadomainMapper.deleteDataConvergeBusinessDatadomainByIds(ids);
    }

    /**
     * 删除数据模块-数据汇聚-数据域信息
     * 
     * @param id 数据模块-数据汇聚-数据域主键
     * @return 结果
     */
    @Override
    public int deleteDataConvergeBusinessDatadomainById(Long id)
    {
        return dataConvergeBusinessDatadomainMapper.deleteDataConvergeBusinessDatadomainById(id);
    }
}
