package com.heaven.dataconverge.mapper;

import java.util.List;
import com.heaven.dataconverge.domain.DataConvergeSource;

/**
 * 数据源Mapper接口
 * 
 * @author Vicene
 * @date 2022-11-21
 */
public interface DataConvergeSourceMapper 
{
    /**
     * 查询数据源
     * 
     * @param id 数据源主键
     * @return 数据源
     */
    public DataConvergeSource selectDataConvergeSourceById(Long id);

    /**
     * 查询数据源列表
     * 
     * @param dataConvergeSource 数据源
     * @return 数据源集合
     */
    public List<DataConvergeSource> selectDataConvergeSourceList(DataConvergeSource dataConvergeSource);

    /**
     * 新增数据源
     * 
     * @param dataConvergeSource 数据源
     * @return 结果
     */
    public int insertDataConvergeSource(DataConvergeSource dataConvergeSource);

    /**
     * 修改数据源
     * 
     * @param dataConvergeSource 数据源
     * @return 结果
     */
    public int updateDataConvergeSource(DataConvergeSource dataConvergeSource);

    /**
     * 删除数据源
     * 
     * @param id 数据源主键
     * @return 结果
     */
    public int deleteDataConvergeSourceById(Long id);

    /**
     * 批量删除数据源
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDataConvergeSourceByIds(Long[] ids);

    /**
     * 获取数据源列表（包含业务板块等）
     * @param dataConvergeSource
     * @return
     */
    List<DataConvergeSource>  selectSourceBusinessSectionList(DataConvergeSource dataConvergeSource);

    /**
     * 数据源详情
     * @param id
     * @return
     */
    DataConvergeSource selectDetailById(Long id);

    /**
     * 获取数据源类型实例
     * @param dbType
     * @return
     */
    List<DataConvergeSource> getDataSourceInstance(String dbType);
}
